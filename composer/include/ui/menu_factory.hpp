/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef _composer_menu_factor_hpp_
#define _composer_menu_factor_hpp_

#include <bg/wnd/popup_menu.hpp>
#include <ui/action.hpp>

#include <map>

namespace composer {
namespace ui {

class MenuFactory {
	friend class UserActions;
public:
	MenuFactory();
	~MenuFactory();


	inline const bg::wnd::MenuDescriptor & menu() const { return _menu; }

protected:

	// Currently, the menu path must have exactly two elements: menu and menu item
	void addMenu(const std::string & path, int32_t identifier);
	void addMenu(const std::string & path, int32_t identifier, const bg::base::KeyboardShortcut & sc);
	
	bg::wnd::MenuDescriptor _menu;
	std::map<std::string, bg::wnd::PopUpMenu*> _menuNames;
};

}
}

#endif
