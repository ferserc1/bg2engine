/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <event_handler.hpp>

class SensorComponent : public bg::scene::Component {
public:
	Component * clone() { return new SensorComponent(); }
	
	virtual void sensorEvent(const bg::base::SensorEvent & evt) {
		if (transform()) {
			transform()->matrix()
				.identity()
				.translate(evt.acceleration().x(),
						   evt.acceleration().y(),
						   evt.acceleration().z());
		}
	}
};


bg::scene::Node * createScene(bg::base::Context * ctx) {
	using namespace bg::scene;
	using namespace bg::math;
	bg::ptr<Node> sceneRoot = new Node(ctx,"SceneRoot");
	
	Node * sphere = new Node(ctx, "Sphere");
	PrimitiveFactory factory(ctx);
	sphere->addComponent(factory.sphere(Scalar(10,distance::cm)));
	sphere->addComponent(new SensorComponent());
	sphere->addComponent(new Transform());
	sceneRoot->addChild(sphere);
	
	Node * cam = new Node(ctx, "Camera");
	cam->addComponent(new bg::scene::Camera());
	bg::math::Matrix4 trx;
	trx.identity()
		.rotate(bg::math::trigonometry::degreesToRadians(22.5f), -1.0f, 0.0f, 0.0f)
		.translate(.0f, .0f, 5.0f);
	cam->addComponent(new bg::scene::Transform(trx));
	cam->component<bg::scene::Camera>()->setFocus(Scalar(5,distance::meter));	// Set the focus at the same distance as the transform is
	sceneRoot->addChild(cam);
	bg::scene::OpticalProjectionStrategy * projectionStrategy = new bg::scene::OpticalProjectionStrategy();
	projectionStrategy->setFrameSize(Scalar(35,distance::mm));	// 35mm film
	projectionStrategy->setFocalLength(Scalar(50,distance::mm));	// 50mm focal length
	cam->component<bg::scene::Camera>()->setProjectionStrategy(projectionStrategy);

	Node * light = new Node(ctx, "Light");
	light->addComponent(new bg::scene::Light());
	trx.identity()
		.rotate(bg::math::trigonometry::degreesToRadians(25.5f), -1.0f, 0.0f, 0.0f)
		.translate(.0f, .0f, 10.0f);
	light->addComponent(new bg::scene::Transform(trx));
	sceneRoot->addChild(light);
	
	return sceneRoot.release();
}

MyEventHandler::MyEventHandler()
{
	
}

MyEventHandler::~MyEventHandler() {
	
}

void MyEventHandler::willCreateContext() {
	if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else if (bg::system::isDesktop()){
		throw bg::base::CompatibilityException("Fatal error: no suitable rendering engine found.");
	}
}

void MyEventHandler::initGL() {
	bg::Engine::Get()->initialize(context());
	
	_inputVisitor = new bg::scene::InputVisitor();
	
	_renderer = bg::render::Renderer::Create(context(), bg::render::Renderer::kRenderPathForward);

	// Render settings method 1: get the setting class (ShadowMap) and use the accessor
	bg::render::ShadowMap * shadowMap = _renderer->settings<bg::render::ShadowMap>();
	if (shadowMap) {
		shadowMap->setShadowType(bg::render::ShadowMap::kSoftShadows);
	}

	// Render settings method 2 (preferred method): use de setRenderSetting() function.
	_renderer->setRenderSetting(bg::render::settings::kShadowMap, bg::render::settings::kShadowMapSize, bg::math::Size2Di(2048));
	
	_sceneRoot = createScene(context());
	_sceneRoot->someChild([&](bg::scene::Node * node) -> bool {
		return (_camera = node->component<bg::scene::Camera>()).valid();
	});
}

void MyEventHandler::reshape(int w, int h) {
	_camera->setViewport(bg::math::Viewport(0,0,w,h));
}

void MyEventHandler::frame(float delta) {
	_renderer->frame(_sceneRoot.getPtr(), delta);
}

void MyEventHandler::draw() {
	_renderer->draw(_sceneRoot.getPtr(), _camera.getPtr());
	context()->swapBuffers();
}

void MyEventHandler::onMemoryWarning() {
	std::cout << "Memory warning received" << std::endl;
}

void MyEventHandler::sensorEvent(const bg::base::SensorEvent & evt) {
	_inputVisitor->sensorEvent(_sceneRoot.getPtr(), evt);
	bg::log(bg::log::kDebug) << evt.acceleration() << bg::endl;
}
