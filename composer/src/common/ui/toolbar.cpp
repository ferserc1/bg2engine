/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <ui/toolbar.hpp>

#include <action/file_actions.hpp>
#include <action/physics_actions.hpp>

#include <app.hpp>

#include <bg/db/db.hpp>

namespace composer {
namespace ui {

ToolBar::ToolBar(bg::gui::Surface * surface)
	:Window(surface)
{
}

ToolBar::~ToolBar() {
}

void ToolBar::init() {
	Window::init();

	using namespace bg::gui;
	HorizontalLayout * layout = new HorizontalLayout();
	layout->setBorderPadding(4);
	contentView()->setLayout(layout);

	bg::system::Path iconsPath = bg::system::Path::ResourcesDir();
	iconsPath.addComponent("icons-light");
	addItem(iconsPath.pathAddingComponent("folder_open.png"), "Open", action::FileActions::kCodeOpen);
	addItem(iconsPath.pathAddingComponent("save.png"), "Save", action::FileActions::kCodeSave);
	addItem(iconsPath.pathAddingComponent("import.png"), "Import", action::FileActions::kCodeImport);
	addItem(iconsPath.pathAddingComponent("export.png"), "Export", action::FileActions::kCodeExport);
    
    //addItem(iconsPath.pathAddingComponent("play.png"),"Play", action::PhysicsActions::kCodePlay);
    //addItem(iconsPath.pathAddingComponent("pause.png"),"Pause", action::PhysicsActions::kCodePause);
    //addItem(iconsPath.pathAddingComponent("stop.png"),"Stop", action::PhysicsActions::kCodePlay);
}

void ToolBar::addItem(const std::string & item, Action::ActionCode c) {
	using namespace bg::gui;
	Button * btn = Button::Create<Button>(context(), skin());
	btn->setText(item);
	btn->setAction([c]() {
		App::Get().userActions().exec(c);
	});
	contentView()->addChild(btn);
}

void ToolBar::addItem(const bg::system::Path & img, const std::string & text, Action::ActionCode c) {
	using namespace bg::gui;
	ToolbarItem * btn = View::Create<ToolbarItem>(context(),skin());
	btn->setIcon(bg::db::loadTexture(context(), img));
	btn->setText(text);
	btn->setAction([c]() {
		App::Get().userActions().exec(c);
	});
	contentView()->addChild(btn);
}

}
}
