package com.bg2engine.android;

import android.content.Context;
import android.content.res.AssetManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.opengl.GLSurfaceView;
import android.os.Environment;
import android.view.MotionEvent;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import com.bg2engine.base.EventHandler;

public class GLView extends GLSurfaceView implements SensorEventListener {
    SensorManager _sensorManager;
    Sensor _accelerometer;

    public GLView(Context context, WindowManager wm) {
        super(context);
        setEGLConfigChooser(8, 8, 8, 0, 16, 0);
        setEGLContextClientVersion(2);
        setRenderer(new Renderer(getResources().getAssets(),context,wm.getDefaultDisplay()));

        _sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        _accelerometer = _sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        _sensorManager.registerListener(this, _accelerometer, SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];
        EventHandler.accelerationEvent(x,y,z);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public boolean onTouchEvent(MotionEvent evt) {
        int numEvents = evt.getPointerCount();
        if (numEvents>4) numEvents = 4;
        int event = 0;
        int action = evt.getActionMasked();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN:
                event = 1;
                break;
            case MotionEvent.ACTION_MOVE:
                event = 2;
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                event = 3;
                break;
            case MotionEvent.ACTION_OUTSIDE:
            case MotionEvent.ACTION_CANCEL:
        }
        switch (numEvents) {
            case 1:
                EventHandler.touchEvent1(event,evt.getX(0),evt.getY(0));
                break;
            case 2:
                EventHandler.touchEvent2(event,evt.getX(0),evt.getY(0),evt.getX(1),evt.getY(1));
                break;
            case 3:
                EventHandler.touchEvent3(event,evt.getX(0),evt.getY(0),evt.getX(1),evt.getY(1),evt.getX(2),evt.getY(2));
                break;
            case 4:
                EventHandler.touchEvent4(event,evt.getX(0),evt.getY(0),evt.getX(1),evt.getY(1),evt.getX(2),evt.getY(2),evt.getX(3),evt.getY(3));
                break;
        }
        return true;
    }

    private static class Renderer implements GLSurfaceView.Renderer {
        private AssetManager _assetManager;
        private Context _context;
        private Display _mainDisplay;

        Renderer(AssetManager mgr, Context ctx, Display mainDisplay) {
            _assetManager = mgr;
            _context = ctx;
            _mainDisplay = mainDisplay;
        }

        public void onDrawFrame(GL10 gl) {
            EventHandler.draw();
        }

        public void onSurfaceChanged(GL10 gl, int width, int height) {
            EventHandler.reshape(width,height);
        }

        public void onSurfaceCreated(GL10 gl, EGLConfig config) {
            Point size = new Point();
            DisplayMetrics dm = new DisplayMetrics();
            _mainDisplay.getMetrics(dm);
            _mainDisplay.getSize(size);
            EventHandler.init(_assetManager,
                    _context.getFilesDir().getAbsolutePath(),
                    Environment.getExternalStorageDirectory().getAbsolutePath(),
                    size.x, size.y, dm.density);
        }
    }
}

