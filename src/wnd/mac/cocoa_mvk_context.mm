#include <bg/wnd/cocoa_mvk_context.hpp>

#include <bg/wnd/window_controller.hpp>

#import <Cocoa/Cocoa.h>

namespace bg {
namespace wnd {

bool CocoaMVKContext::createContext() {
    return VKContext::createContext();
}

void CocoaMVKContext::makeCurrent() {
    VKContext::makeCurrent();
}

void CocoaMVKContext::swapBuffers() {
    VKContext::swapBuffers();
}

void CocoaMVKContext::destroy() {
    VKContext::destroy();
}

void CocoaMVKContext::setCocoaView(bg::plain_ptr view) {
    _cocoaView = view;
}

}
}
