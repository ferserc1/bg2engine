#ifndef _viewer_gui_hpp_
#define _viewer_gui_hpp_

#include <bg/base/referenced_pointer.hpp>
#include <bg/base/context_object.hpp>
#include <bg/base/event_handler.hpp>
#include <bg/gui/surface.hpp>

#include <config_ui.hpp>

namespace viewer {

class Gui : public bg::base::ReferencedPointer, bg::base::ContextObject {
public:
	Gui(bg::base::Context *,bg::wnd::Window *);

	void init();

	void resize(int w, int h);
	void frame(float delta);
	void draw();

	bool keyUp(const bg::base::KeyboardEvent & evt);
	bool keyDown(const bg::base::KeyboardEvent & evt);
	bool mouseDown(const bg::base::MouseEvent & evt);
	bool mouseDrag(const bg::base::MouseEvent & evt);
	bool mouseUp(const bg::base::MouseEvent & evt);
	bool mouseMove(const bg::base::MouseEvent & evt);
	bool mouseWheel(const bg::base::MouseEvent & evt);

	inline ConfigUI * configUI() { return _configUi.getPtr(); }

protected:
	virtual ~Gui();

	bg::ptr<ConfigUI> _configUi;
	bg::ptr<bg::gui::Surface> _surface;
	
	bg::wnd::Window * _window;
};
}

#endif
