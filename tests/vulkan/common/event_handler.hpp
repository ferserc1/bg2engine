/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */



#ifndef event_handler_hpp
#define event_handler_hpp

#include <bg/base/event_handler.hpp>
#include <bg/base/pipeline.hpp>
#include <bg/math/vector.hpp>

class MyEventHandler : public bg::base::EventHandler {
public:
	MyEventHandler();
	
	// Creation and initialization
	virtual void willCreateContext();
	virtual void initGL();
	virtual void willDestroyContext();
	
	// Reshape and draw
	virtual void reshape(int, int);
	virtual void draw();
	// NOTE: do not use draw() event to update the scene, there is a
	// frame(float delta) event to do this.
	
	// Only on mobile platforms. The operating system send this event
	// if we need to free memory
	virtual void onMemoryWarning();
	
	// Touch events
	virtual void touchStart(const bg::base::TouchEvent & evt);
	virtual void touchMove(const bg::base::TouchEvent & evt);
	virtual void touchEnd(const bg::base::TouchEvent & evt);

	// Desktop events
	virtual void keyUp(const bg::base::KeyboardEvent & evt);
	virtual void mouseDown(const bg::base::MouseEvent & evt);
	virtual void mouseDrag(const bg::base::MouseEvent & evt);

	// NOTE: See the full list of events in bg/base/event_handler.hpp
	
protected:
	virtual ~MyEventHandler();
	
	bg::ptr<bg::base::Pipeline> _pipeline;
	bg::math::Position2Di _startPos;
	
	void setColorDiff(float diff, int channel);
};

#endif /* event_handler_hpp */
