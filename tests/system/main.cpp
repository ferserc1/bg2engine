/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

//
//  main.cpp
//  system
//
//  Created by Fernando Serrano Carpena on 27/4/16.
//  Copyright © 2016 Fernando Serrano Carpena. All rights reserved.
//

#include <iostream>
#include <bg/system/path.hpp>
#include <bg/system/archive.hpp>

#include <bg/db/json/value.hpp>
#include <bg/db/json/parser.hpp>


void jsonSample() {
	using namespace bg;
	using namespace bg::db;
	
	ptr<json::Value> root = new json::Value();
	
	ptr<json::Value> array = new json::Value();
	array->push("string");
	array->push(10.55);
	array->push(json::Value::ValueMap{ {"object", new json::Value("inside array") } });
	root->setValue("array",array.getPtr());
	
	ptr<json::Value> object = new json::Value();
	object->setValue("key1","value 1");
	object->setValue("key2",73);
	object->setValue("key3", json::Value::ValueMap{ { "key", new json::Value("object") } });
	object->setValue("key4", true);
	object->setValue("key5", false);
	root->setValue("object", object.getPtr());

	std::ofstream outJson;
	system::Path outFile = system::Path::ExecDir();
	outFile.addComponent("out.json");
	outJson.open(outFile.text());
	root->writeToStream(outJson);
	outJson.close();

	system::Path jsonFile = system::Path::ExecDir();
	jsonFile.addComponent("data/test.json");
	ptr<json::Value> loadedJson = json::Parser::ParseFile(jsonFile);
	root->writeToStream(std::cout);
}

void pathSample() {
	std::cout << "Home path: " << bg::system::Path::HomeDir().text() << std::endl;
	std::cout << "Executable path: " << bg::system::Path::ExecDir().text() << std::endl;
	std::cout << "Temp path: " << bg::system::Path::TempDir().text() << std::endl;
	
	bg::system::Path p1 = bg::system::Path::HomeDir();
	
	std::cout << "Add test/dir to home: " << p1.addComponent("test/dir").text() << std::endl;
	
	std::cout << "Remove last component: " << p1.removeLastComponent().text() << std::endl;
	
	p1.removeLastComponent()
		.removeLastComponent()
		.removeLastComponent()
		.removeLastComponent()
		.removeLastComponent();
	std::cout << "Remove all components: " << p1.text() << std::endl;
	
	bg::system::Path relativePath("this/is/a/relative/path");
	std::cout << "Relative path: " << relativePath.text() << std::endl;
	std::cout << "Add component: " << relativePath.addComponent("new/component").text() << std::endl;
	std::cout << "Remove component: " << relativePath.removeLastComponent().text() << std::endl;
	
	bg::system::Path filePath(bg::system::Path::HomeDir());
	filePath.addComponent("file.txt");
	
	std::cout << "File path: " << filePath.text() << std::endl;
	std::cout << "File name: " << filePath.fileName() << std::endl;
	std::cout << "File extension: " << filePath.extension() << std::endl;
	std::cout << "Last path component: " << filePath.lastPathComponent() << std::endl;
	
	std::cout << "Remove extension: " << filePath.removeExtension().text() << std::endl;
	std::cout << "Add new extension: " << filePath.addExtension("jpg").text() << std::endl;
	
	bg::system::Path exe = bg::system::Path::ExecDir();
	std::cout << "List executable directory: " << std::endl;
	exe.list([&](const bg::system::Path & path) {
		std::cout << path.text();
		if (path.isFile()) {
			std::cout << " (file)" << std::endl;
		}
		else if (path.isDirectory()) {
			std::cout << " (directory)" << std::endl;
		}
	});
	
	exe.removeLastComponent();
	std::cout << "List executable directory parent, recursive: " << std::endl;
	exe.listRecursive([&](const bg::system::Path & path) {
		std::cout << path.text() << std::endl;
	});
	
	bg::system::Path newDir = bg::system::Path::HomeDir();
	newDir.addComponent("newdir");
	newDir.create();
	
	bg::system::Path copyFile(bg::system::Path::ExecDir());
	copyFile.addComponent("bricks.jpg");
	copyFile.copy(newDir.addComponent("image.jpg"));
	
	newDir.removeLastComponent()
		  .remove();
}

void archiveSample() {
	bg::system::Path src = bg::system::Path::ExecDir().addComponent("test-pkg.bg2pkg");
	bg::system::Path dst = bg::system::Path::HomeDir().addComponent("Desktop");
	bg::system::Archive packArchive;
	bg::system::Archive unpackArchive;
	
	try {
		packArchive.open(src, bg::system::Archive::kModePack);
		bg::system::Path filesPath = src.pathRemovingLastComponent();
		filesPath.addComponent("data");
		packArchive.addFile(filesPath.pathAddingComponent("test-shape.vwglb"));
		packArchive.addFile(filesPath.pathAddingComponent("texture.jpg"));
		packArchive.addFile(filesPath.pathAddingComponent("texture_bm.jpg"));
		packArchive.addFile(filesPath.pathAddingComponent("texture_nm.png"));
		packArchive.addFile(filesPath.pathAddingComponent("texture_reflection_mask.png"));
		
		unpackArchive.open(src, bg::system::Archive::kModeUnpack);
		unpackArchive.unpackTo(dst, true);
	}
	catch (std::runtime_error & e) {
		std::cerr << e.what() << std::endl;
	}
}

int main(int argc, const char * argv[]) {
	//pathSample();
	//archiveSample();
	jsonSample();
	
    return 0;
}
