/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <event_handler.hpp>

#include <bg/bg2e.hpp>

#include <bg/render/render.hpp>
#include <bg/android/app.hpp>

class Toolbar : public bg::gui::Window {
public:
	Toolbar(bg::gui::Surface * s) :Window(s) {}

	virtual void init() {
		using namespace bg::gui;

		HorizontalLayout * layout = new HorizontalLayout();
		layout->setBorderPadding(4);
		contentView()->setLayout(layout);
		
		Button * btn1 = View::Create<Button>(context(), skin());
		btn1->setText("Hello");
		btn1->setAction([&]() {
			bg::wnd::MessageBox::Show(bg::wnd::MainLoop::Get()->window(), "Hello", "You press hello button");
		});
		contentView()->addChild(btn1);
		
		Button * btn2 = View::Create<Button>(context(), skin());
		btn2->setText("Bye");
		btn2->setAction([&]() {
			bg::wnd::MessageBox::Show(bg::wnd::MainLoop::Get()->window(), "Bye", "You press bye button");
		});
		contentView()->addChild(btn2);
	}

protected:
	virtual ~Toolbar() {}
};

MyEventHandler::MyEventHandler()
{
	
}

MyEventHandler::~MyEventHandler() {
	
}

void MyEventHandler::willCreateContext() {
	if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else if (bg::system::isDesktop()){
		throw bg::base::CompatibilityException("Fatal error: no suitable rendering engine found.");
	}
}

void MyEventHandler::initGL() {
	using namespace bg::scene;
	using namespace bg::math;
	bg::Engine::Get()->initialize(context());
	context()->setVsyncEnabled(true);

	std::vector<bg::system::Screen> screens = bg::system::Screen::ScreenList();
	for (auto & s : screens) {
		bg::log(bg::log::kDebug) << "Screen " << s.id() << ": "
			<< s.name() << ", res: " << s.size() << ", scale: " << s.scale() << bg::endl;
	}
   

    bg::system::Path path = bg::system::Path::AppDir();
    if (bg::system::currentPlatform()==bg::system::kAndroid) {
        path = bg::system::Path::ResourcesDir();
    }

    if (bg::system::currentPlatform()==bg::system::kAndroid) {
        bg::android::App::Get().extractAllAssets(path.pathAddingComponent("data"));
    }

	bg::db::GuiSkinLoader::RegisterPlugin(new bg::db::plugin::ReadGuiSkinBg2());
	
	_inputVisitor = new InputVisitor();
	_renderer = bg::render::Renderer::Create(context(), bg::render::Renderer::kRenderPathForward);

	_sceneRoot = new Node(context());

	_camera = new Camera();
	Node * cameraNode = new Node(context());
	cameraNode->addComponent(_camera.getPtr());
	cameraNode->addComponent(new Transform());
	bg::manipulation::OrbitNodeController * orbit = new bg::manipulation::OrbitNodeController();
	orbit->setRotation(bg::math::Vector2(0.0f, 0.0f));
	cameraNode->addComponent(orbit);
	_camera->setProjectionStrategy(new OpticalProjectionStrategy());
	_sceneRoot->addChild(cameraNode);

	Node * floor = new Node(context());
	floor->addComponent(PrimitiveFactory(context()).plane(10.0f));
	floor->addComponent(new Transform(Matrix4::Translation(0.0f, -0.5f, 0.0f)));
	_sceneRoot->addChild(floor);
	
	Node * cube = new Node(context());
	cube->addComponent(PrimitiveFactory(context()).cube(1.0f));
	cube->drawable()->material(0)->setDiffuse(bg::math::Color::Green());
	cube->drawable()->material(0)->setShininess(50.0f);
	_sceneRoot->addChild(cube);
	
	Node * light = new Node(context());
	light->addComponent(new Transform(Matrix4::Identity()
		.rotate(trigonometry::degreesToRadians(55.0f), -1.0f, 0.0f, 0.0f)
		.translate(0.0f, 0.0f, 5.0f)));
	light->addComponent(new Light());
	bg::base::Light * l = light->light()->light();
	l->setType(bg::base::Light::kTypeSpot);
	l->setAmbient(Color::Black());
	l->setConstantAttenuation(-1.0f);
	l->setLinearAttenuation(0.01f);
	l->setSpotExponent(65.0f);
	_sceneRoot->addChild(light);

	/////  Create gui skin
	{
		using namespace bg::gui;
		bg::ptr<Skin> skin = bg::db::loadSkin(context(), path.pathAddingComponent("data/default.bg2skin"));

		_guiSurface = new Surface(context(), skin.getPtr());
		_guiSurface->setId("surface");

		Window * wnd = new Window(_guiSurface.getPtr());
		wnd->create();
		wnd->setPosition(bg::math::Position2Di(10, 90));
		wnd->setSize(bg::math::Size2Di(300, 200));
		wnd->setAlignment(kAlignTopLeft);

		bg::ptr<Label> subview = View::Create<Label>(context(), skin.getPtr());
		subview->setText("Hello world\nThis is a test label, used to implement the automatic line break in multiline label.");
		subview->setAutoLineBreak(true);

		subview->setBackgroundColor(bg::math::Color::Red());
		subview->setBorderColor(bg::gui::kStatusHover, bg::math::Color::Pink());
		subview->setBorderWidth(bg::gui::kStatusHover, 10);
		subview->setBackgroundColor(bg::gui::kStatusHover, bg::math::Color::Black());
		subview->setBorderColor(bg::math::Color::Yellow());
		subview->setBorderWidth(1);

		subview->setSize(bg::math::Size2Di(280, 180));
		subview->setPosition(bg::math::Position2Di(10, 10));
		subview->paragraph().setAlignment(bg::text::Paragraph::kHAlignLeft);
		subview->paragraph().setAlignment(bg::text::Paragraph::kVAlignTop);
		wnd->contentView()->addChild(subview.getPtr());


		wnd = new Toolbar(_guiSurface.getPtr());
		wnd->create();
		wnd->setAlignment(kAlignBottomLeft);
		wnd->setAnchor(bg::gui::kAnchorRight);
		wnd->setSize(bg::math::Size2Di(300, 40));

		wnd = new Window(_guiSurface.getPtr());
		wnd->create();
		wnd->setSize(bg::math::Size2Di(380, 300));
		wnd->setAlignment(kAlignMiddleCenter);
		wnd->contentView()->setLayout(new VerticalLayout());

		ImageButton * btn = View::Create<ImageButton>(context(), skin.getPtr());
		btn->setImage(kStatusNormal, bg::db::loadTexture(context(), path.pathAddingComponent("data/test-image-button-normal.png")));
		btn->setImage(kStatusHover, bg::db::loadTexture(context(), path.pathAddingComponent("data/test-image-button-hover.png")));
		btn->setImage(kStatusPress, bg::db::loadTexture(context(), path.pathAddingComponent("data/test-image-button-press.png")));
		btn->setText("Clear Red View");
		Button * btn2 = View::Create<Button>(context(), skin.getPtr());
		btn2->setText("Copy To Red View");
		TextInput * text = View::Create<TextInput>(context(), skin.getPtr());
		text->setText("Hello, World! This is a test to implement the multiline label.");

		text->setWidth(250);
		btn->setAction([subview]() {
			subview->setText("");
		});
		btn2->setAction([subview, text]() {
			subview->setText(text->text());
		});
		text->setEditFinishedClosure([text, subview](bg::gui::FormField * field) {
			subview->setText(text->text());
		});
		CheckBoxField * check = View::Create<CheckBoxField>(context(), skin.getPtr());
		check->setText("Show copy button");
		check->setValue(CheckBox::kValueOn);
		check->setOnValueChangedAction([btn2](CheckBox::Value v) {
			btn2->setVisible(v == CheckBox::kValueOn);
		});

		wnd->contentView()->addChild(btn);
		wnd->contentView()->addChild(btn2);
		wnd->contentView()->addChild(text);
		wnd->contentView()->addChild(check);

		RadioButtonField * radio1 = View::Create<RadioButtonField>(context(), skin.getPtr());
		RadioButtonField * radio2 = View::Create<RadioButtonField>(context(), skin.getPtr());
		RadioButtonField * radio3 = View::Create<RadioButtonField>(context(), skin.getPtr());
		radio1->setText("Radio button 1");
		radio2->setText("Radio button 2");
		radio3->setText("Radio button 3");
		radio1->addToGroup(radio2);
		radio1->addToGroup(radio3);
		radio1->setOnValueChangedAction([text](bool selected, int selectedIndex) {
			text->setText("Selected index: " + std::to_string(selectedIndex));
		});
		radio2->setOnValueChangedAction([subview](bool selected, int) {
			subview->setText(selected ? "Radio 2 selected" : "Radio 2 not selected");
		});

		wnd->contentView()->addChild(radio1);
		wnd->contentView()->addChild(radio2);
		wnd->contentView()->addChild(radio3);


		bg::gui::SpinButtonField * spin = View::Create<SpinButtonField>(context(),skin.getPtr());
		spin->setWidth(100);
		spin->setPrecision(1);
		wnd->contentView()->addChild(spin);

		for (auto i = 0; i < 20; ++i) {
			Button * listBtn = View::Create<Button>(context(), skin.getPtr());
			listBtn->setText("List item " + std::to_string(i));
			wnd->contentView()->addChild(listBtn);
		}

		wnd->contentView()->addDecorator(new bg::gui::ScrollDecorator());

		wnd = new bg::gui::Window(_guiSurface.getPtr());
		wnd->create();
		wnd->setSize(bg::math::Size2Di(170, 50));
		wnd->setAlignment(kAlignTopRight);
		_fpsLabel = View::Create<Label>(context(), skin.getPtr());
		_fpsLabel->paragraph().setAlignment(bg::text::Paragraph::kHAlignRight);
		_fpsLabel->paragraph().setAlignment(bg::text::Paragraph::kVAlignCenter);
		_fpsLabel->setWidth(160);
		_fpsLabel->setPosition(bg::math::Position2Di(5, 5));
		_fpsLabel->setText("FPS:");
		wnd->contentView()->addChild(_fpsLabel.getPtr());
		wnd->contentView()->setBackgroundColor(bg::math::Color::Transparent());
	}
	
}

void MyEventHandler::reshape(int w, int h) {
	_camera->setViewport(bg::math::Viewport(0,0,w,h));
	_guiSurface->resize(w,h);
}

void MyEventHandler::frame(float delta) {
	_renderer->frame(_sceneRoot.getPtr(), delta);
	_fpsCounter.frame(delta);

	static float scale = 1.0f;
	float s = this->scale();
    _guiSurface->setScale(s);

	if (_fpsCounter.valueUpdated() || s!=scale) {
		_fpsLabel->setText("FPS: " + std::to_string(_fpsCounter.fps()) + ", Scale: " + std::string(std::to_string(s),0,4));
		scale = s;
	}
    
//    bg::log(bg::log::kDebug) << "Window scale: " << scale << bg::endl;
}

void MyEventHandler::draw() {
	_renderer->draw(_sceneRoot.getPtr(), _camera.getPtr());
	_guiSurface->draw();
	context()->swapBuffers();
}

void MyEventHandler::onMemoryWarning() {
	std::cout << "Memory warning received" << std::endl;
}

// Mobile touch events
void MyEventHandler::touchStart(const bg::base::TouchEvent & evt)  {
	if (!_guiSurface->touchStart(evt)) {
        _inputVisitor->touchStart(_sceneRoot.getPtr(), evt);
    }
}

void MyEventHandler::touchMove(const bg::base::TouchEvent & evt)  {
    if (!_guiSurface->touchMove(evt)) {
        _inputVisitor->touchMove(_sceneRoot.getPtr(), evt);
    }
}

void MyEventHandler::touchEnd(const bg::base::TouchEvent & evt) {
    if (!_guiSurface->touchEnd(evt)) {
        _inputVisitor->touchEnd(_sceneRoot.getPtr(), evt);
    }
}


// Desktop events
void MyEventHandler::keyUp(const bg::base::KeyboardEvent & evt) {
	if (evt.keyboard().key() == bg::base::Keyboard::kKeyEsc) {
		bg::wnd::MainLoop::Get()->quit(0);
	}
	else if (!_guiSurface->keyUp(evt)) {
		_inputVisitor->keyUp(_sceneRoot.getPtr(), evt);
	}
}

void MyEventHandler::keyDown(const bg::base::KeyboardEvent & evt) {
	if (!_guiSurface->keyDown(evt)) {
		_inputVisitor->keyDown(_sceneRoot.getPtr(), evt);
	}
}

void MyEventHandler::charPress(const bg::base::KeyboardEvent & evt) {
	if (!_guiSurface->charPress(evt)) {
		_inputVisitor->charPress(_sceneRoot.getPtr(), evt);
	}
}

void MyEventHandler::mouseDown(const bg::base::MouseEvent & evt) {
	if (!_guiSurface->mouseDown(evt)) {
		_inputVisitor->mouseDown(_sceneRoot.getPtr(), evt);
	}
}

void MyEventHandler::mouseMove(const bg::base::MouseEvent & evt) {
	if (!_guiSurface->mouseMove(evt)) {
		_inputVisitor->mouseMove(_sceneRoot.getPtr(), evt);
	}
}

void MyEventHandler::mouseDrag(const bg::base::MouseEvent & evt) {
	if (!_guiSurface->mouseDrag(evt)) {
		_inputVisitor->mouseDrag(_sceneRoot.getPtr(), evt);
	}
}

void MyEventHandler::mouseWheel(const bg::base::MouseEvent & evt) {
	if (!_guiSurface->mouseWheel(evt)) {
		_inputVisitor->mouseWheel(_sceneRoot.getPtr(), evt);
	}
}

void MyEventHandler::mouseUp(const bg::base::MouseEvent & evt) {
	_guiSurface->mouseUp(evt);
}

void MyEventHandler::buildMenu(bg::wnd::MenuDescriptor & menu) {
	bg::wnd::PopUpMenu * file = bg::wnd::PopUpMenu::New("File");
	file->addMenuItem({ bg::wnd::kCodeOpen, "Open" });
	file->addMenuItem({ bg::wnd::kCodeClose, "Close" });
	
	if (bg::system::currentPlatform()!=bg::system::kMac) {
		file->addMenuItem({ bg::wnd::kCodeQuit, "Exit" });
	}

	bg::wnd::PopUpMenu * edit = bg::wnd::PopUpMenu::New("Edit");
	edit->addMenuItem({ bg::wnd::kCodeCopy, "Copy" });
	edit->addMenuItem({ bg::wnd::kCodePaste, "Paste" });
	edit->addMenuItem({ bg::wnd::kCodeCustom + 100, "Custom command" });

	menu.push_back(file);
	menu.push_back(edit);
}

void MyEventHandler::menuSelected(const std::string & title, int32_t identifier) {
	std::cout << title << " selected. Identifier: " << identifier << std::endl;
	if (identifier == bg::wnd::kCodeQuit) {
		bg::wnd::MainLoop::Get()->quit(0);
	}
}
