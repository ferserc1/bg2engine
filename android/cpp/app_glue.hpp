
#ifndef _android_bg2esrc_app_glue_hpp_
#define _android_bg2esrc_app_glue_hpp_

#include <bg/android/app.hpp>
#include <bg/engine/openglCore/opengl_state.hpp>
#include <bg/android/gles_context.hpp>
#include <bg/base/event_handler.hpp>

#include <mutex>

#include <sys/time.h>

class Renderer {
public:
    static Renderer & Get() {
        if (!s_renderer) {
            s_renderer = new Renderer();
        }
        return *s_renderer;
    }

	bg::base::EventHandler * createEventHandler();

    void init();
    void reshape(int w, int h);
    void draw();
    void touchStart(const std::vector<bg::math::Position2Di> & touches);
    void touchMove(const std::vector<bg::math::Position2Di> & touches);
    void touchEnd(const std::vector<bg::math::Position2Di> & touches);
    void accelerationEvent(float x, float y, float z);

protected:

    static Renderer * s_renderer;

	bg::ptr<bg::base::EventHandler> _eventHandler;

    Renderer();
	virtual ~Renderer();

private:
    bg::ptr<bg::android::GLESContext> _context;
    std::mutex _sensorMutex;
    bg::math::Vector3 _accelerometer;
    timespec _lastTime;
};

#endif