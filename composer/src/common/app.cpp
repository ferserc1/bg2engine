/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */


#include <app.hpp>
#include <event_handler.hpp>

#include <bg/log.hpp>
#include <bg/wnd/wnd.hpp>

#include <bg/db/db.hpp>


// Actions
#include <action/file_actions.hpp>
#include <action/edit_actions.hpp>
#include <action/physics_actions.hpp>

namespace composer {

App App::s_app;

App::App()
{
	bg::system::Path configPath = bg::system::Path::ExecDir();
	if (bg::system::currentPlatform() == bg::system::kMac) {
		configPath = bg::system::Path::ResourcesDir();
	}
	configPath.addComponent("config.json");
	_mainConfig = new Config(configPath);
	_mainConfig->load();
}

App::~App() {
	delete _mainConfig;
}

int App::run() {
	{
		bg::ptr<bg::wnd::Window> window = bg::wnd::Window::New();

		bg::math::Rect windowRect = config().getVector4i("mainWindowRect",bg::math::Rect(50,50,1024,768));
		if (!window.valid()) {
			std::cerr << "Platform not supported" << std::endl;
			return -1;
		}
		window->setRect(windowRect);
		window->setTitle("bg2 Composer");

		window->setEventHandler(new EventHandler());

		initActions();

		window->create();

		bg::wnd::MainLoop::Get()->setWindow(window.getPtr());
	}
	int status = bg::wnd::MainLoop::Get()->run();
	_mainConfig->save();

	return status;
}

void App::init(bg::base::Context * ctx) {
	_ctx = ctx;

	loadPlugins();

	_scene = new scene::Scene(ctx);
	_scene->newScene();
	_workspace = new ui::Workspace();


	workspace().buildUserInterface(context());
}

void App::destroy() {
	_commandManager.cleanUp();
	_scene->destroy();
	delete _workspace;
	_workspace = nullptr;
	_scene = nullptr;
	_ctx = nullptr;
}

void App::windowRectChanged(const bg::math::Rect & rect) {
	config().set("mainWindowRect", rect);
}

void App::loadPlugins() {
	bg::db::AudioLoader::RegisterPlugin(new bg::db::plugin::ReadWavAudio());

	bg::db::DrawableLoader::RegisterPlugin(new bg::db::plugin::ReadDrawableBg2());
	bg::db::DrawableLoader::RegisterPlugin(new bg::db::plugin::ReadDrawableObj());
	bg::db::DrawableWriter::RegisterPlugin(new bg::db::plugin::WriteDrawableBg2());

	bg::db::FontLoader::RegisterPlugin(new bg::db::plugin::ReadFontTrueType());

	bg::db::NodeLoader::RegisterPlugin(new bg::db::plugin::ReadScene());
	bg::db::NodeLoader::RegisterPlugin(new bg::db::plugin::ReadPrefabBg2());
	bg::db::NodeWriter::RegisterPlugin(new bg::db::plugin::WritePrefabBg2());
	bg::db::NodeWriter::RegisterPlugin(new bg::db::plugin::WriteScene());

	bg::db::GuiSkinLoader::RegisterPlugin(new bg::db::plugin::ReadGuiSkinBg2());
}

void App::initActions() {

	// TODO: Register all action delegates
	userActions().addDelegate(new action::FileActions());
	userActions().addDelegate(new action::EditActions());
    userActions().addDelegate(new action::PhysicsActions());

	userActions().triggerDelegates();
}

}
