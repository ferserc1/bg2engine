/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */


#include <event_handler.hpp>

#include <bg/bg2e.hpp>

#include <app.hpp>

namespace composer {

EventHandler::EventHandler()
{
	
}

EventHandler::~EventHandler() {
	
}

void EventHandler::willCreateContext() {
	if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else if (bg::system::isDesktop()){
		throw bg::base::CompatibilityException("Fatal error: no suitable rendering engine found.");
	}
}

void EventHandler::initGL() {
	using namespace bg::scene;
	using namespace bg::math;
	bg::Engine::Get()->initialize(context());
	context()->setVsyncEnabled(true);

	std::vector<bg::system::Screen> screens = bg::system::Screen::ScreenList();
	for (auto & s : screens) {
		bg::log(bg::log::kDebug) << "Screen " << s.id() << ": "
			<< s.name() << ", res: " << s.size() << ", scale: " << s.scale() << bg::endl;
	}
   

    bg::system::Path path = bg::system::Path::AppDir();
    if (bg::system::currentPlatform()==bg::system::kMac) {
        path = bg::system::Path::ResourcesDir();
    }
	else {
		path.addComponent("data");
	}
	
	_inputVisitor = new InputVisitor();
	//_renderer = bg::render::Renderer::Create(context(), bg::render::Renderer::kRenderPathForward);

	_renderer = bg::render::Renderer::Create(context(), bg::render::Renderer::kRenderPathDeferred);
	// Render settings method 1: get the setting class (ShadowMap) and use the accessor
	bg::render::ShadowMap * shadowMap = _renderer->settings<bg::render::ShadowMap>();
	if (shadowMap) {
		shadowMap->setShadowType(bg::render::ShadowMap::kSoftShadows);
	}

	// Render settings method 2 (preferred): use de setRenderSetting() functions
	//	The settings keys are defined in bg::render::settings namespace, and they are
	//	bg::render::SettingsKey (aka std::string). The keys are defined in the settings
	//	namespace to reduce the risk of misspelling, but you can also specify the
	//	key as a string:  bg::render::settings::shadowMap == "shadowMap"
	using namespace bg::render::settings;
	_renderer->setRenderSetting(kAmbientOcclusion, kKernelSize, 32);
	_renderer->setRenderSetting(kRaytracer, kQuality, bg::base::kQualityMedium);
	_renderer->setRenderSetting(kShadowMap, kShadowMapSize, bg::math::Size2Di(4096));
	_renderer->setRenderSetting(kAmbientOcclusion, kBlur, 4);
	_renderer->setRenderSetting(kAmbientOcclusion, kSampleRadius, 0.14f);
	_renderer->setRenderSetting(kAmbientOcclusion, kQuality, bg::base::kQualityHigh);
	_renderer->setRenderSetting(kRaytracer, kEnabled, true);
	_renderer->setRenderSetting(kRaytracer, kClearOnViewChanged, false);
	_renderer->setRenderSetting(kRaytracer, kScale, 0.7f);

	App::Get().init(context());
}

void EventHandler::destroy() {
	App::Get().destroy();
}

void EventHandler::windowRectChanged(int x, int y, int w, int h) {
	App::Get().windowRectChanged(bg::math::Rect(x, y, w, h));
}

void EventHandler::reshape(int w, int h) {
	bg::math::Viewport vp(0, 0, w, h);
	App::Get().scene().mainCamera()->setViewport(vp);
	App::Get().workspace().resize(vp);
}

void EventHandler::frame(float delta) {
	_renderer->frame(App::Get().scene().sceneRoot(), delta);
	App::Get().workspace().frame(delta, this->scale());
}

void EventHandler::draw() {
	_renderer->draw(App::Get().scene().sceneRoot(), App::Get().scene().mainCamera());
	App::Get().workspace().draw();
	context()->swapBuffers();
}

void EventHandler::onMemoryWarning() {
	std::cout << "Memory warning received" << std::endl;
}

void EventHandler::buildMenu(bg::wnd::MenuDescriptor & menu) {
	for (auto m : App::Get().menuFactory().menu()) {
		menu.push_back(m);
	}
}

void EventHandler::menuSelected(const std::string & title, int32_t identifier) {
	App::Get().userActions().exec(identifier);
}

// Mobile touch events
void EventHandler::touchStart(const bg::base::TouchEvent & evt)  {
	if (!App::Get().workspace().touchStart(evt)) {
		_inputVisitor->touchStart(App::Get().scene().sceneRoot(), evt);
    }
}

void EventHandler::touchMove(const bg::base::TouchEvent & evt)  {
    if (!App::Get().workspace().touchMove(evt)) {
        _inputVisitor->touchMove(App::Get().scene().sceneRoot(), evt);
    }
}

void EventHandler::touchEnd(const bg::base::TouchEvent & evt) {
    if (!App::Get().workspace().touchEnd(evt)) {
        _inputVisitor->touchEnd(App::Get().scene().sceneRoot(), evt);
    }
}


// Desktop events
void EventHandler::keyUp(const bg::base::KeyboardEvent & evt) {
	if (!App::Get().workspace().keyUp(evt)) {
		_inputVisitor->keyUp(App::Get().scene().sceneRoot(), evt);
	}
}

void EventHandler::keyDown(const bg::base::KeyboardEvent & evt) {
	if (!App::Get().workspace().keyDown(evt)) {
		_inputVisitor->keyDown(App::Get().scene().sceneRoot(), evt);
	}
}

void EventHandler::charPress(const bg::base::KeyboardEvent & evt) {
	if (!App::Get().workspace().charPress(evt)) {
		_inputVisitor->charPress(App::Get().scene().sceneRoot(), evt);
	}
}

void EventHandler::mouseDown(const bg::base::MouseEvent & evt) {
	if (!App::Get().workspace().mouseDown(evt)) {
		_inputVisitor->mouseDown(App::Get().scene().sceneRoot(), evt);
	}
}

void EventHandler::mouseMove(const bg::base::MouseEvent & evt) {
	if (!App::Get().workspace().mouseMove(evt)) {
		_inputVisitor->mouseMove(App::Get().scene().sceneRoot(), evt);
	}
}

void EventHandler::mouseDrag(const bg::base::MouseEvent & evt) {
	if (!App::Get().workspace().mouseDrag(evt)) {
		_inputVisitor->mouseDrag(App::Get().scene().sceneRoot(), evt);
	}
}

void EventHandler::mouseWheel(const bg::base::MouseEvent & evt) {
	if (!App::Get().workspace().mouseWheel(evt)) {
		_inputVisitor->mouseWheel(App::Get().scene().sceneRoot(), evt);
	}
}

void EventHandler::mouseUp(const bg::base::MouseEvent & evt) {
	App::Get().workspace().mouseUp(evt);
}


}
