
#import "ViewController.h"

#include "event_handler.hpp"

@implementation ViewController

- (bg::base::EventHandler*)createEventHandler {
	MyEventHandler * eh = new MyEventHandler();
	eh->inc_ref();
	return eh;
}

@end
