/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

//
//  event-handler.cpp
//  bg2e-ios
//
//  Created by Fernando Serrano Carpena on 3/5/16.
//  Copyright © 2016 Fernando Serrano Carpena. All rights reserved.
//

#include <event_handler.hpp>


MyEventHandler::MyEventHandler()
{
	
}

MyEventHandler::~MyEventHandler() {
	
}

void MyEventHandler::willCreateContext() {
	if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else if (bg::system::isDesktop()){
		throw bg::base::CompatibilityException("Fatal error: no suitable rendering engine found.");
	}
}

void MyEventHandler::initGL() {
	bg::Engine::Get()->initialize(context());
	
	using namespace bg::scene;

	bg::db::NodeLoader::RegisterPlugin(new bg::db::plugin::ReadPrefabBg2());
	bg::db::NodeLoader::RegisterPlugin(new bg::db::plugin::ReadScene());
	bg::db::NodeWriter::RegisterPlugin(new bg::db::plugin::WriteScene());

	_inputVisitor = new InputVisitor();
	
	bg::system::Path path = bg::system::Path::AppDir();

	// Load a scene, store again in disk, destroy it and load it from the new copy.
	_sceneRoot = bg::db::loadScene(context(), path.pathAddingComponent("data/sample-scene/scene.vitscnj"));
    /*
    bg::ptr<Node> scene1 = bg::db::loadScene(context(), path.pathAddingComponent("data/sample-scene/scene.vitscnj"));
	bg::system::Path outPath;
	outPath = bg::system::Path::HomeDir().pathAddingComponent("Desktop/test/test-out.vitbundle");
	bg::db::writeScene(context(), outPath, scene1.getPtr());

	scene1 = nullptr;	// This will clear the scene automatically using the reference couter pointer

	// Load scene again
	_sceneRoot = bg::db::loadScene(context(), outPath);
     */

	bg::ptr<FindComponentVisitor<Camera>> findCamera = new FindComponentVisitor<Camera>();
	_sceneRoot->accept(findCamera.getPtr());
	if (findCamera->result().size()>0) {
		_camera = findCamera->result().front();
        _camera->node()->addComponent(new bg::manipulation::OrbitNodeController());
	}
	bg::ptr<FindComponentVisitor<Light>> findLight = new FindComponentVisitor<Light>();
	_sceneRoot->accept(findLight.getPtr());
	for (auto l : findLight->result()) {
		// Optimize the shadow bias to match the renderer best value
		l->light()->setShadowBias(0.00001f);
	}

	_renderer = bg::render::Renderer::Create(context(), bg::render::Renderer::kRenderPathDeferred);
	
	_renderer->setRenderSetting(bg::render::settings::kShadowMap,
								bg::render::settings::kShadowMapSize,
								bg::math::Size2Di(2048));
    _renderer->setRenderSetting(bg::render::settings::kRaytracer, bg::render::settings::kScale, 0.7f);
}

void MyEventHandler::destroy() {
	// IMPORTANT: Remove the tmp directory when the application finishes
	bg::system::Path::TempDir().remove();
}

void MyEventHandler::reshape(int w, int h) {
    if (!_camera->projectionStrategy()) {
        _camera->setProjectionStrategy(new bg::scene::OpticalProjectionStrategy());
    }
	_camera->setViewport(bg::math::Viewport(0,0,w,h));
}

void MyEventHandler::frame(float delta) {
	_renderer->frame(_sceneRoot.getPtr(), delta);
}

void MyEventHandler::draw() {
	_renderer->draw(_sceneRoot.getPtr(), _camera.getPtr());
	
	context()->swapBuffers();
}

void MyEventHandler::onMemoryWarning() {
	std::cout << "Memory warning received" << std::endl;
}

// Mobile touch events
void MyEventHandler::touchStart(const bg::base::TouchEvent & evt)  {
	_inputVisitor->touchStart(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::touchMove(const bg::base::TouchEvent & evt)  {
	_inputVisitor->touchMove(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::touchEnd(const bg::base::TouchEvent & evt) {
	_inputVisitor->touchEnd(_sceneRoot.getPtr(), evt);
}


// Desktop events
void MyEventHandler::keyUp(const bg::base::KeyboardEvent & evt) {
    if (evt.keyboard().key() >= bg::base::Keyboard::kKey0 && evt.keyboard().key() <= bg::base::Keyboard::kKey9) {
        int inc = evt.keyboard().key() - bg::base::Keyboard::kKey0;
        if (inc==0) inc = 10;
        _renderer->setRenderSetting(bg::render::settings::kRaytracer, bg::render::settings::kScale, static_cast<float>(inc) / 10.0f);
    }
	if (evt.keyboard().key() == bg::base::Keyboard::kKeyEsc) {
		bg::wnd::MainLoop::Get()->quit(0);
	}
}

void MyEventHandler::mouseDown(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseDown(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::mouseDrag(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseDrag(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::mouseWheel(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseWheel(_sceneRoot.getPtr(), evt);
}
