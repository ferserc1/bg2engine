/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <scene/scene.hpp>

#include <bg/db/db.hpp>
#include <bg/scene/scene.hpp>
#include <bg/manipulation/manipulation.hpp>
#include <bg/math/math.hpp>

#include <app.hpp>

#include <bg/wnd/message_box.hpp>
#include <bg/wnd/main_loop.hpp>

#include <command/file_commands.hpp>

#include <bg/tools/uuid.hpp>

namespace composer {
namespace scene {

Scene::Scene(bg::base::Context *ctx)
	:ContextObject(ctx)
{
}

Scene::~Scene() {

}

void Scene::newScene() {
	std::string sceneName = App::Get().config().getString("defaultScene", "default.vitscn");
	bg::system::Path scenePath(sceneName);
	if (!scenePath.isAbsolute()) {
		scenePath = bg::system::Path::ExecDir().pathAddingComponent(sceneName);
	}

	try {
		_sceneRoot = bg::db::loadScene(context(), scenePath);
	}
	catch (std::runtime_error & err) {
		createEmptyScene();
	}

	checkCameraPresent(sceneRoot());
	checkLightPresent(sceneRoot());

	App::Get().commandManager().cleanUp();
}

void Scene::open(const std::string & path) {
	try {
		bg::ptr<bg::scene::Node> newSceneNode = bg::db::loadScene(context(), path);
		_sceneRoot = newSceneNode.getPtr();
		_currentScenePath = path;
		checkCameraPresent(_sceneRoot.getPtr());
		checkLightPresent(_sceneRoot.getPtr());
		App::Get().commandManager().cleanUp();
	}
	catch (std::runtime_error & err) {
		bg::wnd::MessageBox::Show(bg::wnd::MainLoop::Get()->window(), "Error opening scene", "Could not open scene " + path + "\n" + err.what());
	}
}

void Scene::import (const std::string & path) {
	auto cmd = new command::ImportCommand(context(), path, sceneRoot());
	cmd->onImportDrawable([&](bg::scene::Drawable * drw, bg::scene::Node * node) {
		bg::system::Path filePath(path);
		drw->setName(bg::tools::uuid());
		node->setName(filePath.fileName());
	});
	App::Get().commandManager().executeCommand(cmd);
}

void Scene::close() {
	_sceneRoot = nullptr;
	App::Get().commandManager().cleanUp();
}

void Scene::destroy() {
	close();
	_mainCamera = nullptr;
	_mainLight = nullptr;
}

void Scene::createEmptyScene() {
	_sceneRoot = new bg::scene::Node(context());
}

void Scene::checkCameraPresent(bg::scene::Node * root) {
	using namespace bg::scene;
	bg::ptr<FindComponentVisitor<Camera>> findCamera = new FindComponentVisitor<Camera>();
	root->accept(findCamera.getPtr());
	bg::math::Viewport vp;

	if (_mainCamera.valid()) {
		vp = _mainCamera->viewport();
	}

	if (findCamera->result().size() == 0) {
		_mainCamera = new Camera();
		Node * cameraNode = new Node(context());
		cameraNode->addComponent(_mainCamera.getPtr());
		root->addChild(cameraNode);
	}
	else {
		_mainCamera = findCamera->result().front();
	}

	_mainCamera->node()->addComponent(new Transform());
	OpticalProjectionStrategy * proj = new OpticalProjectionStrategy();
	_mainCamera->setProjectionStrategy(proj);

	_mainCamera->node()->addComponent(new bg::manipulation::OrbitNodeController());
	_mainCamera->setViewport(vp);
}

void Scene::checkLightPresent(bg::scene::Node * root) {
	using namespace bg::scene;
	using namespace bg::math;
	bg::ptr<FindComponentVisitor<Light>> findLight = new FindComponentVisitor<Light>();
	root->accept(findLight.getPtr());

	if (findLight->result().size() == 0) {
		_mainLight = new Light();
		Node * lightNode = new Node(context());
		lightNode->addComponent(_mainLight.getPtr());
		Matrix4 lightMatrix;
		lightMatrix.identity()
			.rotate(Scalar(75, trigonometry::deg), -1.0, 0.0, 0.0)
			.rotate(Scalar(60, trigonometry::deg), 0.0f, 1.0f, 0.0f);
		lightNode->addComponent(new Transform(lightMatrix));
		root->addChild(lightNode);
	}
	else {
		_mainLight = findLight->result().front();
	}
}

}
}
