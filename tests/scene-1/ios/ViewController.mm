
#import "ViewController.h"

#include "event_handler.hpp"

@implementation ViewController

- (bg::base::EventHandler*)createEventHandler {
	return new MyEventHandler();
}

@end
