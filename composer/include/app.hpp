/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */


#ifndef _composer_app_hpp_
#define _composer_app_hpp_

#include <command/manager.hpp>

#include <scene/scene.hpp>

#include <ui/workspace.hpp>

#include <ui/menu_factory.hpp>

#include <ui/action.hpp>

#include <bg/base/context.hpp>

#include <config.hpp>

namespace composer {

class App {
public:
	static App & Get() { return s_app; }
	
	int run();

	void init(bg::base::Context *);
	void destroy();

	inline ui::Workspace & workspace() { return *_workspace; }
	inline const ui::Workspace & workspace() const { return *_workspace; }
	inline ui::MenuFactory & menuFactory() { return _menuFactory; }
	inline const ui::MenuFactory & menuFactory() const { return _menuFactory; }
	inline ui::UserActions & userActions() { return _userActions; }
	inline const ui::UserActions & userActions() const { return _userActions; }

	inline bool sceneAvailable() { return _scene.valid(); }
	inline scene::Scene & scene() { return *_scene.getPtr(); }

	inline command::Manager & commandManager() { return _commandManager; }

	inline Config & config() { return *_mainConfig; }

	inline bg::base::Context * context() { return _ctx.getPtr(); }

	void windowRectChanged(const bg::math::Rect &);
	
protected:
	App();
	virtual ~App();
	
	ui::Workspace * _workspace;
	ui::MenuFactory _menuFactory;
	ui::UserActions _userActions;
	bg::ptr<scene::Scene> _scene;

	command::Manager _commandManager;

	static App s_app;

	Config * _mainConfig;
	bg::ptr<bg::base::Context> _ctx;

	void initActions();

	void loadPlugins();
};

}

#endif
