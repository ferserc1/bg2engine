/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef _composer_scene_selection_hpp_
#define _composer_scene_selection_hpp_

#include <bg/manipulation/mouse_picker.hpp>
#include <bg/manipulation/selectable.hpp>

namespace composer {
namespace scene {

class Selection;

class SelectionStrategy : public bg::base::ReferencedPointer {
public:
	SelectionStrategy() {}

	virtual void itemPicked(Selection *, bg::manipulation::SelectableItem *) = 0;

protected:
	virtual ~SelectionStrategy() {}
};

class Selection : public bg::base::ContextObject, public bg::base::ReferencedPointer {
public:
	Selection(bg::base::Context *);

	inline void setSelectionStrategy(SelectionStrategy * sel) { _selectionStrategy = sel; }

	void selectionStart(bg::scene::Node *, bg::scene::Camera *, const bg::math::Position2Di & pos);
	void selectionEnd(bg::scene::Node *, bg::scene::Camera *, const bg::math::Position2Di & pos);

	inline const std::vector<bg::ptr<bg::manipulation::SelectableItem>> selection() const { return _selection; }
	inline std::vector<bg::ptr<bg::manipulation::SelectableItem>> selection() { return _selection; }


protected:
	virtual ~Selection();

	bg::ptr<bg::manipulation::MousePicker> _mousePicker;
	std::vector<bg::ptr<bg::manipulation::SelectableItem>> _selection;
	bg::ptr<SelectionStrategy> _selectionStrategy;

	bg::math::Position2Di _startPoint;

	void itemPicked(bg::manipulation::SelectableItem *);
};

}
}
#endif

