echo off

SET platform=win64
SET bg2dir=%cd%
SET outdir=%cd%\..\bg2e-%platform%
SET bin=%bg2dir%\build\bin
SET include=%bg2dir%\include
SET includeout=%outdir%\include
SET libout=%outdir%\lib\%platform%
SET binout=%outdir%\bin\%platform%

if not exist "%includeout%" mkdir "%includeout%"
if not exist "%libout%" mkdir "%libout%"
if not exist "%binout%" mkdir "%binout%"

xcopy "%include%" "%includeout%" /s /e
copy "%bin%\*.lib" "%libout%"
copy "%bin%\*.dll" "%binout%"
REM copy "%bin%\*.exe" "%binout%"
copy "%bg2dir%\LICENSE.txt" "%outdir%\LICENSE.txt"
copy "%bg2dir%\THIRDPARTY.txt" "%outdir%\THIRDPARTY.txt"
REM xcopy "%bin%\data\*.*" "%binout%\data" /s /e
