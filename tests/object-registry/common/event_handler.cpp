/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

//
//  event-handler.cpp
//  bg2e-ios
//
//  Created by Fernando Serrano Carpena on 3/5/16.
//  Copyright © 2016 Fernando Serrano Carpena. All rights reserved.
//

#include <event_handler.hpp>

#include <bg/bg2e.hpp>

class MyComponent : public bg::scene::Component {
public:
	MyComponent() {}

	bg::scene::Component * clone() {
		return new MyComponent();
	}
	
	void init() {
		using namespace bg::scene;
		Node * cube = bg::base::ObjectRegistry::Get().findObjectOfType<Node>("cube");
		Node * sphere = bg::base::ObjectRegistry::Get().findObjectOfType<Node>("sphere");
		Node * floor = bg::base::ObjectRegistry::Get().findObjectOfType<Node>("floor");
		Drawable * drw = nullptr;
	
		if (cube && (drw=cube->drawable())) {
			drw->material(0)->setDiffuse(bg::math::Color::Red());
			drw->material(0)->setShininess(22.0f);
		}
		
		if (sphere && (drw=sphere->drawable())) {
			drw->material(0)->setDiffuse(bg::math::Color::Green());
			drw->material(0)->setShininess(12.0f);
		}
		
		if (floor && (drw=floor->drawable())) {
			drw->material(0)->setDiffuse(bg::math::Color::Blue());
			drw->material(0)->setShininess(40.0f);
		}
	}

protected:
	virtual ~MyComponent() {}
};

MyEventHandler::MyEventHandler()
	:_inputVisitor(new bg::scene::InputVisitor())
{
	
}

MyEventHandler::~MyEventHandler() {
	
}

void MyEventHandler::willCreateContext() {
	if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else if (bg::system::isDesktop()){
		throw bg::base::CompatibilityException("Fatal error: no suitable rendering engine found.");
	}
}

void MyEventHandler::initGL() {
	bg::Engine::Get()->initialize(context());
	
	_renderer = bg::render::Renderer::Create(context(), bg::render::Renderer::kRenderPathDeferred);
	
	bg::render::ShadowMap * shadowMap = _renderer->settings<bg::render::ShadowMap>();
	if (shadowMap) {
		shadowMap->setShadowMapSize(bg::math::Size2Di(2048));
		shadowMap->setShadowType(bg::render::ShadowMap::kSoftShadows);
	}
	
	// Create scene
	{
		using namespace bg::scene;
		using namespace bg::math;
		using namespace bg::math::trigonometry;
		_sceneRoot = new Node(context());
		_sceneRoot->setId("scene-root");
		_sceneRoot->addComponent(new MyComponent());
		
		Node * camera = new Node(context());
		camera->setId("main-camera");
		
		Camera * cameraComponent = new Camera();
		OpticalProjectionStrategy * projectionStrategy = new OpticalProjectionStrategy();
		projectionStrategy->setFrameSize(35.0f);
		projectionStrategy->setFocalLength(50.0f);
		projectionStrategy->setNear(0.1f);
		projectionStrategy->setFar(100.0f);
		cameraComponent->setProjectionStrategy(projectionStrategy);
		
		camera->addComponent(cameraComponent);
		camera->addComponent(new Transform());
		camera->addComponent(new bg::manipulation::OrbitNodeController());
		
		_camera = camera->component<Camera>();
		_sceneRoot->addChild(camera);
		
		Node * light = new Node(context());
		light->setId("main-light");
		
		light->addComponent(new Light());
		light->addComponent(new Transform(Matrix4::Rotation(degreesToRadians(45.0f), -1.0f, 0.0f, 0.0f)
										  .rotate(degreesToRadians(60.0f), 0.0f, 1.0f, 0.0f)
										  .translate(0.0f, 0.0f, -10.0f)));
		
		_sceneRoot->addChild(light);
		
		PrimitiveFactory factory(context());
		
		Node * cube = new Node(context());
		cube->setId("cube");
		cube->addComponent(factory.cube());
		cube->addComponent(new Transform(Matrix4::Translation(1.5f, 0.0f, 0.0f)));
		_sceneRoot->addChild(cube);
		
		Node * sphere = new Node(context());
		sphere->setId("sphere");
		sphere->addComponent(factory.sphere());
		sphere->addComponent(new Transform(Matrix4::Translation(-1.5f, 0.0f, 0.0f)));
		_sceneRoot->addChild(sphere);
		
		Node * floor = new Node(context());
		floor->setId("floor");
		floor->addComponent(factory.plane(10.0f));
		floor->addComponent(new Transform(Matrix4::Translation(0.0f, -0.7f, 0.0f)));
		_sceneRoot->addChild(floor);
	}
}

void MyEventHandler::destroy() {
	
}

void MyEventHandler::reshape(int w, int h) {
	_camera->setViewport(bg::math::Viewport(0,0,w,h));
}

void MyEventHandler::frame(float delta) {
	_renderer->frame(_sceneRoot.getPtr(), delta);
}

void MyEventHandler::draw() {
	_renderer->draw(_sceneRoot.getPtr(), _camera.getPtr());
	context()->swapBuffers();
}

void MyEventHandler::onMemoryWarning() {
	std::cout << "Memory warning received" << std::endl;
}

// Mobile touch events
void MyEventHandler::touchStart(const bg::base::TouchEvent & evt)  {
	_inputVisitor->touchStart(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::touchMove(const bg::base::TouchEvent & evt)  {
	_inputVisitor->touchMove(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::touchEnd(const bg::base::TouchEvent & evt) {
	_inputVisitor->touchEnd(_sceneRoot.getPtr(), evt);
}


// Desktop events
void MyEventHandler::keyUp(const bg::base::KeyboardEvent & evt) {
	if (evt.keyboard().key() == bg::base::Keyboard::kKeyEsc) {
		bg::wnd::MainLoop::Get()->quit(0);
	}
}

void MyEventHandler::mouseDown(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseDown(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::mouseDrag(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseDrag(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::mouseUp(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseUp(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::mouseMove(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseMove(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::mouseWheel(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseWheel(_sceneRoot.getPtr(), evt);
}
