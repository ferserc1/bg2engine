/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <scene.hpp>

#include <bg/db/db.hpp>
#include <bg/scene/scene.hpp>
#include <bg/scene/find_component_visitor.hpp>
#include <bg/manipulation/manipulation.hpp>
#include <bg/math/math.hpp>

#include <app.hpp>

#include <bg/wnd/message_box.hpp>
#include <bg/wnd/main_loop.hpp>

#include <bg/tools/uuid.hpp>

namespace viewer {

Scene::Scene(bg::base::Context *ctx)
	:ContextObject(ctx)
{
	_sceneRoot = new bg::scene::Node(ctx);
	_mainCamera = new bg::scene::Camera();
	_sceneRoot->addComponent(_mainCamera.getPtr());
}

Scene::~Scene() {
	_cameras.clear();
	_mainCamera = nullptr;
}

void Scene::open(const std::string & path) {
	try {
		_cameras.clear();
		bg::ptr<bg::scene::Node> newSceneNode = bg::db::loadScene(context(), path);
		bg::ptr<bg::scene::FindComponentVisitor<bg::scene::Camera>> findCamera = new bg::scene::FindComponentVisitor<bg::scene::Camera>();
		_sceneRoot = newSceneNode.getPtr();
		_currentScenePath = path;
		_sceneRoot->accept(findCamera.getPtr());
		for (auto cam : findCamera->result()) {
			_cameras.push_back(cam);
            if (!cam->projectionStrategy()) {
                auto strategy = new bg::scene::OpticalProjectionStrategy();
                strategy->setNear(0.5f);
                strategy->setFar(1000.0f);
                cam->setProjectionStrategy(strategy);
            }
		}

		if (_cameras.size() > 0) {
			_mainCamera = _cameras[0];
		}
		else {
			_mainCamera = nullptr;
		}
	}
	catch (std::runtime_error & err) {
		bg::wnd::MessageBox::Show(bg::wnd::MainLoop::Get()->window(), "Error opening scene", "Could not open scene " + path + "\n" + err.what());
	}
}

void Scene::setMainCamera(size_t index) {
	if (_cameras.size() > index) {
		_mainCamera = _cameras[index];
	}
}

}
