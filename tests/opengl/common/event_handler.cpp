/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

//
//  event-handler.cpp
//  bg2e-ios
//
//  Created by Fernando Serrano Carpena on 3/5/16.
//  Copyright © 2016 Fernando Serrano Carpena. All rights reserved.
//

#include "event_handler.hpp"

#include <bg/bg2e.hpp>

bg::engine::openglCore::OpenGLState * MyEventHandler::gl() {
	using namespace bg::engine::openglCore;
	
	// The OpenGL and OpenGL ES context objects extends GLStateObject
	GLStateObject * stateObject = dynamic_cast<GLStateObject*>(context());
	return stateObject ? stateObject->glState() : nullptr;
}

MyEventHandler::MyEventHandler()
{
	
}

MyEventHandler::~MyEventHandler() {
	
}

void MyEventHandler::willCreateContext() {
	if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else if (bg::system::isDesktop()){
		throw bg::base::CompatibilityException("Fatal error: no suitable rendering engine found.");
	}
}

void MyEventHandler::initGL() {
	using namespace bg::engine;
	bg::Engine::Get()->initialize(context());
	
	
	gl()->enableDepthTest();
}

void MyEventHandler::willDestroyContext() {
	EventHandler::willDestroyContext();
}

void MyEventHandler::reshape(int w, int h) {
	gl()->viewport(bg::math::Viewport(0,0,w,h));
}

void MyEventHandler::draw() {
	using namespace bg::engine;
	gl()->clear(opengl::kColorBufferBit | opengl::kDepthBufferBit);
	context()->swapBuffers();
}

void MyEventHandler::onMemoryWarning() {
	std::cout << "Memory warning received" << std::endl;
}

void MyEventHandler::setColorDiff(float diff, int channel) {
	bg::math::Color clearColor = gl()->currentclearcolor();
	switch (channel) {
		case 0:
			clearColor.r(clearColor.r() + diff);
			break;
		case 1:
			clearColor.g(clearColor.g() + diff);
			break;
		case 2:
			clearColor.b(clearColor.b() + diff);
			break;
	}
	clearColor.clamp(0.0f,1.0f);
	gl()->clearColor(clearColor);
}

// Mobile touch events
void MyEventHandler::touchStart(const bg::base::TouchEvent & evt)  {
	_startPos = *evt.touches().front();
}

void MyEventHandler::touchMove(const bg::base::TouchEvent & evt)  {
	bg::math::Position2Di pos = *evt.touches().front();
	float diffX = (pos.x() - _startPos.x()) * 0.001f;
	float diffY = (_startPos.y() - pos.y()) * 0.001f;
	float diff = bg::math::abs(diffX)>bg::math::abs(diffY) ? diffX:diffY;
	
	setColorDiff(diff, static_cast<int>(evt.touches().size() - 1));
	_startPos = pos;
}

void MyEventHandler::touchEnd(const bg::base::TouchEvent & evt) {
}


// Desktop events
void MyEventHandler::keyUp(const bg::base::KeyboardEvent & evt) {
	if (evt.keyboard().key()==bg::base::Keyboard::kKeyEsc) {
		bg::wnd::MainLoop::Get()->quit(0);
	}
}

void MyEventHandler::mouseDown(const bg::base::MouseEvent & evt) {
	_startPos = evt.pos();
}

void MyEventHandler::mouseDrag(const bg::base::MouseEvent & evt) {
	float diffX = (evt.pos().x() - _startPos.x()) * 0.001f;
	float diffY = (_startPos.y() - evt.pos().y()) * 0.001f;
	float diff = bg::math::abs(diffX)>bg::math::abs(diffY) ? diffX:diffY;
	int channel = (evt.mouse().buttonMask() & bg::base::Mouse::kLeftButton)	  ? 0 :
				  (evt.mouse().buttonMask() & bg::base::Mouse::kMiddleButton) ? 1 :
				  (evt.mouse().buttonMask() & bg::base::Mouse::kRightButton)  ? 2:-1;
	setColorDiff(diff, channel);
	_startPos = evt.pos();
}

