/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

//
//  event-handler.cpp
//  bg2e-ios
//
//  Created by Fernando Serrano Carpena on 3/5/16.
//  Copyright © 2016 Fernando Serrano Carpena. All rights reserved.
//

#include "event_handler.hpp"

#include <bg/engine.hpp>
#include <bg/wnd/main_loop.hpp>

#include <bg/system/system.hpp>

#include <bg/base/matrix_state.hpp>
#include <bg/base/effect.hpp>
#include <bg/base/image.hpp>
#include <bg/base/texture.hpp>

#include <bg/system/path.hpp>

#include <bg/engine/opengl_es_3.hpp>
#include <bg/engine/opengl_core.hpp>
#include <bg/engine/directx11.hpp>

#include <bg/engine/directx11/shader.hpp>
#include <bg/engine/openglCore/shader.hpp>
#include <bg/engine/openglEs/shader.hpp>
#include <bg/engine/openglCore/poly_list_impl.hpp>

class MyEffect : public bg::base::Effect {
public:
	MyEffect(bg::base::Context * ctx)
		:bg::base::Effect(ctx)
	{
		if (bg::Engine::Get()->identifier() == bg::Engine::Identifier<bg::engine::DirectX11>()) {
			buildDirectX();
		}
		else if (bg::Engine::Get()->identifier() == bg::Engine::Identifier<bg::engine::OpenGLCore>()) {
			buildOpenGL(false);
		}
		else if (bg::Engine::Get()->identifier() == bg::Engine::Identifier<bg::engine::OpenGLES3>()) {
			buildOpenGL(true);
		}
	}

	virtual void activate() {
		if (bg::Engine::Get()->identifier() == bg::Engine::Identifier<bg::engine::DirectX11>()) {
			using namespace bg::engine::directx11;
			ConstantBuffer * matrixBuffer = _dxVertexShader->constantBuffers().front().getPtr();
			
			MatrixBuffer * dataPtr = matrixBuffer->beginMap<MatrixBuffer>();
			dataPtr->modelMatrix = matrixState().modelMatrixStack().matrix();
			dataPtr->viewMatrix = matrixState().viewMatrixStack().matrix();
			dataPtr->projectionMatrix = matrixState().projectionMatrixStack().matrix();
			matrixBuffer->endMap(0);
			
			bg::engine::directx11::ConstantBuffer * pixelBuffer = _dxPixelShader->constantBuffers().front().getPtr();
			
			PixelBuffer * pixelPtr = pixelBuffer->beginMap<PixelBuffer>();
			pixelPtr->tint = material().diffuse();
			pixelBuffer->endMap(0);

			_dxPixelShader->setTexture(0, 1, material().texture());
			
			_dxVertexShader->activate();
			_dxPixelShader->activate();
		}
		if (bg::Engine::Get()->identifier() == bg::Engine::Identifier<bg::engine::OpenGLCore>() ||
			bg::Engine::Get()->identifier() == bg::Engine::Identifier<bg::engine::OpenGLES3>())
		{
			_glShader->setActive();
			
			_glShader->setUniform("inModelMatrix", matrixState().modelMatrixStack().matrix());
			_glShader->setUniform("inViewMatrix", matrixState().viewMatrixStack().matrix());
			_glShader->setUniform("inProjectionMatrix", matrixState().projectionMatrixStack().matrix());
			_glShader->setUniform("inTint", material().diffuse());
			_glShader->setUniform("inTexture", material().texture(), 0);
		}
	}

	virtual void bindPolyList(bg::base::PolyList * plist) {
		if (bg::Engine::Get()->identifier() == bg::Engine::Identifier<bg::engine::DirectX11>()) {
			_dxVertexShader->bindPolyList(plist);
		}
		else if (bg::Engine::Get()->identifier() == bg::Engine::Identifier<bg::engine::OpenGLCore>() ||
			bg::Engine::Get()->identifier() == bg::Engine::Identifier<bg::engine::OpenGLES3>())
		{
			_glShader->bindPolyList(plist);
		}
	}

	virtual void unbind() {
		if (bg::Engine::Get()->identifier() == bg::Engine::Identifier<bg::engine::OpenGLCore>() ||
			bg::Engine::Get()->identifier() == bg::Engine::Identifier<bg::engine::OpenGLES3>())
		{
			_glShader->clearActive();
			_glShader->unbindPolyList();
		}
	}

protected:
	

	struct MatrixBuffer {
		bg::math::Matrix4 modelMatrix;
		bg::math::Matrix4 viewMatrix;
		bg::math::Matrix4 projectionMatrix;
	};
	
	struct PixelBuffer {
		bg::math::Color tint;
	};

	void buildDirectX() {
		using namespace bg::engine::directx11;
		std::string vshader;
		std::string pshader;
		
		getDXVertexShader(vshader);
		getDXPixelShader(pshader);

		_dxVertexShader = new VertexShader(context());
		_dxVertexShader->addInputLayout(BufferRole::kRoleVertex);
		_dxVertexShader->addInputLayout(BufferRole::kRoleColor);
		_dxVertexShader->addInputLayout(BufferRole::kRoleTex0);
		_dxVertexShader->addConstantBuffer(new ConstantBuffer(context(), sizeof(MatrixBuffer), BufferUsage::kUsageDynamic));
		_dxVertexShader->create(vshader, "BasicVertexShader");

		_dxPixelShader = new PixelShader(context());
		_dxPixelShader->addConstantBuffer(new ConstantBuffer(context(), sizeof(PixelBuffer), BufferUsage::kUsageDynamic));

		_dxPixelShader->create(pshader, "BasicPixelShader");
	}

	void buildOpenGL(bool mobile) {
		std::string vshader;
		std::string fshader;
		getGlVertextShader(mobile, vshader);
		getGlFragmentShader(mobile, fshader);
		_glShader = mobile ? new bg::engine::openglEs::Shader(context()) :
							 new bg::engine::openglCore::Shader(context());
		
		try {
			_glShader->attachShader(bg::engine::openglCore::kVertexShader, vshader);
			_glShader->attachShader(bg::engine::openglCore::kFragmentShader, fshader);
			_glShader->link();
			_glShader->setOutputParameterName(bg::engine::openglCore::ShaderParamType::kShaderOutFragmentLocation, "fs_outColor");
			
			_glShader->addVertexBufferInput("inPosition", bg::engine::openglCore::VertexBufferRole::kRoleVertex);
			_glShader->addVertexBufferInput("inColor", bg::engine::openglCore::VertexBufferRole::kRoleColor);
			_glShader->addVertexBufferInput("inTex0", bg::engine::openglCore::VertexBufferRole::kRoleTex0);
			_glShader->initUniforms({ "inModelMatrix", "inViewMatrix", "inProjectionMatrix", "inTint", "inTexture" });
		}
		catch (bg::base::ShaderException & e) {
			std::cerr << e.what() << std::endl;
			bg::wnd::MainLoop::Get()->quit(-1);
		}
		
	}

	bg::ptr<bg::engine::directx11::VertexShader> _dxVertexShader;
	bg::ptr<bg::engine::directx11::PixelShader> _dxPixelShader;

	// openglEs::Shader inherits from openglCore::Shader, so we can store it in the
	// same pointer
	bg::ptr<bg::engine::openglCore::Shader> _glShader;

	void getDXVertexShader(std::string & source) {
		source = "\
			cbuffer MatrixBuffer {\n\
				matrix worldMatrix;\n\
				matrix viewMatrix;\n\
				matrix projectionMatrix;\n\
			};\n\
			\n\
			struct VertexInputType {\n\
				float4 position : POSITION;\n\
				float4 color : COLOR;\n\
				float2 tex0 : TEXCOORD0;\n\
			};\n\
			\n\
			struct PixelInputType {\n\
				float4 position: SV_POSITION;\n\
				float4 color: COLOR;\n\
				float2 tex0: TEXCOORD0;\n\
			};\n\
			\n\
			PixelInputType BasicVertexShader(VertexInputType input) {\n\
				PixelInputType output;\n\
				input.position.w = 1.0f;\n\
				output.position = mul(mul(projectionMatrix, mul(viewMatrix, worldMatrix)), input.position);\n\
				//output.position = mul(input.position, worldMatrix);\n\
				//output.position = mul(output.position, viewMatrix);\n\
				//output.position = mul(output.position, projectionMatrix);\n\
				\n\
				output.color = input.color;\n\
				output.tex0 = input.tex0;\n\
				return output;\n\
			}";
	}
	
	void getDXPixelShader(std::string & source) {
		source = "\
			Texture2D shaderTexture;\n\
			SamplerState sampleType;\n\
			\n\
			cbuffer PixelBuffer {\n\
				float4 tint;\n\
			};\n\
			\n\
			struct PixelInputType {\n\
				float4 position : SV_POSITION;\n\
				float4 color : COLOR;\n\
				float2 tex0 : TEXCOORD0;\n\
			};\n\
			\n\
			float4 BasicPixelShader(PixelInputType input) : SV_TARGET {\n\
				return input.color * shaderTexture.Sample(sampleType, input.tex0) * tint;\n\
			}";
	}
	
	void getGlVertextShader(bool mobile, std::string & source) {
		if (mobile) {
			source = "#version 300 es\n";
		}
		else {
			source = "#version 330\n";
		}
		source += "\
			in vec3 inPosition;\n\
			in vec4 inColor;\n\
			in vec2 inTex0;\n\
			\n\
			uniform mat4 inModelMatrix;\n\
			uniform mat4 inViewMatrix;\n\
			uniform mat4 inProjectionMatrix;\n\
			\n\
			out vec4 fsColor;\n\
			out vec2 fsTexCoord;\n\
			\n\
			void main() {\n\
				gl_Position = inProjectionMatrix * inViewMatrix * inModelMatrix * vec4(inPosition,1.0);\n\
				fsColor = inColor;\n\
				fsTexCoord = inTex0;\n\
			}";
	}
	
	void getGlFragmentShader(bool mobile, std::string & source) {
		if (mobile) {
			source = "#version 300 es\n\
			precision highp float;\n\
			out mediump vec4 fs_outColor;\n";
		}
		else {
			source = "#version 330\n\
			layout (location = 0) out vec4 fs_outColor;\n";
		}
		source += "\
			in vec4 fsColor;\n\
			in vec2 fsTexCoord;\n\
			uniform vec4 inTint;\n\
			uniform sampler2D inTexture;\n\
			\n\
			void main() {\n\
				fs_outColor = fsColor * texture(inTexture,fsTexCoord) * inTint;\n\
			}";
	}
};

MyEventHandler::MyEventHandler()
{
	
}

MyEventHandler::~MyEventHandler() {
}

void MyEventHandler::willCreateContext() {
	//	NOTE:
	//		* On iOS platform, the engine is setup automatically, and will use
	//		  Metal or OpenGL ES depending on the view controller that you use.
	//		* On Android platform, the engine is set automatically to OpenGLES3

	if (bg::engine::DirectX11::Supported()) {
		bg::Engine::Init(new bg::engine::DirectX11());
	}
	else if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else if (bg::system::isDesktop()){
		throw bg::base::CompatibilityException("Fatal error: no suitable rendering engine found.");
	}
}

void MyEventHandler::initGL() {
	bg::Engine::Get()->initialize(context());
	
	_effect = new MyEffect(context());

	_pipeline = new bg::base::Pipeline(context());
	_pipeline->setClearColor(bg::math::Color(0.5f,0.5f,0.5f,1.0f));
	_pipeline->setEffect(_effect.getPtr());
	bg::base::Pipeline::SetCurrent(_pipeline.getPtr());
	
	bg::system::Path imagePath = bg::system::Path::ExecDir();
	if (bg::system::currentPlatform()==bg::system::kMac) {
		imagePath.addComponent("../../../");
	}
	imagePath.addComponent("data/texture.jpg");

	bg::ptr<bg::base::Image> image = new bg::base::Image();
	image->load(imagePath);
	bg::ptr<bg::base::Texture> tex = new bg::base::Texture(context());
	tex->setWrapModeU(bg::base::Texture::kWrapModeRepeat);
	tex->setWrapModeV(bg::base::Texture::kWrapModeRepeat);
	tex->createWithImage(image.getPtr());
	_effect->material().setTexture(tex.getPtr());
	_effect->material().setDiffuse(bg::math::Color(0.5f,0.5f,0.5f,1.0f));
	

	_effect->matrixState().projectionMatrixStack().matrix()
		.perspective(45.0f, 1.0f, 0.1f, 100.0f);
	
	_effect->matrixState().viewMatrixStack().matrix()
		.identity()
		.translate(0.0f, 0.0f, -5.0f);

	_effect->matrixState().modelMatrixStack().matrix()
		.identity()
		.rotate(bg::math::trigonometry::degreesToRadians(12.0f), 0.0f, 0.0f, 1.0f);

	_plist = new bg::base::PolyList(context());

	_plist->addVertex(bg::math::Vector3(-1.0f, -1.0f, 0.0f));
	_plist->addTexCoord0(bg::math::Vector2(0.0f, 0.0f));
	_plist->addColor(bg::math::Color(1.0f, 0.0f, 1.0f, 1.0f));

	_plist->addVertex(bg::math::Vector3( 1.0f,-1.0f, 0.0f));
	_plist->addTexCoord0(bg::math::Vector2(1.0f, 0.0f));
	_plist->addColor(bg::math::Color(1.0f, 1.0f, 0.0f, 1.0f));

	_plist->addVertex(bg::math::Vector3(1.0f, 1.0f, 0.0f));
	_plist->addTexCoord0(bg::math::Vector2(1.0f, 1.0f));
	_plist->addColor(bg::math::Color(0.0f, 1.0f, 0.0f, 1.0f));

	_plist->addVertex(bg::math::Vector3(-1.0f, 1.0f, 0.0f));
	_plist->addTexCoord0(bg::math::Vector2(0.0f, 1.0f));
	_plist->addColor(bg::math::Color(0.0f, 0.5f, 1.0f, 1.0f));

	_plist->addTriangle(0, 1, 2);
	_plist->addTriangle(2, 3, 0);

	_plist->build();
}

void MyEventHandler::willDestroyContext() {
	EventHandler::willDestroyContext();
}

void MyEventHandler::reshape(int w, int h) {
	_pipeline->setViewport(bg::math::Viewport(0,0,w,h));
	_effect->matrixState().projectionMatrixStack().matrix()
		.perspective(45.0f, _pipeline->viewport().aspectRatio(), 0.1f, 100.0f);
}

void MyEventHandler::draw() {
	_pipeline->clearBuffers(bg::base::ClearBuffers::kColorDepth);
	_pipeline->draw(_plist.getPtr());

	context()->swapBuffers();
}

void MyEventHandler::onMemoryWarning() {
	std::cout << "Memory warning received" << std::endl;
}

void MyEventHandler::setColorDiff(float diff, int channel) {
	bg::math::Color clearColor = _pipeline->clearColor();
	switch (channel) {
		case 0:
			clearColor.r(clearColor.r() + diff);
			break;
		case 1:
			clearColor.g(clearColor.g() + diff);
			break;
		case 2:
			clearColor.b(clearColor.b() + diff);
			break;
	}
	clearColor.clamp(0.0f,1.0f);
	_pipeline->setClearColor(clearColor);
}

// Mobile touch events
void MyEventHandler::touchStart(const bg::base::TouchEvent & evt)  {
	_startPos = *evt.touches().front();
}

void MyEventHandler::touchMove(const bg::base::TouchEvent & evt)  {
	bg::math::Position2Di pos = *evt.touches().front();
	float diffX = (pos.x() - _startPos.x()) * 0.001f;
	float diffY = (_startPos.y() - pos.y()) * 0.001f;
	float diff = bg::math::abs(diffX)>bg::math::abs(diffY) ? diffX:diffY;
	
	setColorDiff(diff, static_cast<int>(evt.touches().size() - 1));
	_startPos = pos;
}

void MyEventHandler::touchEnd(const bg::base::TouchEvent & evt) {
}


// Desktop events
void MyEventHandler::keyUp(const bg::base::KeyboardEvent & evt) {
	if (evt.keyboard().key()==bg::base::Keyboard::kKeyEsc) {
		bg::wnd::MainLoop::Get()->quit(0);
	}
}

void MyEventHandler::mouseDown(const bg::base::MouseEvent & evt) {
	_startPos = evt.pos();
}

void MyEventHandler::mouseDrag(const bg::base::MouseEvent & evt) {
	float diffX = (evt.pos().x() - _startPos.x()) * 0.001f;
	float diffY = (_startPos.y() - evt.pos().y()) * 0.001f;
	float diff = bg::math::abs(diffX)>bg::math::abs(diffY) ? diffX:diffY;
	int channel = (evt.mouse().buttonMask() & bg::base::Mouse::kLeftButton)	  ? 0 :
				  (evt.mouse().buttonMask() & bg::base::Mouse::kMiddleButton) ? 1 :
				  (evt.mouse().buttonMask() & bg::base::Mouse::kRightButton)  ? 2:-1;
	setColorDiff(diff, channel);
	_startPos = evt.pos();
}

