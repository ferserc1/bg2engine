/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <action/file_actions.hpp>

#include <app.hpp>

#include <bg/wnd/wnd.hpp>

namespace composer {
namespace action {

FileActions::~FileActions() {
}

void FileActions::registerActions(ui::UserActions & actions) {
	using namespace bg::base;
	actions.registerAction(new ui::Action(kCodeOpen, "Open Scene", "File/Open Scene", [&]() {
		bg::ptr<bg::wnd::FileDialog> dlg = bg::wnd::FileDialog::OpenFileDialog();
		dlg->addFilter("vitscn");
		dlg->addFilter("vitscnj");
		if (dlg->show()) {
			App::Get().scene().open(dlg->getResultPath());
		}
	}));

	actions.registerAction(new ui::Action(kCodeSave, "Save", "File/Save", [&]() {
		bg::wnd::MessageBox::Show(bg::wnd::MainLoop::Get()->window(), "Save scene", "Not implemented");
	}));

	actions.registerAction(new ui::Action(kCodeSaveAs, "Save As", "File/Save As", [&]() {
		bg::wnd::MessageBox::Show(bg::wnd::MainLoop::Get()->window(), "Save scene as", "Not implemented");
	}));

	actions.registerAction(new ui::Action(kCodeClose, "Close Scene", "File/Close", [&]() {
		bg::wnd::MessageBox::Show(bg::wnd::MainLoop::Get()->window(), "Close scene", "Not implemented");
	}));

	actions.registerAction(new ui::Action(kCodeImport, "Import Model", "File/Import", [&]() {
		// Undo implemented in scene.import
		bg::ptr<bg::wnd::FileDialog> dlg = bg::wnd::FileDialog::OpenFileDialog();
		dlg->addFilter("bg2");
		dlg->addFilter("vwglb");
		dlg->addFilter("obj");
		if (dlg->show()) {
			App::Get().scene().import(dlg->getResultPath());
		}
	}));

	actions.registerAction(new ui::Action(kCodeExport, "Export Model", "File/Export", [&]() {
		bg::wnd::MessageBox::Show(bg::wnd::MainLoop::Get()->window(), "Export model", "Not implemented");
	}));

	actions.registerAction(new ui::Action(kCodeQuit, "Quit", "File/Quit", { Keyboard::kCommandOrControlKey, Keyboard::kKeyQ }, [&]() {
		// TODO: Check file save, cancel etc.
        App::Get().destroy();
		bg::wnd::MainLoop::Get()->quit(0);
	}));
}


}
}
