/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <event_handler.hpp>

#include <bg/android/app.hpp>

MyEventHandler::MyEventHandler()
{
	
}

MyEventHandler::~MyEventHandler() {
	std::cout << "Delete event handler" << std::endl;
}

void MyEventHandler::willCreateContext() {
	if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else if (bg::system::isDesktop()){
		throw bg::base::CompatibilityException("Fatal error: no suitable rendering engine found.");
	}
}

void MyEventHandler::initGL() {
	bg::Engine::Get()->initialize(context());

    if (bg::system::currentPlatform()==bg::system::kAndroid) {
        bg::system::Path path = bg::system::Path::ResourcesDir().pathAddingComponent("data");
        bg::android::App::Get().extractAllAssets(path);
    }

	bg::db::AudioLoader::RegisterPlugin(new bg::db::plugin::ReadWavAudio());

	bg::system::Path path = bg::system::Path::AppDir();

	_pipeline = new bg::base::Pipeline(context());
	bg::base::Pipeline::SetCurrent(_pipeline.getPtr());
	_pipeline->setClearColor(bg::math::Color(0.2f, 0.5f, 0.8f, 1.0f));
	
	bg::audio::DeviceIdVector devices;
	bg::audio::Device::EnumerateDevices(devices);
	std::cout << "Audio devices:" << std::endl;
	for (auto d : devices) {
		std::cout << d << std::endl;
	}
	
	_audioDevice = new bg::audio::Device();
	_audioDevice->open();
	
	_audioContext = new bg::audio::Context(_audioDevice.getPtr());
	_audioContext->create();

	_listener = new bg::audio::Listener();
	_listener->setPosition(bg::math::Vector3(0.0f, 0.0f, 0.0f));
	_audioContext->setListener(_listener.getPtr());
	
	_source = new bg::audio::Source();
	_source->create();
	_source->setPosition(bg::math::Vector3(0.0f, 0.0f, -1.5f));
	_source->setLooping(true);

	_buffer = bg::db::loadAudio(_audioContext.getPtr(), path.pathAddingComponent("data/water.wav"));
	_source->bindBuffer(_buffer.getPtr());
}

void MyEventHandler::destroy() {
	_source->destroy();
	_buffer->destroy();
	_audioContext->destroy();
	_audioDevice->close();
}

void MyEventHandler::reshape(int w, int h) {
	_pipeline->setViewport(bg::math::Viewport(0,0,w,h));
}

void MyEventHandler::draw() {
	_audioContext->update();
	_pipeline->clearBuffers(bg::base::ClearBuffers::kColorDepth);
	context()->swapBuffers();
}

void MyEventHandler::onMemoryWarning() {
	std::cout << "Memory warning received" << std::endl;
}

// Mobile touch events
void MyEventHandler::touchStart(const bg::base::TouchEvent & evt)  {
	if (_source->isPlaying()) {
		_source->pause();
	}
	else {
		_source->play();
	}
}

void MyEventHandler::sensorEvent(const bg::base::SensorEvent & evt) {
    bg::log(bg::log::kDebug) << "Sensor changed: " << evt.acceleration() << bg::endl;
}

// Desktop events
void MyEventHandler::mouseDown(const bg::base::MouseEvent & evt) {
	if(_source->isPlaying()) {
		_source->pause();
	}
	else {
		_source->play();
	}
}

void MyEventHandler::keyUp(const bg::base::KeyboardEvent & evt) {
	if (evt.keyboard().key() == bg::base::Keyboard::kKeyEsc) {
		bg::wnd::MainLoop::Get()->quit(0);
	}
	else if (evt.keyboard().key() == bg::base::Keyboard::kKeyLeft) {
		_source->setPosition(_source->position() + bg::math::Vector3(-0.5f, 0.0f, 0.0f));
	}
	else if (evt.keyboard().key() == bg::base::Keyboard::kKeyRight) {
		_source->setPosition(_source->position() + bg::math::Vector3(0.5f, 0.0f, 0.0f));
	}
	else if (evt.keyboard().key() == bg::base::Keyboard::kKeyUp) {
		_source->setPosition(_source->position() + bg::math::Vector3(0.0f, 0.5f, 0.0f));
	}
	else if (evt.keyboard().key() == bg::base::Keyboard::kKeyDown) {
		_source->setPosition(_source->position() + bg::math::Vector3(0.0f,-0.5f, 0.0f));
	}
}


