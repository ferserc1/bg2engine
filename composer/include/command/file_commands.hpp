/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef _command_file_commands_hpp_
#define _command_file_commands_hpp_

#include <command/command.hpp>
#include <bg/scene/node.hpp>
#include <bg/scene/drawable.hpp>

#include <functional>

namespace composer {
namespace command {

class ImportCommand : public ContextCommand {
public:
	ImportCommand(bg::base::Context * ctx, const std::string & path, bg::scene::Node * parentNode)
		:ContextCommand(ctx), _path(path), _parentNode(parentNode) {}

	typedef std::function<void(bg::scene::Drawable *, bg::scene::Node *)> ImportDrawableClosure;

	virtual void execute();
	virtual void undo();

	inline void onImportDrawable(ImportDrawableClosure c) { _importDrawableClosure = c; }

protected:
	virtual ~ImportCommand() {}

	std::string _path;
	bg::ptr<bg::scene::Node> _parentNode;
	bg::ptr<bg::scene::Node> _importedNode;

	ImportDrawableClosure _importDrawableClosure;
};

}
}

#endif