
#include <gui.hpp>

#include <bg/gui/gui.hpp>
#include <bg/db/ui_skin.hpp>
#include <bg/wnd/window.hpp>

namespace viewer {

Gui::Gui(bg::base::Context * ctx, bg::wnd::Window * wnd)
	:ContextObject(ctx)
	,_window(wnd)
{
	init();
}

Gui::~Gui() {

}

void Gui::init() {
	using namespace bg::gui;
	bg::system::Path path = bg::system::Path::AppDir();
    path.addComponent("data");
    if (bg::system::currentPlatform()==bg::system::kMac) {
        path = bg::system::Path::ResourcesDir();
    }
	bg::ptr<Skin> skin = bg::db::loadSkin(context(), path.pathAddingComponent("default.bg2skin"));

	_surface = new Surface(context(), skin.getPtr());
	_surface->setId("surface");

	_configUi = new ConfigUI(_surface.getPtr());
	
}

void Gui::resize(int w, int h) {
	_surface->resize(w, h);
}

void Gui::frame(float delta) {
	float s = _window->scale();
	_surface->setScale(s);
}

void Gui::draw() {
	_surface->draw();
}

bool Gui::keyUp(const bg::base::KeyboardEvent & evt) {
	return _surface->keyUp(evt);
}

bool Gui::keyDown(const bg::base::KeyboardEvent & evt) {
	return _surface->keyDown(evt);
}

bool Gui::mouseDown(const bg::base::MouseEvent & evt) {
	return _surface->mouseDown(evt);
}

bool Gui::mouseDrag(const bg::base::MouseEvent & evt) {
	return _surface->mouseDrag(evt);
}

bool Gui::mouseUp(const bg::base::MouseEvent & evt) {
	return _surface->mouseUp(evt);
}

bool Gui::mouseMove(const bg::base::MouseEvent & evt) {
	return _surface->mouseMove(evt);
}

bool Gui::mouseWheel(const bg::base::MouseEvent & evt) {
	return _surface->mouseWheel(evt);
}


}
