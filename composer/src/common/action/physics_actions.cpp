/*
 *    bg2 engine license
 *    Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *    Permission is hereby granted, free of charge, to any person obtaining a copy
 *    of this software and associated documentation files (the "Software"), to deal
 *    in the Software without restriction, including without limitation the rights
 *    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *    of the Software, and to permit persons to whom the Software is furnished to do
 *    so, subject to the following conditions:
 *
 *    The above copyright notice and this permission notice shall be included in all
 *    copies or substantial portions of the Software.
 *
 *    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *    OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */


#include <action/physics_actions.hpp>

#include <app.hpp>

#include <bg/scene/find_component_visitor.hpp>
#include <bg/scene/dynamics.hpp>

namespace composer {
namespace action {

void getDynamics(std::vector<bg::scene::Dynamics*> dynamics) {
    bg::scene::Node * root = App::Get().scene().sceneRoot();
    
    bg::ptr<bg::scene::FindComponentVisitor<bg::scene::Dynamics>> visitor = new bg::scene::FindComponentVisitor<bg::scene::Dynamics>();
    root->accept(visitor.getPtr());
    
    for (auto dyn : visitor->result()) {
        dynamics.push_back(dyn);
    }
}
    
void iterateDynamics(std::function<void(bg::scene::Dynamics*)> cb) {
    bg::scene::Node * root = App::Get().scene().sceneRoot();
    
    bg::ptr<bg::scene::FindComponentVisitor<bg::scene::Dynamics>> visitor = new bg::scene::FindComponentVisitor<bg::scene::Dynamics>();
    root->accept(visitor.getPtr());
    
    for (auto dyn : visitor->result()) {
        cb(dyn);
    }
}
    
PhysicsActions::~PhysicsActions() {
    
}

void PhysicsActions::registerActions(ui::UserActions & actions) {
    using namespace bg::base;
    
    actions.registerAction(new ui::Action(kCodePlay, "Play", "Physics/Play", [&]() {
        iterateDynamics([&](bg::scene::Dynamics * dyn) {
            dyn->play();
        });
    }));
    
    actions.registerAction(new ui::Action(kCodePause, "Pause", "Physics/Pause", [&]() {
        iterateDynamics([&](bg::scene::Dynamics * dyn) {
            dyn->pause();
        });
    }));
    
    actions.registerAction(new ui::Action(kCodeStop, "Stop", "Physics/Stop", [&]() {
        iterateDynamics([&](bg::scene::Dynamics * dyn) {
            dyn->stop();
        });
    }));
}

}
}
