// App glue file, located in ${bg2e}/android/cpp
#include <app_glue.hpp>

// Test event handler, located in ${test_dir}/common
#include <event_handler.hpp>

// App glue factory
bg::base::EventHandler * Renderer::createEventHandler() {
    bg::ptr<MyEventHandler> result = new MyEventHandler();
    return result.release();
}

