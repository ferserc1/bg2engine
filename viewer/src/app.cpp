/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */


#include <app.hpp>
#include <event_handler.hpp>

#include <bg/log.hpp>
#include <bg/wnd/wnd.hpp>

#include <bg/db/db.hpp>

#include <bg/gui/gui.hpp>

namespace viewer {

App App::s_app;

App::App()
{
	bg::system::Path configPath = bg::system::Path::ExecDir();
	if (bg::system::currentPlatform() == bg::system::kMac) {
		configPath = bg::system::Path::ResourcesDir();
	}
	configPath.addComponent("config.json");
	_mainConfig = new Config(configPath);
	_mainConfig->load();
}

App::~App() {
	_ctx = nullptr;
	delete _mainConfig;
}

int App::run() {
	{
		bg::ptr<bg::wnd::Window> window = bg::wnd::Window::New();

		bg::math::Rect windowRect = config().getVector4i("mainWindowRect",bg::math::Rect(50,50,1024,768));
		if (!window.valid()) {
			std::cerr << "Platform not supported" << std::endl;
			return -1;
		}
		_window = window;

		window->setIcon("viewer.ico");
		window->setRect(windowRect);
		window->setTitle("bg2 Composer");
		_eventHandler = new EventHandler();
		window->setEventHandler(_eventHandler.getPtr());

		window->create();


		bg::wnd::MainLoop::Get()->setWindow(window.getPtr());
	}
	int status = bg::wnd::MainLoop::Get()->run();
	_window = nullptr;
	_eventHandler = nullptr;
	_mainConfig->save();

	return status;
}

void App::init(bg::base::Context * ctx) {
	_ctx = ctx;

	loadPlugins();

	_scene = new Scene(ctx);

	_gui = new Gui(ctx,_window.getPtr());
}

void App::destroy() {
	_gui = nullptr;
	_scene = nullptr;
	_ctx = nullptr;
}

void App::windowRectChanged(const bg::math::Rect & rect) {
	config().set("mainWindowRect", rect);
}

void App::loadPlugins() {
	bg::db::AudioLoader::RegisterPlugin(new bg::db::plugin::ReadWavAudio());

	bg::db::DrawableLoader::RegisterPlugin(new bg::db::plugin::ReadDrawableBg2());
	bg::db::DrawableLoader::RegisterPlugin(new bg::db::plugin::ReadDrawableObj());
	bg::db::DrawableWriter::RegisterPlugin(new bg::db::plugin::WriteDrawableBg2());

	bg::db::FontLoader::RegisterPlugin(new bg::db::plugin::ReadFontTrueType());

	bg::db::NodeLoader::RegisterPlugin(new bg::db::plugin::ReadScene());
	bg::db::NodeLoader::RegisterPlugin(new bg::db::plugin::ReadPrefabBg2());
	bg::db::NodeWriter::RegisterPlugin(new bg::db::plugin::WritePrefabBg2());
	bg::db::NodeWriter::RegisterPlugin(new bg::db::plugin::WriteScene());

	bg::db::GuiSkinLoader::RegisterPlugin(new bg::db::plugin::ReadGuiSkinBg2());
}

void App::initActions() {

}

}
