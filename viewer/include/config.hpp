/*
*	bg2 engine license
*	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
*
*	Permission is hereby granted, free of charge, to any person obtaining a copy
*	of this software and associated documentation files (the "Software"), to deal
*	in the Software without restriction, including without limitation the rights
*	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
*	of the Software, and to permit persons to whom the Software is furnished to do
*	so, subject to the following conditions:
*
*	The above copyright notice and this permission notice shall be included in all
*	copies or substantial portions of the Software.
*
*	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
*	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
*	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
*	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
*	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
*	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
*/

#ifndef _viewer_config_hpp_
#define _viewer_config_hpp_

#include <bg/system/path.hpp>
#include <bg/db/json/parser.hpp>
#include <bg/math/math.hpp>

#include <map>

namespace viewer {

class Config;
class ConfigOptions : public bg::base::ReferencedPointer {
public:
	ConfigOptions(Config * cfg) {}

protected:
	virtual ~ConfigOptions() {}

	Config * _cfg;
};

class Config {
public:
	Config(const bg::system::Path & configPath);
	virtual ~Config();

	void load();
	void save();

	void set(const std::string & key, const std::string & value);
	void set(const std::string & key, bool value);
	void set(const std::string & key, int value);
	void set(const std::string & key, float value);
	void set(const std::string & key, const bg::math::Vector2 & value);
	void set(const std::string & key, const bg::math::Vector3 & value);
	void set(const std::string & key, const bg::math::Vector4 & value);
	void set(const std::string & key, const bg::math::Vector2i & value);
	void set(const std::string & key, const bg::math::Vector3i & value);
	void set(const std::string & key, const bg::math::Vector4i & value);
	void set(const std::string & key, const bg::math::Matrix3 & value);
	void set(const std::string & key, const bg::math::Matrix4 & value);

	std::string getString(const std::string & key, const std::string & def = "");
	bool getBool(const std::string & key, bool def = false);
	int getInt(const std::string & key, int def = 0);
	float getFloat(const std::string & key, float def = 0.f);
	bg::math::Vector2 getVector2(const std::string & key, const bg::math::Vector2 & def = bg::math::Vector2{});
	bg::math::Vector3 getVector3(const std::string & key, const bg::math::Vector3 & def = bg::math::Vector3{});
	bg::math::Vector4 getVector4(const std::string & key, const bg::math::Vector4 & def = bg::math::Vector4{});
	bg::math::Vector2i getVector2i(const std::string & key, const bg::math::Vector2i & def = bg::math::Vector2i{});
	bg::math::Vector3i getVector3i(const std::string & key, const bg::math::Vector3i & def = bg::math::Vector3i{});
	bg::math::Vector4i getVector4i(const std::string & key, const bg::math::Vector4i & def = bg::math::Vector4i{});
	bg::math::Matrix3 getMatrix3(const std::string & key, const bg::math::Matrix3 & def = bg::math::Matrix3::Identity());
	bg::math::Matrix4 getMatrix4(const std::string & key, const bg::math::Matrix4 & def = bg::math::Matrix4::Identity());
	
	template <class T>
	void registerOptions() {
		_configOptions[T::Id()] = new T(this);
	}

	template <class T>
	T * options(const std::string & o) {
		return dynamic_cast<T*>(_configOptions[o].getPtr());
	}

protected:
	bg::system::Path _savePath;
	bg::ptr<bg::db::json::Value> _config;

	std::map < std::string, bg::ptr<ConfigOptions>> _configOptions;
};

}

#endif
