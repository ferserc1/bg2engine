/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

//
//  event-handler.cpp
//  bg2e-ios
//
//  Created by Fernando Serrano Carpena on 3/5/16.
//  Copyright © 2016 Fernando Serrano Carpena. All rights reserved.
//

#include "event_handler.hpp"

#include <bg/engine.hpp>
#include <bg/wnd/main_loop.hpp>

#include <bg/system/system.hpp>

#include <bg/base/matrix_state.hpp>
#include <bg/base/effect.hpp>
#include <bg/base/image.hpp>
#include <bg/base/texture.hpp>
#include <bg/base/render_surface.hpp>

#include <bg/engine/directx11.hpp>
#include <bg/engine/opengl_core.hpp>

#include <bg/system/path.hpp>


MyEventHandler::MyEventHandler()
{
	
}

MyEventHandler::~MyEventHandler() {
	
}

void MyEventHandler::willCreateContext() {
	if (bg::engine::DirectX11::Supported()) {
		bg::Engine::Init(new bg::engine::DirectX11());
	}
	else if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else if (bg::system::isDesktop()){
		throw bg::base::CompatibilityException("Fatal error: no suitable rendering engine found.");
	}
}

void MyEventHandler::initGL() {
	bg::Engine::Get()->initialize(context());
	
	_pipeline = new bg::base::Pipeline(context());
	bg::base::Pipeline::SetCurrent(_pipeline.getPtr());
	
	bg::system::Path imagePath = bg::system::Path::ExecDir();
	if (bg::system::currentPlatform()==bg::system::kMac) {
		imagePath.addComponent("../../../");
	}
	imagePath.addComponent("data/texture.jpg");

	bg::ptr<bg::base::Image> image = new bg::base::Image();
	image->load(imagePath);
	_texture = new bg::base::Texture(context());
	_texture->setWrapModeU(bg::base::Texture::kWrapModeClamp);
	_texture->setWrapModeV(bg::base::Texture::kWrapModeClamp);
	_texture->createWithImage(image.getPtr());
}

void MyEventHandler::reshape(int w, int h) {
	_pipeline->setViewport(bg::math::Viewport(0,0,w,h));
}

void MyEventHandler::frame(float delta) {
}

void MyEventHandler::draw() {
	_pipeline->clearBuffers(bg::base::ClearBuffers::kColorDepth);
	_pipeline->drawTexture(_texture.getPtr());
	context()->swapBuffers();
}

void MyEventHandler::onMemoryWarning() {
	std::cout << "Memory warning received" << std::endl;
}

// Mobile touch events
void MyEventHandler::touchStart(const bg::base::TouchEvent & evt)  {
}

void MyEventHandler::touchMove(const bg::base::TouchEvent & evt)  {
}

void MyEventHandler::touchEnd(const bg::base::TouchEvent & evt) {
}


// Desktop events
void MyEventHandler::keyUp(const bg::base::KeyboardEvent & evt) {
	if (evt.keyboard().key() == bg::base::Keyboard::kKeyEsc) {
		bg::wnd::MainLoop::Get()->quit(0);
	}
}

void MyEventHandler::mouseDown(const bg::base::MouseEvent & evt) {
}

void MyEventHandler::mouseDrag(const bg::base::MouseEvent & evt) {
}

