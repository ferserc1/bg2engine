/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <event_handler.hpp>

#include <bg/bg2e.hpp>

#include <bg/render/render.hpp>
#include <bg/android/app.hpp>


class TextComponent : public bg::scene::Component {
public:
	TextComponent(bg::text::Font * f) :_font(f) {}

	TextComponent * clone() { return new TextComponent(_font.getPtr()); }

	virtual void init() {
		_stringRenderer = new bg::text::StringRenderer(_font.getPtr(),context());
		_textRect = new bg::text::TextRect(context(),_font.getPtr());
		_textRect->setText("In Spanish, \"cannon\" it's said \"cañón\".\nText after new line character.\rText after return character.\n\tone tab.\n\t\ttwo tabs.");
		_textRect->setTextureSize(bg::math::Size2Di(2048));
		_textRect->material()->setShininess(10.0f);
		_textRect->material()->setDiffuse(bg::math::Color::Green());
		_textRect->material()->setLightEmission(0.5f);
		_textRect->material()->setCullFace(false);
	}
	
	virtual void frame(float delta) {
		_textRect->frame(delta);
	}
	
	virtual void draw(bg::base::Pipeline * pl) {
		//_stringRenderer->draw(pl,"In Spanish, \"cannon\" it's said \"cañón\".\nText after new line character.\rText after return character.\n\tone tab.\n\t\ttwo tabs.");
		
		_textRect->draw(pl);
	}

	virtual void keyUp(const bg::base::KeyboardEvent & evt) {
		switch (evt.keyboard().key()) {
		case bg::base::Keyboard::kKeyRight:
			_stringRenderer->paragraph().setCharSpacing(_stringRenderer->paragraph().charSpacing() + 0.1f);
			_textRect->paragraph().setCharSpacing(_stringRenderer->paragraph().charSpacing() + 0.1f);
			break;
		case bg::base::Keyboard::kKeyLeft:
			_stringRenderer->paragraph().setCharSpacing(_stringRenderer->paragraph().charSpacing() - 0.1f);
			_textRect->paragraph().setCharSpacing(_stringRenderer->paragraph().charSpacing() - 0.1f);
			break;
		case bg::base::Keyboard::kKeyDown:
			_stringRenderer->paragraph().setLineSpacing(_stringRenderer->paragraph().lineSpacing() + 0.1f);
			_textRect->paragraph().setLineSpacing(_stringRenderer->paragraph().lineSpacing() + 0.1f);
			break;
		case bg::base::Keyboard::kKeyUp:
			_stringRenderer->paragraph().setLineSpacing(_stringRenderer->paragraph().lineSpacing() - 0.1f);
			_textRect->paragraph().setLineSpacing(_stringRenderer->paragraph().lineSpacing() - 0.1f);
			break;
		case bg::base::Keyboard::kKeyA:
			_stringRenderer->paragraph().setAlignment(bg::text::Paragraph::kHAlignLeft);
			_textRect->paragraph().setAlignment(bg::text::Paragraph::kHAlignLeft);
			break;
		case bg::base::Keyboard::kKeyS:
			_stringRenderer->paragraph().setAlignment(bg::text::Paragraph::kHAlignCenter);
			_textRect->paragraph().setAlignment(bg::text::Paragraph::kHAlignCenter);
			break;
		case bg::base::Keyboard::kKeyD:
			_stringRenderer->paragraph().setAlignment(bg::text::Paragraph::kHAlignRight);
			_textRect->paragraph().setAlignment(bg::text::Paragraph::kHAlignRight);
			break;
		case bg::base::Keyboard::kKeyR:
			_stringRenderer->paragraph().setAlignment(bg::text::Paragraph::kVAlignTop);
			_textRect->paragraph().setAlignment(bg::text::Paragraph::kVAlignTop);
			break;
		case bg::base::Keyboard::kKeyF:
			_stringRenderer->paragraph().setAlignment(bg::text::Paragraph::kVAlignCenter);
			_textRect->paragraph().setAlignment(bg::text::Paragraph::kVAlignCenter);
			break;
		case bg::base::Keyboard::kKeyV:
			_stringRenderer->paragraph().setAlignment(bg::text::Paragraph::kVAlignBottom);
			_textRect->paragraph().setAlignment(bg::text::Paragraph::kVAlignBottom);
			break;
		default:
			break;
		}
	}
	
protected:
	virtual ~TextComponent() {}

	bg::ptr<bg::text::Font> _font;
	bg::ptr<bg::text::StringRenderer> _stringRenderer;
	bg::ptr<bg::text::TextRect> _textRect;
	
};

MyEventHandler::MyEventHandler()
{
	
}

MyEventHandler::~MyEventHandler() {
	
}

void MyEventHandler::willCreateContext() {
	if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else if (bg::system::isDesktop()){
		throw bg::base::CompatibilityException("Fatal error: no suitable rendering engine found.");
	}
}

void MyEventHandler::initGL() {
	using namespace bg::scene;
	using namespace bg::math;
	bg::Engine::Get()->initialize(context());

    bg::system::Path path = bg::system::Path::AppDir();
    if (bg::system::currentPlatform()==bg::system::kAndroid) {
        path = bg::system::Path::ResourcesDir();
    }

    if (bg::system::currentPlatform()==bg::system::kAndroid) {
        bg::android::App::Get().extractAllAssets(path.pathAddingComponent("data"));
    }

	bg::db::FontLoader::RegisterPlugin(new bg::db::plugin::ReadFontTrueType());

	path.addComponent("data/Hind-Medium.ttf");
	
	
	_font = bg::db::loadFont(context(), path, 20.0f);
	bg::base::Material * mat = new bg::base::Material();
	mat->setDiffuse(bg::math::Color::Green());
	mat->setLightEmission(1.0f);
	mat->setProcessAsTransparent(true);
	mat->setCullFace(false);
	_font->setMaterial(mat);
	_font->setTextScale(0.5f);
	
	_inputVisitor = new InputVisitor();
	_renderer = bg::render::Renderer::Create(context(), bg::render::Renderer::kRenderPathForward);
	_renderer->setRenderSetting(bg::render::settings::kAmbientOcclusion,
								bg::render::settings::kKernelSize, 32);
	_renderer->setRenderSetting(bg::render::settings::kAmbientOcclusion,
								bg::render::settings::kBlur, 2);

	_sceneRoot = new Node(context());

	_camera = new Camera();
	Node * cameraNode = new Node(context());
	cameraNode->addComponent(_camera.getPtr());
	cameraNode->addComponent(new Transform());
	bg::manipulation::OrbitNodeController * orbit = new bg::manipulation::OrbitNodeController();
	orbit->setRotation(bg::math::Vector2(0.0f, 0.0f));
	cameraNode->addComponent(orbit);
	_camera->setProjectionStrategy(new OpticalProjectionStrategy());
	_sceneRoot->addChild(cameraNode);

	Node * floor = new Node(context());
	floor->addComponent(PrimitiveFactory(context()).plane(10.0f));
	floor->addComponent(new Transform(Matrix4::Translation(0.0f, -0.5f, 0.0f)));
	_sceneRoot->addChild(floor);
	
	Node * cube = new Node(context());
	//cube->addComponent(PrimitiveFactory(context()).cube(1.0f));
	cube->addComponent(new TextComponent(_font.getPtr()));
	_sceneRoot->addChild(cube);
	
	Node * light = new Node(context());
	light->addComponent(new Transform(Matrix4::Identity()
		.rotate(trigonometry::degreesToRadians(55.0f), -1.0f, 0.0f, 0.0f)
		.translate(0.0f, 0.0f, 5.0f)));
	light->addComponent(new Light());
	bg::base::Light * l = light->light()->light();
	l->setType(bg::base::Light::kTypeSpot);
	l->setAmbient(Color::Black());
	l->setConstantAttenuation(-1.0f);
	l->setLinearAttenuation(0.01f);
	l->setSpotExponent(65.0f);
	_sceneRoot->addChild(light);
}

void MyEventHandler::reshape(int w, int h) {
	_camera->setViewport(bg::math::Viewport(0,0,w,h));
}

void MyEventHandler::frame(float delta) {
	_renderer->frame(_sceneRoot.getPtr(), delta);
}

void MyEventHandler::draw() {
	_renderer->draw(_sceneRoot.getPtr(), _camera.getPtr());
	context()->swapBuffers();
}

void MyEventHandler::onMemoryWarning() {
	std::cout << "Memory warning received" << std::endl;
}

// Mobile touch events
void MyEventHandler::touchStart(const bg::base::TouchEvent & evt)  {
	_inputVisitor->touchStart(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::touchMove(const bg::base::TouchEvent & evt)  {
	_inputVisitor->touchMove(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::touchEnd(const bg::base::TouchEvent & evt) {
	_inputVisitor->touchEnd(_sceneRoot.getPtr(), evt);
}


// Desktop events
void MyEventHandler::keyUp(const bg::base::KeyboardEvent & evt) {
	if (evt.keyboard().key() == bg::base::Keyboard::kKeyEsc) {
		bg::wnd::MainLoop::Get()->quit(0);
	}
	else {
		_inputVisitor->keyUp(_sceneRoot.getPtr(), evt);
	}
}

void MyEventHandler::mouseDown(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseDown(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::mouseDrag(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseDrag(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::mouseWheel(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseWheel(_sceneRoot.getPtr(), evt);
}

