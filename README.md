# bg2 engine
## Business Grade Graphic Engine

bg2 engine is an open source graphic engine oriented to develop enterprise applications. You can also create games, but the main objective of bg2 is to provide a modern graphic engine that you can easilly integrate in a native application (Windows 10 with XAML, macOS Objective-C or Swift, iOS, etc).

bg2 engine is available in two APIs: C++ and Javascript. You can share scenes and objects between both APIs, and the libraries are similar, so is very easy to learn one API if you already know the other one.

This repository hosts the bg2 engine C++ API. To download the Javascript API, visit:

[https://bitbucket.org/ferserc1/bg2e-js](https://bitbucket.org/ferserc1/bg2e-js)

## Features of de C++ API

Keep in mind that the engine is still under development. This list includes all the roadmap features, but some of them should not be available yet.

- Object oriented C++11 API.
- Multi engine capabilities (OpenGL, OpenGL ES, DirectX).
- Multi platform (Windows, macOS, iOS).
- Forward and deferred render paths.
- Real time shadows.
- Screen space ambient occlusion.
- Screen space ray tracing (reflections).
- Plugin system to support different formats.
- Sound spatialization using [OpenAL](http://openal.org).
- Rigid body physics using [Bullet Physics](http;//bulletphysics.org).
- Very simple and quick installation.
- Extensible scene components.
- Lua scripting utilities.

## License

bg2 engine is published under the MIT X11 license. Basically:

- YOU CAN use, copy, modify or distribute bg2 engine freely, in commercial or non-commercial applications. You can also distribute the source code or the precompiled binaries.
- YOU MUST include the unmodified copyright notice in all copies or substantial portions of bg2 engine (basically, you can't claim that you have created bg2 engine).
- YOU CAN'T require me any warranty (the software is provided without any warranty).

See LICENSE.md for more information.

#### Download dependencies

You can get the prebuilt dependencies or compile them from the source. The prebuilt depencencies are ready to uncompress and use, and is the easiest way to start, but you may need to compile the source from code, for example, if you need to support 32 bits systems.

You must place the dependencies folders at the same path as you put the bg2engine folder, downloaded from the repository:

 My Development folder
 	|- bg2engine  > the repository
	|- bullet > bullet physics dependencies
	|- openal > alsoft dependencies (only in Windows platform)
	|- lua > lua dependencies


##### Prebuild dependencies:

This is the simplest way to start working with bg2 engine. Download and unzip the dependencies. The unzipped folder is the folder that you must to place at the same path as the repository.

- [Bullet Phisics](https://bitbucket.org/ferserc1/bg2engine/downloads/bullet-android-ios-macos-winVS2015-winVS2017.zip).
- [Lua scripting language](https://bitbucket.org/ferserc1/bg2engine/downloads/lua-macOS-ios-winVS2015-winVS2017.zip).
- [OpenAL Soft](https://bitbucket.org/ferserc1/bg2engine/downloads/openal-android-winVS2015-winVS2017.zip) (only for Windows platform).

##### Build dependencies from source:

You can get the source code through these links. Keep in mind that you must place the dependencies using the same folder structure as the prebuilt ones:

- [Bullet Physics](http://bulletphysics.org).
- [Lua scripting language](http://lua.org).
- [OpenAL Soft](http://kcat.strangesoft.net/openal.html) (only for Windows platform).

## Installation
### Windows

The engine is currently tested in Windows 10, but you can make it work in Windows 8 installing the Windows SDK (it's necesary to provide the DirectX headers). The development has been done using Visual Studio Community 2017, that you can obtain freely [here](https://www.visualstudio.com). The project files and the prebuilt dependencies only works in 64 bits systems.

You can build the engine following these steps:

1. Download or build the dependencies as shown before.
2. Download the repository.
3. Open bg2e.sln in Visual Studio Community 2017.
4. Select Build > Batch build and mark all targets and configurations.
5. Press Build.


### macOS

To build the engine for macOS, use the official development tools from Apple Inc. You can get the Xcode Tools from the Mac App Store. To build the engine, follow this steps:

1. Download or build the dependencies as shown before.
2. Download the repository.
3. Open bg2e.xcodeproj.
4. Select Install target.
5. Select Product > Build (debug), or Product > Build for > Running (release)
6. You can find the products in [bg2engine]/build/osx


### iOS

To build the iOS project, follow the same instructions as shown in the previous section, but opening the bg2e-ios.xcodeproj file instead of bg2e.xcodeproj. You will find the built products in [bg2engine]/build/ios.

