/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */


#include <iostream>
#include <bg/lua/lua.hpp>

float mySub(float x, float y) {
	return x - y;
}

int main(int argc, char ** argv) {
	bg::lua::State state;
	bg::system::Path dataPath = bg::system::Path::ExecDir();
	
	std::string cString = "Hello from C";
	std::function<std::string(void)> func = [cString] () -> std::string {
		return cString;
	};
	state.registerFunction("cFromLua", func);

	state.registerFunction("sub", &mySub);

	std::function<float(float, float)> addFunc = [&](float x, float y) -> float {
		return x + y;
	};
	state.registerFunction("add", addFunc);

	std::string script =
		"function add_and_hello(a, b)\n"
		"	return add(a, b), cFromLua()\n"
		"end\n"
		"function sub_and_hello(a, b)\n"
		"	return sub(a, b), \"Hello from Lua\"\n"
		"end";

	if (!state.load(script)) {	// You can also use state.open(path) to load a script from a file
		std::cerr << "Error parsing lua script" << std::endl;
		return -1;
	}

	float result = 0.0f;
	std::string greeting = "";
	std::tie(result, greeting) = state.call<float, std::string>("add_and_hello", 3.45f, 4.75f);

	std::cout << result << ", " << greeting << std::endl;

	std::tie(result, greeting) = state.call<float, std::string>("sub_and_hello", 5.01f, 7.22f);

	std::cout << result << ", " << greeting << std::endl;

	return 0;
}