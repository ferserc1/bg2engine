/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef _composer_ui_workspace_hpp_
#define _composer_ui_workspace_hpp_

#include <defines.hpp>
#include <ui/window.hpp>

#include <ui/scene_window.hpp>
#include <ui/statusbar.hpp>
#include <ui/toolbar.hpp>

#include <map>

#include <bg/gui/gui.hpp>

namespace composer {
namespace ui {

class Workspace {
public:
	COMPOSER_CLASSID("Workspace")
	
	Workspace();

	void buildUserInterface(bg::base::Context *);
	
	inline ui::SceneWindow & sceneWindow() { return *_sceneWindow; }
	inline ui::StatusBar & statusBar() { return *_statusBar; }
	inline ui::ToolBar & toolBar() { return *_toolBar; }

	void resize(const bg::math::Viewport & vp);
	void frame(float,float uiScale);
	void draw();

	inline bool touchStart(const bg::base::TouchEvent & evt) { return _surface->touchStart(evt); }
	inline bool touchMove(const bg::base::TouchEvent & evt) { return _surface->touchMove(evt); }
	inline bool touchEnd(const bg::base::TouchEvent & evt) { return _surface->touchEnd(evt); }
	inline bool keyUp(const bg::base::KeyboardEvent & evt) { return _surface->keyUp(evt); }
	inline bool keyDown(const bg::base::KeyboardEvent & evt) { return _surface->keyDown(evt); }
	inline bool charPress(const bg::base::KeyboardEvent & evt) { return _surface->charPress(evt); }
	inline bool mouseDown(const bg::base::MouseEvent & evt) { return _surface->mouseDown(evt); }
	inline bool mouseMove(const bg::base::MouseEvent & evt) { return _surface->mouseMove(evt); }
	inline bool mouseDrag(const bg::base::MouseEvent & evt) { return _surface->mouseDrag(evt); }
	inline bool mouseWheel(const bg::base::MouseEvent & evt) { return _surface->mouseWheel(evt); }
	inline bool mouseUp(const bg::base::MouseEvent & evt) { return _surface->mouseUp(evt); }

protected:
	bg::ptr<bg::gui::Skin> _skin;
	bg::ptr<bg::gui::Surface> _surface;

	bg::ptr<ui::SceneWindow> _sceneWindow;
	bg::ptr<ui::StatusBar> _statusBar;
	bg::ptr<ui::ToolBar> _toolBar;
	
	void initWindows();
};

}
}

#endif
