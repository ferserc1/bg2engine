/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

// bg2e-viewer event_handler.hpp


#ifndef viewer_event_handler_hpp
#define viewer_event_handler_hpp

#include <bg/base/event_handler.hpp>
#include <bg/base/pipeline.hpp>
#include <bg/render/renderer.hpp>
#include <bg/scene/input_visitor.hpp>

#include <bg/scene/dynamics.hpp>

#include <bg/math/vector.hpp>

#include <functional>

namespace viewer {

class EventHandler : public bg::base::EventHandler {
public:

	EventHandler();

	void setForwardRender();
	void setDeferredRender();
	inline bool isDeferredRender() const { return _renderer == _deferredRenderer.getPtr(); }
	inline bool isForwardRender() const { return _renderer == _forwardRenderer.getPtr(); }

	void setRaytracerQuality(int quality);
	void setSSAOQuality(int quality);
	int raytracerQuality();
	int ssaoQuality();
	
	// Creation and initialization
	virtual void willCreateContext();
	virtual void initGL();
	virtual void willDestroyContext();
	
	// Reshape and draw
	virtual void reshape(int, int);
	virtual void frame(float);
	virtual void draw();
	
	// Desktop events
	virtual void keyUp(const bg::base::KeyboardEvent & evt);
	virtual void keyDown(const bg::base::KeyboardEvent & evt);
	virtual void mouseDown(const bg::base::MouseEvent & evt);
	virtual void mouseDrag(const bg::base::MouseEvent & evt);
	virtual void mouseUp(const bg::base::MouseEvent & evt);
	virtual void mouseMove(const bg::base::MouseEvent & evt);
	virtual void mouseWheel(const bg::base::MouseEvent & evt);

	virtual void buildMenu(bg::wnd::MenuDescriptor & menu);
	virtual void menuSelected(const std::string & title, int32_t identifier);

	void windowRectChanged(int, int, int, int);

	void openFile();

	void play();
	void pause();
	void stop();

protected:
	virtual ~EventHandler();

	bg::ptr<bg::render::Renderer> _forwardRenderer;
	bg::ptr<bg::render::Renderer> _deferredRenderer;
	bg::render::Renderer * _renderer;

	bg::ptr<bg::scene::InputVisitor> _inputVisitor;

	bg::math::Viewport _viewport;

	void dynamics(std::function<void(bg::scene::Dynamics*)>);
};

}

#endif /* event_handler_hpp */
