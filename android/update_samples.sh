#!/bin/bash
files="$(find ../tests/**/android/app/bg2e -name 'CMakeLists.txt')"
src="CMakeLists.txt"
for f in $files
do
    echo "Updating file " $f ;
    cp $src $f;
done

files="$(find ../tests/**/android/app/src/main/java/com/bg2engine/android -name 'GLView.java')"
src="com/bg2engine/android/GLView.java"
for f in $files
do
    echo "Updating file " $f ;
    cp $src $f;
done

files="$(find ../tests/**/android/app/src/main/java/com/bg2engine/base -name 'EventHandler.java')"
src="com/bg2engine/base/EventHandler.java"
for f in $files
do
    echo "Updating file " $f ;
    cp $src $f;
done
