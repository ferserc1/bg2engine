/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <app.hpp>

#include <ui/workspace.hpp>

#include <bg/base/exception.hpp>

#include <bg/db/db.hpp>

#include <bg/system/system.hpp>

namespace composer {
namespace ui {

Workspace::Workspace()
{
}

void Workspace::buildUserInterface(bg::base::Context * context) {
	using namespace bg::gui;
	bg::system::Path path = bg::system::Path::AppDir();
	if (bg::system::currentPlatform() == bg::system::kMac) {
		path = bg::system::Path::ResourcesDir();
	}
	else {
		path.addComponent("data");
	}

	_skin = bg::db::loadSkin(context, path.pathAddingComponent("default.bg2skin"));

	_surface = new Surface(context, _skin.getPtr());
	_surface->setId("surface");

	initWindows();
}

void Workspace::initWindows() {
	_sceneWindow = new SceneWindow(_surface.getPtr());
	_toolBar = new ToolBar(_surface.getPtr());
	_statusBar = new StatusBar(_surface.getPtr());

	_sceneWindow->create();
	_toolBar->create();
	_statusBar->create();

	Config & cfg = App::Get().config();

	int sceneWindowWidth = cfg.getInt("sceneWindowWidth", 250);
	int toolBarHeight = cfg.getInt("toolBarHeight", 65);
	int statusBarHeight = cfg.getInt("statusBarHeight", 35);

	_sceneWindow->setAlignment(bg::gui::kAlignTopLeft);
	_sceneWindow->setAnchor(bg::gui::kAnchorBottom);
	_sceneWindow->setAlignmentMargin(bg::math::Size2Di(0, toolBarHeight));
	_sceneWindow->setAnchorMargin(bg::math::Size2Di(0, statusBarHeight));
	_sceneWindow->setSize(bg::math::Size2Di(sceneWindowWidth, 100));

	_toolBar->setAlignment(bg::gui::kAlignTopLeft);
	_toolBar->setAnchor(bg::gui::kAnchorRight);
	_toolBar->setSize(bg::math::Size2Di(100, toolBarHeight));

	_statusBar->setAlignment(bg::gui::kAlignBottomLeft);
	_statusBar->setAnchor(bg::gui::kAnchorRight);
	_statusBar->setSize(bg::math::Size2Di(100, statusBarHeight));
}

void Workspace::resize(const bg::math::Viewport & vp) {
	_surface->resize(vp.width(), vp.height());
}

void Workspace::frame(float delta, float uiScale) {
	_surface->setScale(uiScale);
}

void Workspace::draw() {
	_surface->draw();
}

}
}
