/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */


#ifndef _viewer_app_
#define _viewer_app_

//#include <scene/scene.hpp>

#include <bg/base/context.hpp>
#include <bg/wnd/window.hpp>

#include <config.hpp>
#include <scene.hpp>
#include <gui.hpp>
#include <event_handler.hpp>

namespace viewer {

class App {
public:
	static App & Get() { return s_app; }

	int run();

	void init(bg::base::Context *);

	void destroy();
	
	inline Config & config() { return *_mainConfig; }

	inline bg::base::Context * context() { return _ctx.getPtr(); }
	inline Scene * scene() { return _scene.getPtr(); }
	inline Gui * gui() { return _gui.getPtr(); }
	inline EventHandler * eventHandler() { return _eventHandler.getPtr(); }

	void windowRectChanged(const bg::math::Rect &);
	
protected:
	App();
	virtual ~App();
	
	static App s_app;

	Config * _mainConfig;
	bg::ptr<Scene> _scene;
	bg::ptr<Gui> _gui;
	bg::ptr<bg::base::Context> _ctx;
	bg::ptr<bg::wnd::Window> _window;
	bg::ptr<EventHandler> _eventHandler;

	void initActions();

	void loadPlugins();
};

}

#endif
