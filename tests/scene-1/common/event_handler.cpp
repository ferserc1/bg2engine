/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

//
//  event-handler.cpp
//  bg2e-ios
//
//  Created by Fernando Serrano Carpena on 3/5/16.
//  Copyright © 2016 Fernando Serrano Carpena. All rights reserved.
//

#include <event_handler.hpp>

#include <bg/bg2e.hpp>


class MyComponent : public bg::scene::Component {
public:
	Component * clone() { return new MyComponent(); }

	virtual void frame(float delta) {
		using namespace bg::scene;
		Transform * trx = node()->component<Transform>();
		if (trx) {
			trx->matrix().rotate(delta * bg::math::kPiOver4 * 0.1f, 0.0f, 1.0f, 0.0f);
		}
	}
};

class LightAnimation : public bg::scene::Component {
public:
	Component * clone() { return new LightAnimation(); }

	virtual void frame(float delta) {
		using namespace bg::scene;
		Transform * trx = node()->component<Transform>();
		if (trx) {
			trx->matrix().rotate(-delta * bg::math::kPiOver4 * 0.5f, 0.0f, 1.0f, 0.0f);
		}
	}
};

bg::scene::Drawable * createFloor(bg::base::Context * ctx) {
	using namespace bg::scene;
	bg::ptr<bg::scene::Drawable> drawable = bg::scene::PrimitiveFactory(ctx).plane(10.0f);
	
	bg::system::Path imagePath = bg::system::Path::AppDir();
	
	bg::ptr<bg::base::Texture> texture = bg::db::loadTexture(ctx, imagePath.pathAddingComponent("data/bricks.jpg"));
	bg::ptr<bg::base::Texture> nmapTex = bg::db::loadTexture(ctx, imagePath.pathAddingComponent("data/bricks_nm.png"));
	bg::ptr<bg::base::Texture> shinTex = bg::db::loadTexture(ctx, imagePath.pathAddingComponent("data/bricks_shin.jpg"));

	drawable->material(0)->setTexture(texture.getPtr());
	drawable->material(0)->setNormalMap(nmapTex.getPtr());
	drawable->material(0)->setShininessMask(shinTex.getPtr());
	drawable->material(0)->setShininess(85.0f);
	drawable->material(0)->setTextureScale(bg::math::Vector2(5.0f));
	drawable->material(0)->setNormalMapScale(bg::math::Vector2(5.0f));

	return drawable.release();
}

bg::scene::Node * createScene(bg::base::Context * ctx) {
	using namespace bg::scene;
	bg::ptr<Node> sceneRoot = new Node(ctx,"SceneRoot");
	
	bg::db::NodeLoader::RegisterPlugin(new bg::db::plugin::ReadPrefabBg2());
	
	bg::scene::Node * floor = new Node(ctx, "Floor");
	floor->addComponent(createFloor(ctx));
	floor->addComponent(new Transform(bg::math::Matrix4::Translation(0.0f, -0.5f, 0.0f)));
	sceneRoot->addChild(floor);


	bg::system::Path objectPath = bg::system::Path::AppDir();
	Node * cube = bg::db::loadPrefab(ctx, objectPath.pathAddingComponent("data/test-shape.bg2"));
	cube->addComponent(new MyComponent());
	sceneRoot->addChild(cube);
	
		

	Node * cam = new Node(ctx, "Camera");
	cam->addComponent(new bg::scene::Camera());
	bg::math::Matrix4 trx;
	trx.identity()
		.rotate(bg::math::trigonometry::degreesToRadians(22.5f), -1.0f, 0.0f, 0.0f)
		.translate(.0f, .0f, 5.0f);
	cam->addComponent(new bg::scene::Transform(trx));
	sceneRoot->addChild(cam);

	Node * light = new Node(ctx, "Light");
	light->addComponent(new bg::scene::Light());
	trx.identity()
		.rotate(bg::math::trigonometry::degreesToRadians(25.5f), -1.0f, 0.0f, 0.0f)
		.translate(.0f, .0f, 10.0f);
	light->addComponent(new bg::scene::Transform(trx));

	Node * lightAnimation = new Node(ctx, "Light animation");
	lightAnimation->addComponent(new LightAnimation());
	lightAnimation->addComponent(new Transform());
	lightAnimation->addChild(light);
	sceneRoot->addChild(lightAnimation);
	
	return sceneRoot.release();
}

MyEventHandler::MyEventHandler()
	:_frameVisitor(new bg::scene::FrameVisitor())
	,_drawVisitor(new bg::scene::DrawVisitor())
{
	
}

MyEventHandler::~MyEventHandler() {
	
}

void MyEventHandler::willCreateContext() {
	if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else if (bg::system::isDesktop()){
		throw bg::base::CompatibilityException("Fatal error: no suitable rendering engine found.");
	}
}

void MyEventHandler::initGL() {
	bg::Engine::Get()->initialize(context());
	
	_pipeline = new bg::base::Pipeline(context());
	
	bg::base::Pipeline::SetCurrent(_pipeline.getPtr());
	_pipeline->setDepthTest(true);

	_sceneRoot = createScene(context());
	_sceneRoot->someChild([&](bg::scene::Node * node) -> bool {
		if (node->name()=="Camera") {
			_cameraNode = node;
			return true;
		}
		return false;
	});
		
	bg::fx::Forward * fx = new bg::fx::Forward(context());
	_pipeline->setEffect(fx);

	_drawVisitor->setPipeline(_pipeline.getPtr());
}

void MyEventHandler::reshape(int w, int h) {
	using namespace bg::math;
	_pipeline->setViewport(Viewport(0,0,w,h));
	_pipeline->effect()->matrixState().projectionMatrixStack()
			.perspective(Scalar(45,trigonometry::deg), _pipeline->viewport().aspectRatio(), 0.1f, 100.0f);
}

void MyEventHandler::frame(float delta) {
	_frameVisitor->setDelta(delta);
	_sceneRoot->accept(_frameVisitor.getPtr());

	bg::scene::Camera * cam = _cameraNode.valid() ? _cameraNode->component<bg::scene::Camera>():nullptr;
	if (cam) {
		_pipeline->effect()->matrixState()
			.viewMatrixStack()
			.set(cam->viewMatrix());
	}

	bg::fx::Forward * fx = dynamic_cast<bg::fx::Forward*>(_pipeline->effect());
	if (fx) {
		for(auto lc : bg::scene::Light::ActiveLights()) {
			fx->lightData().addLight(lc->light(), lc->transform(), nullptr);
		}
	}
}

void MyEventHandler::draw() {
	_pipeline->clearBuffers(bg::base::ClearBuffers::kColorDepth);
	
	_sceneRoot->accept(_drawVisitor.getPtr());
	
	context()->swapBuffers();
}

void MyEventHandler::onMemoryWarning() {
	std::cout << "Memory warning received" << std::endl;
}

// Mobile touch events
void MyEventHandler::touchStart(const bg::base::TouchEvent & evt)  {
}

void MyEventHandler::touchMove(const bg::base::TouchEvent & evt)  {
}

void MyEventHandler::touchEnd(const bg::base::TouchEvent & evt) {
}


// Desktop events
void MyEventHandler::keyUp(const bg::base::KeyboardEvent & evt) {
	if (evt.keyboard().key() == bg::base::Keyboard::kKeyEsc) {
		bg::wnd::MainLoop::Get()->quit(0);
	}
}

void MyEventHandler::mouseDown(const bg::base::MouseEvent & evt) {
}

void MyEventHandler::mouseDrag(const bg::base::MouseEvent & evt) {
}

