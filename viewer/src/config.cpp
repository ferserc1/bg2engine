/*
*	bg2 engine license
*	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
*
*	Permission is hereby granted, free of charge, to any person obtaining a copy
*	of this software and associated documentation files (the "Software"), to deal
*	in the Software without restriction, including without limitation the rights
*	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
*	of the Software, and to permit persons to whom the Software is furnished to do
*	so, subject to the following conditions:
*
*	The above copyright notice and this permission notice shall be included in all
*	copies or substantial portions of the Software.
*
*	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
*	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
*	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
*	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
*	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
*	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
*/

#include <config.hpp>

#include <bg/system/system.hpp>

namespace viewer {

Config::Config(const bg::system::Path & path)
{
	_savePath = path;
}

Config::~Config() {
}

void Config::load() {
	using namespace bg::db::json;
	if (!_savePath.exists()) {
		_config = new Value(Value::kObject);
	}
	else {
		_config = Parser::ParseFile(_savePath);
	}
}

void Config::save() {
	using namespace bg::db::json;
	std::ofstream file;
	file.open(_savePath.text());
	if (file.good()) {
		_config->writeToStream(file);
		file.close();
	}
}

void Config::set(const std::string & key, const std::string & value) {
	_config->setValue(key, value);
}

void Config::set(const std::string & key, bool value) {
	_config->setValue(key, value);
}

void Config::set(const std::string & key, int value) {
	_config->setValue(key, value);
}

void Config::set(const std::string & key, float value) {
	_config->setValue(key, value);
}

void Config::set(const std::string & key, const bg::math::Vector2 & value) {
	_config->setValue(key, value);
}

void Config::set(const std::string & key, const bg::math::Vector3 & value) {
	_config->setValue(key, value);
}

void Config::set(const std::string & key, const bg::math::Vector4 & value) {
	_config->setValue(key, value);
}

void Config::set(const std::string & key, const bg::math::Vector2i & value) {
	_config->setValue(key, value);
}

void Config::set(const std::string & key, const bg::math::Vector3i & value) {
	_config->setValue(key, value);
}

void Config::set(const std::string & key, const bg::math::Vector4i & value) {
	_config->setValue(key, value);
}

void Config::set(const std::string & key, const bg::math::Matrix3 & value) {
	_config->setValue(key, value);
}

void Config::set(const std::string & key, const bg::math::Matrix4 & value) {
	_config->setValue(key, value);
}

std::string Config::getString(const std::string & key, const std::string & def) {
	return bg::db::json::Value::String((*_config)[key], def);
}

bool Config::getBool(const std::string & key, bool def) {
	return bg::db::json::Value::Bool((*_config)[key], def);
}

int Config::getInt(const std::string & key, int def) {
	return bg::db::json::Value::Int((*_config)[key], def);
}

float Config::getFloat(const std::string & key, float def) {
	return bg::db::json::Value::Float((*_config)[key], def);
}

bg::math::Vector2 Config::getVector2(const std::string & key, const bg::math::Vector2 & def) {
	return bg::db::json::Value::Vec2((*_config)[key], def);
}

bg::math::Vector3 Config::getVector3(const std::string & key, const bg::math::Vector3 & def) {
	return bg::db::json::Value::Vec3((*_config)[key], def);
}

bg::math::Vector4 Config::getVector4(const std::string & key, const bg::math::Vector4 & def) {
	return bg::db::json::Value::Vec4((*_config)[key], def);
}

bg::math::Vector2i Config::getVector2i(const std::string & key, const bg::math::Vector2i & def) {
	return bg::db::json::Value::Vec2i((*_config)[key], def);
}

bg::math::Vector3i Config::getVector3i(const std::string & key, const bg::math::Vector3i & def) {
	return bg::db::json::Value::Vec3i((*_config)[key], def);
}

bg::math::Vector4i Config::getVector4i(const std::string & key, const bg::math::Vector4i & def) {
	return bg::db::json::Value::Vec4i((*_config)[key], def);
}

bg::math::Matrix3 Config::getMatrix3(const std::string & key, const bg::math::Matrix3 & def) {
	return bg::db::json::Value::Mat3((*_config)[key], def);
}

bg::math::Matrix4 Config::getMatrix4(const std::string & key, const bg::math::Matrix4 & def) {
	return bg::db::json::Value::Mat4((*_config)[key], def);
}

}
