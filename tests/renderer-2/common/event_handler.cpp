/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

//
//  event-handler.cpp
//  bg2e-ios
//
//  Created by Fernando Serrano Carpena on 3/5/16.
//  Copyright © 2016 Fernando Serrano Carpena. All rights reserved.
//

#include <event_handler.hpp>

#include <bg/engine.hpp>
#include <bg/bg2e.hpp>

class MyComponent : public bg::scene::Component {
public:
	Component * clone() { return new MyComponent(); }

	virtual void frame(float delta) {
		using namespace bg::scene;
		Transform * trx = node()->component<Transform>();
		if (trx) {
			trx->matrix().rotate(delta * bg::math::kPiOver4 * 0.5f, 0.0f, 1.0f, 0.0f);
		}
	}
};

class LightAnimation : public bg::scene::Component {
public:
	Component * clone() { return new LightAnimation(); }
	virtual void frame(float delta) {
		using namespace bg::scene;
		Transform * trx = node()->component<Transform>();
		if (trx) {
			trx->matrix().rotate(-delta * bg::math::kPiOver4 * 0.2f, 0.0f, 1.0f, 0.0f);
		}
	}
};

bg::scene::Drawable * createFloor(bg::base::Context * ctx) {
	using namespace bg::scene;
	bg::ptr<bg::scene::Drawable> drawable = bg::scene::PrimitiveFactory(ctx).plane(10.0f);
	
	bg::system::Path imagePath = bg::system::Path::AppDir();
	
	bg::ptr<bg::base::Texture> texture = bg::db::loadTexture(ctx, imagePath.pathAddingComponent("data/bricks.jpg"));
	bg::ptr<bg::base::Texture> nmapTex = bg::db::loadTexture(ctx, imagePath.pathAddingComponent("data/bricks_nm.png"));
	bg::ptr<bg::base::Texture> shinTex = bg::db::loadTexture(ctx, imagePath.pathAddingComponent("data/bricks_shin.jpg"));

	drawable->material(0)->setTexture(texture.getPtr());
	//drawable->material(0)->setNormalMap(nmapTex.getPtr());
	drawable->material(0)->setShininessMask(shinTex.getPtr());
	drawable->material(0)->setShininess(85.0f);
	drawable->material(0)->setTextureScale(bg::math::Vector2(5.0f));
	drawable->material(0)->setNormalMapScale(bg::math::Vector2(5.0f));
	drawable->material(0)->setReflectionAmount(1.0f);
	drawable->material(0)->setReflectionMask(shinTex.getPtr());
	drawable->material(0)->setRoughness(0.4);
	drawable->material(0)->setRoughnessMask(shinTex.getPtr());

	return drawable.release();
}

bg::scene::Drawable * createCube(bg::base::Context * ctx) {
	using namespace bg::scene;
	bg::ptr<bg::scene::Drawable> drawable = bg::scene::PrimitiveFactory(ctx).cube();

	bg::system::Path imagePath = bg::system::Path::AppDir();
	bg::ptr<bg::base::Texture> texture = bg::db::loadTexture(ctx, imagePath.pathAddingComponent("data/texture.jpg"));

	drawable->material(0)->setTexture(texture.getPtr());
	drawable->material(0)->setUnlit(true);

	return drawable.release();
}

bg::scene::Node * MyEventHandler::createScene(bg::base::Context * ctx) {
	using namespace bg::scene;
	bg::ptr<Node> sceneRoot = new Node(ctx,"SceneRoot");
	
	bg::system::Path objectPath = bg::system::Path::AppDir();

	bg::scene::Cubemap * cm = new bg::scene::Cubemap();
	
	cm->setImageFile(bg::base::Texture::CubemapFace::kFacePositiveX, objectPath.pathAddingComponent("data/posx.jpg").text());
	cm->setImageFile(bg::base::Texture::CubemapFace::kFaceNegativeX, objectPath.pathAddingComponent("data/negx.jpg").text());
	cm->setImageFile(bg::base::Texture::CubemapFace::kFacePositiveY, objectPath.pathAddingComponent("data/posy.jpg").text());
	cm->setImageFile(bg::base::Texture::CubemapFace::kFaceNegativeY, objectPath.pathAddingComponent("data/negy.jpg").text());
	cm->setImageFile(bg::base::Texture::CubemapFace::kFacePositiveZ, objectPath.pathAddingComponent("data/posz.jpg").text());
	cm->setImageFile(bg::base::Texture::CubemapFace::kFaceNegativeZ, objectPath.pathAddingComponent("data/negz.jpg").text());
	cm->loadCubemap(ctx);
	sceneRoot->addComponent(cm);
	
	bg::db::NodeLoader::RegisterPlugin(new bg::db::plugin::ReadPrefabBg2());
	bg::db::NodeWriter::RegisterPlugin(new bg::db::plugin::WritePrefabBg2());
	bg::db::DrawableWriter::RegisterPlugin(new bg::db::plugin::WriteDrawableBg2());
	
	Node * cube = bg::db::loadPrefab(ctx, objectPath.pathAddingComponent("data/test-shape.bg2"));
	cube->component<Drawable>()->material(0)->setReflectionAmount(0.4f);
	sceneRoot->addChild(cube);

	bg::db::writeDrawable(ctx, objectPath.pathAddingComponent("data/test-shape-prefab.bg2"), cube->drawable());

	Node * sphere = new Node(ctx,"sphere");
	sphere->addComponent(PrimitiveFactory(ctx).sphere(bg::math::Scalar(40,bg::math::distance::cm)));
	sphere->addComponent(new Transform(bg::math::Matrix4::Translation(0.0f, -0.2f, 1.0f)));
	sphere->component<Drawable>()->material(0)->setDiffuse(bg::math::Color(0.54f,0.81f,1.0f,0.76f));
	sphere->component<Drawable>()->material(0)->setShininess(9.0f);
	sphere->component<Drawable>()->material(0)->setReflectionAmount(0.5f);
	sceneRoot->addChild(sphere);
	
	bg::scene::Node * floor = new Node(ctx,"Floor");
	floor->addComponent(createFloor(ctx));
	_floorMaterial = floor->component<bg::scene::Drawable>()->material(0);
	floor->addComponent(new Transform(bg::math::Matrix4::Translation(0.0f, -0.5f, 0.0f)));
	sceneRoot->addChild(floor);
	
	bg::scene::Node * cube2 = new Node(ctx, "Cube2");
	cube2->addComponent(new Transform(bg::math::Matrix4::Translation(-1.5f, 0.0f, 0.0f)));
	cube2->addComponent(createCube(ctx));
	sceneRoot->addChild(cube2);

	Node * cam = new Node(ctx, "Camera");
	cam->addComponent(new bg::scene::Camera());
	bg::math::Matrix4 trx;
	trx.identity()
		.rotate(bg::math::trigonometry::degreesToRadians(22.5f), -1.0f, 0.0f, 0.0f)
		.translate(.0f, .0f, 5.0f);
	cam->addComponent(new bg::scene::Transform(trx));
	cam->component<bg::scene::Camera>()->setFocus(5.0f);	// Set the focus at the same distance as the transform is
	sceneRoot->addChild(cam);
	bg::scene::OpticalProjectionStrategy * projectionStrategy = new bg::scene::OpticalProjectionStrategy();
	projectionStrategy->setFrameSize(35.0f);	// 35mm film
	projectionStrategy->setFocalLength(50.0f);	// 50mm focal length
	projectionStrategy->setFar(600.0f);
	cam->component<bg::scene::Camera>()->setProjectionStrategy(projectionStrategy);
	cam->addComponent(new bg::manipulation::OrbitNodeController());

	Node * lightAnimation = new Node(ctx, "Light animation");
	lightAnimation->addComponent(new LightAnimation());
	lightAnimation->addComponent(new Transform());
	sceneRoot->addChild(lightAnimation);

	Node * light = new Node(ctx, "Light");
	Light * lightComp = new Light();
	light->addComponent(lightComp);
	lightComp->light()->setType(bg::base::Light::kTypePoint);
	lightComp->light()->setAmbient(bg::math::Color(0.1f,0.1f,0.14f,1.0f));
	lightComp->light()->setDiffuse(bg::math::Color(0.1f,0.9f,0.6f,1.0f));
	lightComp->light()->setSpecular(bg::math::Color(0.5f, 0.9f, 0.7f, 1.0f));
	trx.identity()
	.translate(2.0f, 0.1f, 0.0f);
	light->addComponent(new bg::scene::Transform(trx));
	lightAnimation->addChild(light);
	
	Node * light2 = new Node(ctx,"Light2");
	Light * lightComp2 = new Light();
	light2->addComponent(lightComp2);
	lightComp2->light()->setType(bg::base::Light::kTypePoint);
	lightComp2->light()->setAmbient(bg::math::Color::Black());
	lightComp2->light()->setDiffuse(bg::math::Color(0.2f,0.2f,1.0f,98.0f));
	lightComp2->light()->setSpecular(bg::math::Color(0.5f, 0.5f, 1.0f, 1.0f));
	light2->addComponent(new  bg::scene::Transform(bg::math::Matrix4::Translation(-2.0f, 0.1f, 0.0f)));
	lightAnimation->addChild(light2);
	
	Node * light3 = new Node(ctx, "Light");
	Light * lightComp3 = new Light();
	light3->addComponent(lightComp3);
	lightComp3->light()->setDiffuse(bg::math::Color(0.85f,0.8f,0.8f,1.0f));
	lightComp3->light()->setSpecular(bg::math::Color(0.55f, 0.5f, 0.5f, 1.0f));
	lightComp3->light()->setAmbient(bg::math::Color(0.0f, 0.0f, 0.0f, 1.0f));
	lightComp3->light()->setShadowStrength(1.0f);
	trx.identity()
		.rotate(bg::math::trigonometry::degreesToRadians(25.5f), -1.0f, 0.0f, 0.0f)
		.translate(.0f, .0f, 2.0f);
	light3->addComponent(new bg::scene::Transform(trx));
	lightAnimation->addChild(light3);

	Node * light4 = new Node(ctx, "Light4");
	Light * lightComp4 = new Light();
	light4->addComponent(lightComp4);
	lightComp4->light()->setType(bg::base::Light::kTypePoint);
	lightComp4->light()->setAmbient(bg::math::Color::Black());
	lightComp4->light()->setDiffuse(bg::math::Color(0.2f, 1.0f, 0.1f, 98.0f));
	lightComp4->light()->setSpecular(bg::math::Color(0.5f, 1.0f, 1.0f, 1.0f));
	light4->addComponent(new  bg::scene::Transform(bg::math::Matrix4::Translation(0.0f, 0.1f, -2.0f)));
	lightAnimation->addChild(light4);
	
	Node * light5 = new Node(ctx, "Light5");
	Light * lightComp5 = new Light();
	light5->addComponent(lightComp5);
	lightComp5->light()->setType(bg::base::Light::kTypePoint);
	lightComp5->light()->setAmbient(bg::math::Color::Black());
	lightComp5->light()->setDiffuse(bg::math::Color(1.0f, 0.3f, 0.1f, 98.0f));
	lightComp5->light()->setSpecular(bg::math::Color(1.0f, 0.5f, 0.0f, 1.0f));
	light5->addComponent(new  bg::scene::Transform(bg::math::Matrix4::Translation(0.0f, 0.1f, 2.0f)));
	lightAnimation->addChild(light5);


	return sceneRoot.release();
}

MyEventHandler::MyEventHandler()
{
	
}

MyEventHandler::~MyEventHandler() {
	
}

void MyEventHandler::willCreateContext() {
	if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else if (bg::system::isDesktop()){
		throw bg::base::CompatibilityException("Fatal error: no suitable rendering engine found.");
	}
}

void MyEventHandler::initGL() {
	bg::Engine::Get()->initialize(context());
	//context()->setVsyncEnabled(true);
	
	_inputVisitor = new bg::scene::InputVisitor;

	_renderer = bg::render::Renderer::Create(context(), bg::render::Renderer::kRenderPathDeferred);
	_renderer->setClearColor(bg::math::Color::Blue());
	
	// Render settings method 1: get the setting class (ShadowMap) and use the accessor
	bg::render::ShadowMap * shadowMap = _renderer->settings<bg::render::ShadowMap>();
	if (shadowMap) {
		shadowMap->setShadowType(bg::render::ShadowMap::kSoftShadows);
	}

	// Render settings method 2 (preferred): use de setRenderSetting() functions
	//	The settings keys are defined in bg::render::settings namespace, and they are
	//	bg::render::SettingsKey (aka std::string). The keys are defined in the settings
	//	namespace to reduce the risk of misspelling, but you can also specify the
	//	key as a string:  bg::render::settings::shadowMap == "shadowMap"
	using namespace bg::render::settings;
	_renderer->setRenderSetting(kAmbientOcclusion, kKernelSize, 16);
	_renderer->setRenderSetting(kShadowMap, kShadowMapSize, bg::math::Size2Di(2048));
	_renderer->setRenderSetting(kAmbientOcclusion, kBlur, 1);
	_renderer->setRenderSetting(kAmbientOcclusion, kQuality, bg::base::kQualityMedium);
	_renderer->setRenderSetting(kRaytracer, kEnabled, true);
	_renderer->setRenderSetting(kRaytracer, kQuality, bg::base::kQualityExtreme);
	_renderer->setRenderSetting(kRaytracer, kScale, 0.4f);
	
	_sceneRoot = createScene(context());
	_sceneRoot->someChild([&](bg::scene::Node * node) -> bool {
		return (_camera = node->component<bg::scene::Camera>()).valid();
	});
}

void MyEventHandler::reshape(int w, int h) {
	_camera->setViewport(bg::math::Viewport(0,0,w,h));
}

void MyEventHandler::frame(float delta) {
	_renderer->frame(_sceneRoot.getPtr(), delta);
	_fpsCounter.frame(delta);
	if (_fpsCounter.valueUpdated()) {
		bg::log(bg::log::kDebug) << "FPS: " << _fpsCounter.fps() << bg::endl;
	}
}

void MyEventHandler::draw() {
	_renderer->draw(_sceneRoot.getPtr(), _camera.getPtr());
	context()->swapBuffers();
}

void MyEventHandler::onMemoryWarning() {
	std::cout << "Memory warning received" << std::endl;
}

// Mobile touch events
void MyEventHandler::touchStart(const bg::base::TouchEvent & evt)  {
	_inputVisitor->touchStart(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::touchMove(const bg::base::TouchEvent & evt)  {
	_inputVisitor->touchMove(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::touchEnd(const bg::base::TouchEvent & evt) {
	_inputVisitor->touchEnd(_sceneRoot.getPtr(), evt);
}


// Desktop events
void MyEventHandler::keyUp(const bg::base::KeyboardEvent & evt) {
	if (evt.keyboard().key() == bg::base::Keyboard::kKeyEsc) {
		bg::wnd::MainLoop::Get()->quit(0);
	}
	else if (evt.keyboard().key() == bg::base::Keyboard::kKeyAdd) {
		using namespace bg::render::settings;
		_renderer->setRenderSetting(kAmbientOcclusion, kKernelSize, 32);
		_renderer->setRenderSetting(kRaytracer, kQuality, bg::base::kQualityExtreme);
		_renderer->setRenderSetting(kShadowMap, kShadowMapSize, bg::math::Size2Di(2048));
		_renderer->setRenderSetting(kShadowMap, kShadowType, 1);
		_renderer->setRenderSetting(kAmbientOcclusion, kBlur, 4);
		_renderer->setRenderSetting(kAmbientOcclusion, kQuality, bg::base::kQualityHigh);
	}
	else if (evt.keyboard().key() == bg::base::Keyboard::kKeySub) {
		using namespace bg::render::settings;
		_renderer->setRenderSetting(kAmbientOcclusion, kKernelSize, 8);
		_renderer->setRenderSetting(kRaytracer, kQuality, bg::base::kQualityHigh);
		_renderer->setRenderSetting(kShadowMap, kShadowMapSize, bg::math::Size2Di(512));
		_renderer->setRenderSetting(kShadowMap, kShadowType, 0);
		_renderer->setRenderSetting(kAmbientOcclusion, kBlur, 1);
		_renderer->setRenderSetting(kAmbientOcclusion, kQuality, bg::base::kQualityMedium);
	}
	else if (evt.keyboard().key() == bg::base::Keyboard::kKeySpace) {
		using namespace bg::render::settings;
		bool enabled = _renderer->boolSettingValue(kDebug,kEnabled);
		_renderer->setRenderSetting(kDebug,kEnabled,!enabled);
	}
	else if (evt.keyboard().key() == bg::base::Keyboard::kKeyReturn) {
		bg::wnd::Window * wnd = bg::wnd::MainLoop::Get()->window();
		bg::math::Rect rect = wnd->rect();
		bg::system::MouseCursor::SetPosition(bg::math::Position2Di(rect.x() + rect.width() / 2, rect.y() + rect.height() / 2));
	}
	else if (evt.keyboard().key() == bg::base::Keyboard::kKey0) {
		_floorMaterial->setRoughness(0.0f);
	}
	else if (evt.keyboard().key() == bg::base::Keyboard::kKey1) {
		_floorMaterial->setRoughness(0.1f);
	}
	else if (evt.keyboard().key() == bg::base::Keyboard::kKey2) {
		_floorMaterial->setRoughness(0.2f);
	}
	else if (evt.keyboard().key() == bg::base::Keyboard::kKey3) {
		_floorMaterial->setRoughness(0.3f);
	}
	else if (evt.keyboard().key() == bg::base::Keyboard::kKey4) {
		_floorMaterial->setRoughness(0.4f);
	}
	else if (evt.keyboard().key() == bg::base::Keyboard::kKey5) {
		_floorMaterial->setRoughness(0.5f);
	}
	else if (evt.keyboard().key() == bg::base::Keyboard::kKey6) {
		_floorMaterial->setRoughness(0.6f);
	}
	else if (evt.keyboard().key() == bg::base::Keyboard::kKey7) {
		_floorMaterial->setRoughness(0.7f);
	}
	else if (evt.keyboard().key() == bg::base::Keyboard::kKey8) {
		_floorMaterial->setRoughness(0.8f);
	}
	else if (evt.keyboard().key() == bg::base::Keyboard::kKey9) {
		_floorMaterial->setRoughness(1.0f);
	}
}

void MyEventHandler::mouseDown(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseDown(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::mouseMove(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseMove(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::mouseDrag(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseDrag(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::mouseUp(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseUp(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::mouseWheel(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseWheel(_sceneRoot.getPtr(), evt);
}

