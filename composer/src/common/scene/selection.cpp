/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <scene/selection.hpp>

#include <bg/math/math.hpp>

namespace composer {
namespace scene {

Selection::Selection(bg::base::Context * ctx)
	:ContextObject(ctx)
{
	_mousePicker = new bg::manipulation::MousePicker(context());
}

Selection::~Selection() {

}

void Selection::selectionStart(bg::scene::Node *, bg::scene::Camera *, const bg::math::Position2Di & pos) {
	_startPoint = pos;
}

void Selection::selectionEnd(bg::scene::Node * sceneRoot, bg::scene::Camera * camera, const bg::math::Position2Di & pos) {
	using namespace bg::math;
	using namespace bg::manipulation;
	using namespace bg::scene;
	Position2Di endPos = pos;

	if (bg::math::abs(_startPoint.distance(endPos)) < 3) {
		bg::ptr<SelectableItem> picked = _mousePicker->pick(sceneRoot, camera, endPos);
		if (picked.valid() && picked->valid()) {
			itemPicked(picked.getPtr());
		}
	}
}

void Selection::itemPicked(bg::manipulation::SelectableItem * item) {
	if (_selectionStrategy.valid()) {
		_selectionStrategy->itemPicked(this, item);
	}
}

}
}