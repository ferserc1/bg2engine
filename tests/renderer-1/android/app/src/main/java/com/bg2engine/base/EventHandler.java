package com.bg2engine.base;

import android.content.res.AssetManager;

public class EventHandler {
    static {
        System.loadLibrary("bg2e-glue");
    }

    public static native void init(AssetManager mgr, String internalStoragePath, String externalStoragePath, int displayWidth, int displayHeight, float displayDensity);
    public static native void reshape(int w, int h);
    public static native void draw();
    public static native void touchEvent1(int evt, float x0, float y0);
    public static native void touchEvent2(int evt, float x0, float y0, float x1, float y1);
    public static native void touchEvent3(int evt, float x0, float y0, float x1, float y1, float x2, float y2);
    public static native void touchEvent4(int evt, float x0, float y0, float x1, float y1, float x2, float y2, float x3, float y3);
    public static native void accelerationEvent(float x, float y, float z);
}
