/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <command/manager.hpp>

#include <bg/wnd/wnd.hpp>

namespace composer {
namespace command {

bool Manager::executeCommand(Command * cmd) {
	bool status = true;
	try {
		cmd->execute();
		_undoneCommands.clear();
		_commandStack.push_back(cmd);
	}
	catch (std::runtime_error & err) {
		using namespace bg::wnd;
		MessageBox::Show(MainLoop::Get()->window(), "Error", std::string("Could not execute command: ") + err.what());
		status = false;
	}
	return status;
}

bool Manager::undo() {
	bool status = true;
	if (_commandStack.size()>0) {
		try {
			bg::ptr<Command> cmd = _commandStack.back().getPtr();
			cmd->undo();
			_commandStack.pop_back();
			_undoneCommands.push_back(cmd.getPtr());
		}
		catch (std::runtime_error & err) {
			using namespace bg::wnd;
			MessageBox::Show(MainLoop::Get()->window(), "Error", std::string("Could not undo: ") + err.what());
			status = false;
		}
	}
	return status;
}

bool Manager::redo() {
	bool status = true;
	if (_undoneCommands.size()>0) {
		try {
			bg::ptr<Command> cmd = _undoneCommands.back();
			cmd->execute();
			_undoneCommands.pop_back();
			_commandStack.push_back(cmd.getPtr());
		}
		catch (std::runtime_error & err) {
			using namespace bg::wnd;
			MessageBox::Show(MainLoop::Get()->window(), "Error", std::string("Could not redo: ") + err.what());
			status = false;
		}
	}
	return status;
}

void Manager::cleanUp() {
	_commandStack.clear();
	_undoneCommands.clear();
}

}
}
