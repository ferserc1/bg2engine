/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

//
//  bg2e-viewer event-handler.cpp

#include <event_handler.hpp>

#include <app.hpp>

#include <bg/engine.hpp>
#include <bg/wnd/main_loop.hpp>
#include <bg/wnd/file_dialog.hpp>

#include <bg/scene/find_component_visitor.hpp>
#include <bg/scene/dynamics.hpp>

#include <bg/system/system.hpp>

#include <bg/engine/opengl_es_3.hpp>
#include <bg/engine/opengl_core.hpp>
#include <bg/engine/directx11.hpp>

#include <bg/render/render.hpp>

namespace viewer {

EventHandler::EventHandler()
{
	
}

EventHandler::~EventHandler() {
	
}

void EventHandler::setForwardRender() {
	_renderer = _forwardRenderer.getPtr();
	App::Get().config().set("renderPath", "forward");
}

void EventHandler::setDeferredRender() {
	_renderer = _deferredRenderer.getPtr();
	App::Get().config().set("renderPath", "deferred");
}

void EventHandler::setRaytracerQuality(int quality) {
	using namespace bg::render::settings;
	switch (quality) {
	case 0:
		_deferredRenderer->setRenderSetting(kRaytracer, kScale, 0.25f);
		break;
	case 1:
		_deferredRenderer->setRenderSetting(kRaytracer, kScale, 0.5f);
		break;
	case 2:
		_deferredRenderer->setRenderSetting(kRaytracer, kScale, 0.75f);
		break;
	case 3:
		_deferredRenderer->setRenderSetting(kRaytracer, kScale, 1.0f);
		break;
	}
	App::Get().config().set("ssrtQuality", quality);
}

void EventHandler::setSSAOQuality(int quality) {
	using namespace bg::render::settings;
	switch (quality) {
	case 0:
		_deferredRenderer->setRenderSetting(kAmbientOcclusion, kKernelSize, 8);
		_deferredRenderer->setRenderSetting(kAmbientOcclusion, kBlur, 1);
		_deferredRenderer->setRenderSetting(kAmbientOcclusion, kQuality, bg::base::kQualityLow);
		break;
	case 1:
		_deferredRenderer->setRenderSetting(kAmbientOcclusion, kKernelSize, 16);
		_deferredRenderer->setRenderSetting(kAmbientOcclusion, kBlur, 1);
		_deferredRenderer->setRenderSetting(kAmbientOcclusion, kQuality, bg::base::kQualityMedium);
		break;
	case 2:
		_deferredRenderer->setRenderSetting(kAmbientOcclusion, kKernelSize, 32);
		_deferredRenderer->setRenderSetting(kAmbientOcclusion, kBlur, 2);
		_deferredRenderer->setRenderSetting(kAmbientOcclusion, kQuality, bg::base::kQualityHigh);
		break;
	case 3:
		_deferredRenderer->setRenderSetting(kAmbientOcclusion, kKernelSize, 32);
		_deferredRenderer->setRenderSetting(kAmbientOcclusion, kBlur, 4);
		_deferredRenderer->setRenderSetting(kAmbientOcclusion, kQuality, bg::base::kQualityExtreme);
		break;
	}
	App::Get().config().set("ssaoQuality", quality);
}

int EventHandler::raytracerQuality() {
	return App::Get().config().getInt("ssrtQuality", 1);
}

int EventHandler::ssaoQuality() {
	return App::Get().config().getInt("ssaoQuality", 1);
}

void EventHandler::willCreateContext() {
	if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else if (bg::system::isDesktop()){
		throw bg::base::CompatibilityException("Fatal error: no suitable rendering engine found.");
	}
}

void EventHandler::initGL() {
	bg::Engine::Get()->initialize(context());

	_inputVisitor = new bg::scene::InputVisitor();
	
	_forwardRenderer = bg::render::Renderer::Create(context(), bg::render::Renderer::kRenderPathForward);
	_deferredRenderer = bg::render::Renderer::Create(context(), bg::render::Renderer::kRenderPathDeferred);

	setRaytracerQuality(App::Get().config().getInt("ssrtQuality", 1));
	setSSAOQuality(App::Get().config().getInt("ssaoQuality", 1));
	if (App::Get().config().getString("renderPath", "forward") == "deferred") {
		setDeferredRender();
	}
	else {
		setForwardRender();
	}
    
    using namespace bg::render::settings;
    _deferredRenderer->setRenderSetting(kShadowMap, kShadowMapSize, bg::math::Size2Di(4096));
    _forwardRenderer->setRenderSetting(kShadowMap, kShadowMapSize, bg::math::Size2Di(4096));

	App::Get().init(context());
}

void EventHandler::willDestroyContext() {
	stop();

	_forwardRenderer = nullptr;
	_deferredRenderer = nullptr;
	_renderer = nullptr;
	_inputVisitor = nullptr;

	App::Get().destroy();

	bg::base::EventHandler::willDestroyContext();
}

void EventHandler::reshape(int w, int h) {
	if (App::Get().scene()->ready()) {
		_viewport = bg::math::Viewport(0, 0, w, h);
		bg::scene::Camera * cam = App::Get().scene()->mainCamera();
		cam->setViewport(_viewport);
		if (!cam->projectionStrategy()) {
			cam->projection().perspective(50.0f, _viewport.aspectRatio(), 0.2f, 1000.0f);
		}
	}
	App::Get().gui()->resize(w, h);
}

void EventHandler::frame(float delta) {
	if (App::Get().scene()->ready()) {
		_renderer->frame(App::Get().scene()->sceneRoot(), delta);
	}
	App::Get().gui()->frame(delta);
}

void EventHandler::draw() {
	if (App::Get().scene()->ready()) {
		_renderer->draw(App::Get().scene()->sceneRoot(), App::Get().scene()->mainCamera());
	}
	App::Get().gui()->draw();
	context()->swapBuffers();
}

// Desktop events
void EventHandler::keyUp(const bg::base::KeyboardEvent & evt) {
	if (!App::Get().gui()->keyUp(evt)) {
		_inputVisitor->keyUp(App::Get().scene()->sceneRoot(), evt);
	}
}

void EventHandler::keyDown(const bg::base::KeyboardEvent & evt) {
	if (!App::Get().gui()->keyDown(evt)) {
		_inputVisitor->keyDown(App::Get().scene()->sceneRoot(), evt);
	}
}

void EventHandler::mouseDown(const bg::base::MouseEvent & evt) {
	if (!App::Get().gui()->mouseDown(evt)) {
		_inputVisitor->mouseDown(App::Get().scene()->sceneRoot(), evt);
	}
}

void EventHandler::mouseDrag(const bg::base::MouseEvent & evt) {
	if (!App::Get().gui()->mouseDrag(evt)) {
		_inputVisitor->mouseDrag(App::Get().scene()->sceneRoot(), evt);
	}
}

void EventHandler::mouseUp(const bg::base::MouseEvent & evt) {
	if (!App::Get().gui()->mouseUp(evt)) {
		_inputVisitor->mouseUp(App::Get().scene()->sceneRoot(), evt);
	}
}

void EventHandler::mouseMove(const bg::base::MouseEvent & evt) {
	if (!App::Get().gui()->mouseMove(evt)) {
		_inputVisitor->mouseMove(App::Get().scene()->sceneRoot(), evt);
	}
}

void EventHandler::mouseWheel(const bg::base::MouseEvent & evt) {
	if (!App::Get().gui()->mouseWheel(evt)) {
		_inputVisitor->mouseWheel(App::Get().scene()->sceneRoot(), evt);
	}
}

void EventHandler::buildMenu(bg::wnd::MenuDescriptor & menu) {
	bg::wnd::PopUpMenu * file = bg::wnd::PopUpMenu::New("File");
	file->addMenuItem({ bg::wnd::kCodeOpen, "Open" });
	file->addMenuItem({ bg::wnd::kCodeCustom + 100, "Graphic Settings" });


	if (bg::system::currentPlatform() != bg::system::kMac) {
		file->addMenuItem({ bg::wnd::kCodeQuit, "Quit" });
	}

	menu.push_back(file);

	bg::wnd::PopUpMenu * physics = bg::wnd::PopUpMenu::New("Physics");
	physics->addMenuItem({ bg::wnd::kCodeCustom + 200, "Play" });
	physics->addMenuItem({ bg::wnd::kCodeCustom + 201, "Pause" });
	physics->addMenuItem({ bg::wnd::kCodeCustom + 202, "Stop" });

	menu.push_back(physics);
}

void EventHandler::menuSelected(const std::string & title, int32_t identifier) {
	switch (identifier) {
	case bg::wnd::kCodeOpen:
		openFile();
		break;
	case bg::wnd::kCodeQuit:
		bg::wnd::MainLoop::Get()->quit(0);
		break;
	case bg::wnd::kCodeCustom + 100:
		App::Get().gui()->configUI()->show();
		break;

	case bg::wnd::kCodeCustom + 200:
		play();
		break;
	case bg::wnd::kCodeCustom + 201:
		pause();
		break;
	case bg::wnd::kCodeCustom + 202:
		stop();
		break;
	}
}

void EventHandler::windowRectChanged(int x, int y, int w, int h) {
	App::Get().windowRectChanged(bg::math::Rect(x,y,w,h));
}

void EventHandler::openFile() {
	stop();

	bg::ptr<bg::wnd::FileDialog> dialog = bg::wnd::FileDialog::OpenFileDialog();
	dialog->addFilter("vitscnj");
	dialog->addFilter("vitscn");

	if (dialog->show()) {
		App::Get().scene()->open(dialog->getResultPath());
		reshape(_viewport.width(), _viewport.height());
	}
}

void EventHandler::play() {
	dynamics([&](bg::scene::Dynamics * w) {
		w->play();
	});
}

void EventHandler::pause() {
	dynamics([&](bg::scene::Dynamics * w) {
		w->pause();
	});
}

void EventHandler::stop() {
	dynamics([&](bg::scene::Dynamics * w) {
		w->stop();
	});
}

void EventHandler::dynamics(std::function<void(bg::scene::Dynamics*)> cb) {
	bg::ptr<bg::scene::FindComponentVisitor<bg::scene::Dynamics>> find = new bg::scene::FindComponentVisitor<bg::scene::Dynamics>();
	App::Get().scene()->sceneRoot()->accept(find.getPtr());

	for (auto item : find->result()) {
		cb(item);
	}
}

}
