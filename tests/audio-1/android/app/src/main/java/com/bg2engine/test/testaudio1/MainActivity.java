package com.bg2engine.test.testaudio1;

import android.app.Activity;
import android.hardware.SensorEvent;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends Activity {
    protected com.bg2engine.android.GLView mGLView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGLView = new com.bg2engine.android.GLView(getApplication(),getWindowManager());
        setContentView(mGLView);
    }


}
