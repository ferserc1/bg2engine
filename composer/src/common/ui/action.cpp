/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <ui/action.hpp>

#include <app.hpp>

namespace composer {
namespace ui {

Action::Action(ActionCode code, const std::string & title, ActionClosure a)
	:_code(code)
	,_title(title)
	,_action(a)
{

}

Action::Action(ActionCode code, const std::string & title, const std::string & menuPath, ActionClosure a)
	:_code(code)
	,_title(title)
	,_menuPath(menuPath)
	,_action(a)
{

}

Action::Action(ActionCode code, const std::string & title, const std::string & menuPath, const bg::base::KeyboardShortcut & shortcut, ActionClosure a)
	:_code(code)
	,_title(title)
	,_menuPath(menuPath)
	,_shortcut(shortcut)
	,_action(a)
{

}

Action::~Action() {

}

void UserActions::registerAction(Action * action) {
	_actionMap[action->code()] = action;
	if (!action->menuPath().empty()) {
		App::Get().menuFactory().addMenu(action->menuPath(), action->code(), action->shortcut());
	}
}

Action * UserActions::find(Action::ActionCode c) {
	if (_actionMap.find(c) != _actionMap.end()) {
		return _actionMap[c].getPtr();
	}
	return nullptr;
}

void UserActions::exec(Action::ActionCode c) {
	Action * a = find(c);
	if (a) a->execute();
}

void UserActions::triggerDelegates() {
	for (auto d : _actionDelegates) {
		d->registerActions(*this);
	}
}


}
}
