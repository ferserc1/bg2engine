
#include <Windows.h>

#include <app.hpp>

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR lpCmdLine, INT nCmdShow) {
	return composer::App::Get().run();
}