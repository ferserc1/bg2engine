#include <jni.h>
#include <string>
#include <iostream>
#include <chrono>

#include <bg/android/app.hpp>
#include <bg/math/math.hpp>
#include <bg/android/gles_context.hpp>
#include <bg/engine/opengl_es_3.hpp>

#include <app_glue.hpp>

#include <fstream>
#include <bg/log.hpp>

Renderer * Renderer::s_renderer = nullptr;

long long currentTime() {
    /*
    struct timeval tv;
    gettimeofday(&tv,nullptr);
    return (tv.tv_sec * 1000) + (tv.tv_usec / 1000);
     */
    timespec time;
    clock_gettime(CLOCK_REALTIME, &time);
    return static_cast<long long>(time.tv_nsec);
}

void Renderer::init() {
	using namespace bg::engine::opengl;
    //_lastTime = currentTime();
    clock_gettime(CLOCK_REALTIME, &_lastTime);
	bg::android::App::Get().logDebug("Init");

	_eventHandler = createEventHandler();

    bg::Engine::Init(new bg::engine::OpenGLES3());
	_eventHandler->willCreateContext();

	_eventHandler->setContext(_context.getPtr());
	_eventHandler->initGL();
}

void Renderer::reshape(int w, int h) {
	_eventHandler->reshape(w, h);
}


timespec diff(timespec & start, timespec & end) {
    timespec temp;
    if ((end.tv_nsec-start.tv_nsec)<0) {
        temp.tv_sec = end.tv_sec-start.tv_sec-1;
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    }
    else {
        temp.tv_sec = end.tv_sec - start.tv_sec;
        temp.tv_nsec = end.tv_nsec - start.tv_nsec;
    }
    return temp;
}

void Renderer::draw() {
    {
        std::lock_guard<std::mutex> lock(_sensorMutex);
        _eventHandler->sensorEvent(bg::base::SensorEvent(_accelerometer));
    }

    timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    timespec d = diff(_lastTime,ts);
    clock_gettime(CLOCK_REALTIME, &_lastTime);
    float delta = static_cast<float>(d.tv_nsec) / 1000000000.0f;;
	_eventHandler->frame(delta);
	_eventHandler->draw();
}

void Renderer::touchStart(const std::vector<bg::math::Position2Di> & touches) {
	using namespace bg::android;
	bg::base::TouchEvent evt;
	for (auto p : touches) {
		evt.addTouch(p);
	}
	_eventHandler->touchStart(evt);
}

void Renderer::touchMove(const std::vector<bg::math::Position2Di> & touches) {
	bg::base::TouchEvent evt;
	for (auto p : touches) {
		evt.addTouch(p);
	}
	_eventHandler->touchMove(evt);
}

void Renderer::touchEnd(const std::vector<bg::math::Position2Di> & touches) {
	bg::base::TouchEvent evt;
	for (auto p : touches) {
		evt.addTouch(p);
	}
	_eventHandler->touchEnd(evt);
}

void Renderer::accelerationEvent(float x, float y, float z) {
    if (_eventHandler.valid()) {
        std::lock_guard<std::mutex> lock(_sensorMutex);
        _accelerometer.set(x,y,z);
    }
}

Renderer::Renderer()
	:_context(new bg::android::GLESContext())
{

}

Renderer::~Renderer() {

}

extern "C" {
    JNIEXPORT void JNICALL Java_com_bg2engine_base_EventHandler_init(JNIEnv * env, jobject obj, jobject assetManager, jstring inStorate, jstring exStorage, jint displayWidth, jint displayHeight, jfloat displayDensity);
    JNIEXPORT void JNICALL Java_com_bg2engine_base_EventHandler_reshape(JNIEnv * env, jobject obj, jint width, jint height);
    JNIEXPORT void JNICALL Java_com_bg2engine_base_EventHandler_draw(JNIEnv * env, jobject obj);
    JNIEXPORT void JNICALL Java_com_bg2engine_base_EventHandler_touchEvent1(JNIEnv * env, jobject obj, jint evt, jfloat x0, jfloat y0);
    JNIEXPORT void JNICALL Java_com_bg2engine_base_EventHandler_touchEvent2(JNIEnv * env, jobject obj, jint evt, jfloat x0, jfloat y0, jfloat x1, jfloat y1);
    JNIEXPORT void JNICALL Java_com_bg2engine_base_EventHandler_touchEvent3(JNIEnv * env, jobject obj, jint evt, jfloat x0, jfloat y0, jfloat x1, jfloat y1, jfloat x2, jfloat y2);
    JNIEXPORT void JNICALL Java_com_bg2engine_base_EventHandler_touchEvent4(JNIEnv * env, jobject obj, jint evt, jfloat x0, jfloat y0, jfloat x1, jfloat y1, jfloat x2, jfloat y2, jfloat x3, jfloat y3);
    JNIEXPORT void JNICALL Java_com_bg2engine_base_EventHandler_accelerationEvent(JNIEnv * env, jobject obj, jfloat x, jfloat y, jfloat z);
}

JNIEXPORT void JNICALL
Java_com_bg2engine_base_EventHandler_init(JNIEnv * env, jobject obj, jobject assetManager, jstring inStorage, jstring exStorage, jint displayWidth, jint displayHeight, jfloat displayDensity) {
    using namespace bg::android;
    App::Get().init(env,assetManager,inStorage,exStorage,displayWidth,displayHeight,displayDensity);
    Renderer::Get().init();
}

JNIEXPORT void JNICALL
Java_com_bg2engine_base_EventHandler_reshape(JNIEnv * env, jobject obj, jint width, jint height) {
    Renderer::Get().reshape(width,height);
}

JNIEXPORT void JNICALL
Java_com_bg2engine_base_EventHandler_draw(JNIEnv * env, jobject obj) {
    Renderer::Get().draw();
}

JNIEXPORT void JNICALL
Java_com_bg2engine_base_EventHandler_touchEvent1(JNIEnv * env, jobject obj, jint evt, jfloat x0, jfloat y0) {
    switch (evt) {
        case 1:
            Renderer::Get().touchStart(std::vector<bg::math::Position2Di>{
                    { static_cast<int>(x0), static_cast<int>(y0) }
            });
            break;
        case 2:
            Renderer::Get().touchMove(std::vector<bg::math::Position2Di>{
                    { static_cast<int>(x0), static_cast<int>(y0) }
            });
            break;
        case 3:
            Renderer::Get().touchEnd(std::vector<bg::math::Position2Di>{
                    { static_cast<int>(x0), static_cast<int>(y0) }
            });
            break;
    }
}

JNIEXPORT void JNICALL
Java_com_bg2engine_base_EventHandler_touchEvent2(JNIEnv * env, jobject obj, jint evt, jfloat x0, jfloat y0, jfloat x1, jfloat y1) {
    switch (evt) {
        case 1:
            Renderer::Get().touchStart(std::vector<bg::math::Position2Di>{
                    { static_cast<int>(x0), static_cast<int>(y0) },
                    { static_cast<int>(x1), static_cast<int>(y1) }
            });
            break;
        case 2:
            Renderer::Get().touchMove(std::vector<bg::math::Position2Di>{
                    { static_cast<int>(x0), static_cast<int>(y0) },
                    { static_cast<int>(x1), static_cast<int>(y1) }
            });
            break;
        case 3:
            Renderer::Get().touchEnd(std::vector<bg::math::Position2Di>{
                    { static_cast<int>(x0), static_cast<int>(y0) },
                    { static_cast<int>(x1), static_cast<int>(y1) }
            });
            break;
    }
}

JNIEXPORT void JNICALL
Java_com_bg2engine_base_EventHandler_touchEvent3(JNIEnv * env, jobject obj, jint evt, jfloat x0, jfloat y0, jfloat x1, jfloat y1, jfloat x2, jfloat y2) {
    switch (evt) {
        case 1:
            Renderer::Get().touchStart(std::vector<bg::math::Position2Di>{
                    { static_cast<int>(x0), static_cast<int>(y0) },
                    { static_cast<int>(x1), static_cast<int>(y1) },
                    { static_cast<int>(x2), static_cast<int>(y2) }
            });
            break;
        case 2:
            Renderer::Get().touchMove(std::vector<bg::math::Position2Di>{
                    { static_cast<int>(x0), static_cast<int>(y0) },
                    { static_cast<int>(x1), static_cast<int>(y1) },
                    { static_cast<int>(x2), static_cast<int>(y2) }
            });
            break;
        case 3:
            Renderer::Get().touchEnd(std::vector<bg::math::Position2Di>{
                    { static_cast<int>(x0), static_cast<int>(y0) },
                    { static_cast<int>(x1), static_cast<int>(y1) },
                    { static_cast<int>(x2), static_cast<int>(y2) }
            });
            break;
    }
}

JNIEXPORT void JNICALL
Java_com_bg2engine_base_EventHandler_touchEvent4(JNIEnv * env, jobject obj, jint evt, jfloat x0, jfloat y0, jfloat x1, jfloat y1, jfloat x2, jfloat y2, jfloat x3, jfloat y3) {
    switch (evt) {
        case 1:
            Renderer::Get().touchStart(std::vector<bg::math::Position2Di>{
                    { static_cast<int>(x0), static_cast<int>(y0) },
                    { static_cast<int>(x1), static_cast<int>(y1) },
                    { static_cast<int>(x2), static_cast<int>(y2) },
                    { static_cast<int>(x3), static_cast<int>(y3) }
            });
            break;
        case 2:
            Renderer::Get().touchMove(std::vector<bg::math::Position2Di>{
                    { static_cast<int>(x0), static_cast<int>(y0) },
                    { static_cast<int>(x1), static_cast<int>(y1) },
                    { static_cast<int>(x2), static_cast<int>(y2) },
                    { static_cast<int>(x3), static_cast<int>(y3) }
            });
            break;
        case 3:
            Renderer::Get().touchEnd(std::vector<bg::math::Position2Di>{
                    { static_cast<int>(x0), static_cast<int>(y0) },
                    { static_cast<int>(x1), static_cast<int>(y1) },
                    { static_cast<int>(x2), static_cast<int>(y2) },
                    { static_cast<int>(x3), static_cast<int>(y3) }
            });
            break;
    }
}

JNIEXPORT void JNICALL
Java_com_bg2engine_base_EventHandler_accelerationEvent(JNIEnv * env, jobject obj, jfloat x, jfloat y, jfloat z) {
    Renderer::Get().accelerationEvent(x / -9.8f,y / -9.8f, z / -9.8f);
}