/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <event_handler.hpp>

#include <bg/bg2e.hpp>

class LightAnimation : public bg::scene::Component {
public:
	Component * clone() { return new LightAnimation(); }
	virtual void frame(float delta) {
		using namespace bg::scene;
		Transform * trx = node()->component<Transform>();
		static float d = 0.0f;
		d += delta;
		if (trx) {
			trx->matrix()
				.identity()
				.rotate(d, 0.0f, 1.0f, 0.0f)
				.rotate(bg::math::trigonometry::degreesToRadians(20.0f), -1.0f, 0.0f, 0.0f)
				.translate(0.0f, 0.0f, 5.0f);
		}		
	}
};

class LightType: public bg::scene::Component {
public:
	Component * clone() { return new LightType(); }

	virtual void frame(float delta) {
		static float elapsed = 0.0f;
		elapsed+=delta;
		if(elapsed>5.0f) {
			elapsed = 0.0f;
			switch(light()->light()->type()) {
			case bg::base::Light::kTypeDirectional:
				light()->light()->setType(bg::base::Light::kTypePoint);
				break;
			case bg::base::Light::kTypePoint:
				light()->light()->setType(bg::base::Light::kTypeSpot);
				break;
			case bg::base::Light::kTypeSpot:
				light()->light()->setType(bg::base::Light::kTypeDirectional);
				break;
			}
		}
	}
};

bg::scene::Drawable * MyEventHandler::createFloor(bg::base::Context * ctx) {
	using namespace bg::scene;
	bg::ptr<bg::scene::Drawable> drawable = bg::scene::PrimitiveFactory(ctx).plane(40.0f);

	drawable->material(0)->setShininess(95.0f);

	return drawable.release();
}

bg::scene::Node * MyEventHandler::createScene(bg::base::Context * ctx) {
	using namespace bg::scene;
	using namespace bg::math;
	using namespace bg::math::distance;
	bg::ptr<Node> sceneRoot = new Node(ctx,"SceneRoot");
	
	bg::db::NodeLoader::RegisterPlugin(new bg::db::plugin::ReadPrefabBg2());	
	
	Node * cube = new Node(ctx);
	cube->addComponent(new Transform());
	cube->addComponent(PrimitiveFactory(ctx).cube());
	cube->component<Drawable>()->material(0)->setDiffuse(Color(1.0f, 0.0f, 0.0f, 0.8f));
	cube->component<Drawable>()->material(0)->setReceiveShadows(false);
	sceneRoot->addChild(cube);
	
	Node * sphere = new Node(ctx, "Sphere");
	sphere->addComponent(PrimitiveFactory(ctx).sphere(Scalar(50,cm)));
	sphere->component<Drawable>()->material(0)->setDiffuse(Color::Blue());
	sphere->addComponent(new Transform(Matrix4::Translation(0.0f, 0.0f, 1.5f)));
	cube->addChild(sphere);
	
	bg::scene::Node * floor = new Node(ctx,"Floor");
	floor->addComponent(createFloor(ctx));
	floor->addComponent(new Transform(bg::math::Matrix4::Translation(0.0f, -0.5f, 0.0f)));
	sceneRoot->addChild(floor);
	

	Node * cam = new Node(ctx, "Camera");
	cam->addComponent(new bg::scene::Camera());
	bg::math::Matrix4 trx;
	trx.identity()
		.rotate(bg::math::trigonometry::degreesToRadians(-170.0f), 0.0f, 1.0f, 0.0f)
		.rotate(bg::math::trigonometry::degreesToRadians(12.5f), -1.0f, 0.0f, 0.0f)
		.translate(.0f, .0f, 10.0f);
	cam->addComponent(new bg::scene::Transform(trx));
	cam->component<bg::scene::Camera>()->setFocus(10.0f);	// Set the focus at the same distance as the transform is
	sceneRoot->addChild(cam);
	bg::scene::OpticalProjectionStrategy * projectionStrategy = new bg::scene::OpticalProjectionStrategy();
	projectionStrategy->setFrameSize(35.0f);	// 35mm film
	projectionStrategy->setFocalLength(50.0f);	// 50mm focal length
	cam->component<bg::scene::Camera>()->setProjectionStrategy(projectionStrategy);

	Node * light = new Node(ctx, "Light");
	light->addComponent(new bg::scene::Light());
	light->addComponent(new LightType());
	light->component<Light>()->light()->setShadowStrength(1.0f);
	light->component<Light>()->light()->setAmbient(bg::math::Color(0.1f,0.1f,0.1f,1.0f));
	light->component<Light>()->light()->setSpotExponent(45.0f);
	light->component<Light>()->light()->setSpotCutoff(36.0f);
	light->component<Light>()->light()->setConstantAttenuation(1.0f);
	light->component<Light>()->light()->setLinearAttenuation(0.1f);
	light->component<Light>()->light()->setQuadraticAttenuation(0.0f);
	trx.identity()
		.rotate(bg::math::trigonometry::degreesToRadians(25.5f), -1.0f, 0.0f, 0.0f)
		.translate(.0f, .0f, 5.0f);
	light->addComponent(new bg::scene::Transform(trx));

	Node * lightAnimation = new Node(ctx, "Light animation");
	lightAnimation->addComponent(new LightAnimation());
	lightAnimation->addComponent(new Transform());
	lightAnimation->addChild(light);
	sceneRoot->addChild(lightAnimation);
	
	return sceneRoot.release();
}

MyEventHandler::MyEventHandler()
{
	
}

MyEventHandler::~MyEventHandler() {
	
}

void MyEventHandler::willCreateContext() {
	if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else if (bg::system::isDesktop()){
		throw bg::base::CompatibilityException("Fatal error: no suitable rendering engine found.");
	}
}

void MyEventHandler::initGL() {
	bg::Engine::Get()->initialize(context());
	context()->setVsyncEnabled(true);
	
    _hqRenderer = bg::render::Renderer::Create(context(), bg::render::Renderer::kRenderPathDeferred);
    _basicRenderer = bg::render::Renderer::Create(context(), bg::render::Renderer::kRenderPathForward);
    _renderer = _hqRenderer.getPtr();

	// Render settings method 1: get the setting class (ShadowMap) and use the accessor
	bg::render::ShadowMap * shadowMap = _renderer->settings<bg::render::ShadowMap>();
	if (shadowMap) {
		shadowMap->setShadowType(bg::render::ShadowMap::kSoftShadows);
	}

	// Render settings method 2 (preferred method): use de setRenderSetting() function.
	_renderer->setRenderSetting(bg::render::settings::kShadowMap, bg::render::settings::kShadowMapSize, bg::math::Size2Di(2048));

	_sceneRoot = createScene(context());
	_sceneRoot->someChild([&](bg::scene::Node * node) -> bool {
		return (_camera = node->component<bg::scene::Camera>()).valid();
	});
}

void MyEventHandler::reshape(int w, int h) {
	_camera->setViewport(bg::math::Viewport(0,0,w,h));
}

void MyEventHandler::frame(float delta) {
	_renderer->frame(_sceneRoot.getPtr(), delta);

	if (_fpsCounter.frame(delta)) {
		bg::log(bg::log::kDebug) << _fpsCounter.fps() << bg::endl;
	}
}

void MyEventHandler::draw() {
	_renderer->draw(_sceneRoot.getPtr(), _camera.getPtr());
	context()->swapBuffers();
}

void MyEventHandler::onMemoryWarning() {
	std::cout << "Memory warning received" << std::endl;
}

// Mobile touch events
void MyEventHandler::touchStart(const bg::base::TouchEvent & evt)  {
}

void MyEventHandler::touchMove(const bg::base::TouchEvent & evt)  {
}

void MyEventHandler::touchEnd(const bg::base::TouchEvent & evt) {
}


// Desktop events
void MyEventHandler::keyUp(const bg::base::KeyboardEvent & evt) {
	if (evt.keyboard().key() == bg::base::Keyboard::kKeyEsc) {
		bg::wnd::MainLoop::Get()->quit(0);
	}
    if (evt.keyboard().key() == bg::base::Keyboard::kKeySpace) {
        _renderer = _renderer==_hqRenderer.getPtr() ? _basicRenderer.getPtr() : _hqRenderer.getPtr(); 
    }
}

void MyEventHandler::mouseDown(const bg::base::MouseEvent & evt) {
}

void MyEventHandler::mouseDrag(const bg::base::MouseEvent & evt) {
}

