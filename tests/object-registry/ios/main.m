//
//  main.m
//  event-handler
//
//  Created by Fernando Serrano Carpena on 3/5/16.
//  Copyright © 2016 Fernando Serrano Carpena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
