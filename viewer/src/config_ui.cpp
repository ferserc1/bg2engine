/*
*	bg2 engine license
*	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
*
*	Permission is hereby granted, free of charge, to any person obtaining a copy
*	of this software and associated documentation files (the "Software"), to deal
*	in the Software without restriction, including without limitation the rights
*	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
*	of the Software, and to permit persons to whom the Software is furnished to do
*	so, subject to the following conditions:
*
*	The above copyright notice and this permission notice shall be included in all
*	copies or substantial portions of the Software.
*
*	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
*	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
*	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
*	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
*	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
*	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
*/

#include <config_ui.hpp>
#include <app.hpp>

#include <bg/gui/gui.hpp>

namespace viewer {

ConfigUI::ConfigUI(bg::gui::Surface * surface)
	:Window(surface)
{
	using namespace bg::gui;

	create();
	setSize(bg::math::Size2Di(300, 400));
	setAlignment(bg::gui::Alignment::kAlignMiddleCenter);
	contentView()->setLayout(new VerticalLayout());

	Label * renderPath = View::Create<Label>(surface->context(), surface->skin());
	renderPath->setText("Render path");
	contentView()->addChild(renderPath);

	RadioButtonField * forward = View::Create<RadioButtonField>(surface->context(), surface->skin());
	RadioButtonField * deferred = View::Create<RadioButtonField>(surface->context(), surface->skin());
	forward->setText("Forward render");
	deferred->setText("Deferred render");
	forward->setOnValueChangedAction([&](bool selected, int selectedIndex) {
		if (selectedIndex == 0) {
			App::Get().eventHandler()->setForwardRender();
		}
		else {
			App::Get().eventHandler()->setDeferredRender();
		}
	});
	forward->addToGroup(deferred);
	contentView()->addChild(forward);
	contentView()->addChild(deferred);
	if (App::Get().eventHandler()->isForwardRender()) {
		forward->setActiveOption(0);
	}
	else {
		forward->setActiveOption(1);
	}

	Label * raytracerQuality = View::Create<Label>(surface->context(), surface->skin());
	raytracerQuality->setText("Raytracer quality");
	contentView()->addChild(raytracerQuality);

	RadioButtonField * low = View::Create<RadioButtonField>(surface->context(), surface->skin());
	RadioButtonField * mid = View::Create<RadioButtonField>(surface->context(), surface->skin());
	RadioButtonField * high = View::Create<RadioButtonField>(surface->context(), surface->skin());
	RadioButtonField * extreme = View::Create<RadioButtonField>(surface->context(), surface->skin());
	low->addToGroup(mid);
	low->addToGroup(high);
	low->addToGroup(extreme);
	low->setText("Low");
	mid->setText("Medium");
	high->setText("High");
	extreme->setText("Extreme");
	low->setOnValueChangedAction([&](bool selected, int selectedIndex) {
		App::Get().eventHandler()->setRaytracerQuality(selectedIndex);
	});
	contentView()->addChild(low);
	contentView()->addChild(mid);
	contentView()->addChild(high);
	contentView()->addChild(extreme);
	low->setActiveOption(App::Get().eventHandler()->raytracerQuality());


	Label * ssaoQuality = View::Create<Label>(surface->context(), surface->skin());
	ssaoQuality->setText("Ambient occlussion quality");
	contentView()->addChild(ssaoQuality);

	low = View::Create<RadioButtonField>(surface->context(), surface->skin());
	mid = View::Create<RadioButtonField>(surface->context(), surface->skin());
	high = View::Create<RadioButtonField>(surface->context(), surface->skin());
	extreme = View::Create<RadioButtonField>(surface->context(), surface->skin());
	low->addToGroup(mid);
	low->addToGroup(high);
	low->addToGroup(extreme);
	low->setText("Low");
	mid->setText("Medium");
	high->setText("High");
	extreme->setText("Extreme");
	low->setOnValueChangedAction([&](bool selected, int selectedIndex) {
		App::Get().eventHandler()->setSSAOQuality(selectedIndex);
	});
	contentView()->addChild(low);
	contentView()->addChild(mid);
	contentView()->addChild(high);
	contentView()->addChild(extreme);
	low->setActiveOption(App::Get().eventHandler()->ssaoQuality());

	Button * closeButton = View::Create<Button>(surface->context(), surface->skin());
	closeButton->setText("Close");
	closeButton->setAction([&]() {
		this->hide();
	});
	contentView()->addChild(closeButton);

	hide();
}

ConfigUI::~ConfigUI() {

}

}
