/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

//
//  event-handler.cpp
//  bg2e-ios
//
//  Created by Fernando Serrano Carpena on 3/5/16.
//  Copyright © 2016 Fernando Serrano Carpena. All rights reserved.
//

#include <event_handler.hpp>

#include <bg/bg2e.hpp>
#include <bg/android/app.hpp>

bg::scene::Node * getSphere(bg::base::Context * ctx);
bg::scene::Node * getCube(bg::base::Context * ctx);
bg::scene::Node * getModel(bg::base::Context * ctx);

class ObjectRemove : public bg::scene::Component {
public:
	enum Type {
		kTypeCube = 0,
		kTypeSphere,
		kTypeModel
	};
		
	ObjectRemove(Type type) :_type(type), _lifeTime(0.0f) {}

	bg::scene::Component * clone() {
		return new ObjectRemove(_type);
	}

	virtual void init() {
		collider()->addCollisionClosure([&](const bg::physics::CollisionData & data) {
			std::cout << "Collision detected with node " << data.nodeB->name() << std::endl;
		});
	}


	virtual void frame(float delta) {
		float ttl = 1.0f;
		switch (_type) {
		case kTypeCube:
			ttl = 10.0f;
			break;
		case kTypeSphere:
			ttl = 12.0f;
			break;
		case kTypeModel:
			ttl = 14.0f;
			break;
		}
		
		// Only increment life time when the simulation loop is running
		if (rigidBody() && rigidBody()->body()->simulationRunning()) {
			_lifeTime += delta;
		}
		
		if(rigidBody() && rigidBody()->body()->world() && transform() && _lifeTime>ttl)
		{
			// You can get the physics world containing the rigid body or collider using the following methods:
			//		rigidBody()->body()->world()
			//		collider()->shape()->world()
			// But be careful: the world() method returns nullptr if the rigid body or collider is not added
			// to an active simulation loop.
			bg::physics::World * world = rigidBody()->body()->world();
			world->removeNode(node());

			// To remove or add objects inside a life cycle functions (such as frame()), you may use
			// Node::RemoveFromScene() and Node::AddToScene() functions to do it safely.
			bg::scene::Node::RemoveFromScene(node(), [&](bg::scene::Node *) {
				std::cout << "-- Object removed ---------------------" << std::endl << std::endl << std::endl;
			});

			// You can still use node() after RemoveFromScene(), because the node will be removed after
			// the frame() life cycle ends. The callback function is called when the action is completed
			bg::scene::Node * newNode = nullptr;
			switch (_type) {
			case kTypeCube:
				newNode = getCube(node()->context());
				break;
			case kTypeSphere:
				newNode = getSphere(node()->context());
				break;
			case kTypeModel:
				newNode = getModel(node()->context());
				break;
			}
			bg::scene::Node::AddToScene(newNode, node()->parent(), [&](bg::scene::Node * node) {
				bg::scene::Dynamics * dyn = node->parent()->component<bg::scene::Dynamics>();
				dyn->world()->addNode(node);
			});
		}
	}

	virtual void keyUp(const bg::base::KeyboardEvent & evt) {
		if(evt.keyboard().key() == bg::base::Keyboard::kKeyT) {
			if (rigidBody()) {
				rigidBody()->body()->setTransform(bg::math::Matrix4::Translation(0.0f, 5.0f, 0.0f));
			}
		}
	}

protected:
	Type _type;
	float _lifeTime;
};

bg::scene::Node * getModel(bg::base::Context * ctx) {
	using namespace bg::scene;
	bg::system::Path path = bg::system::Path::AppDir();
	if (bg::system::isMobile()) {
		path = bg::system::Path::ResourcesDir();
	}
	bg::ptr<Node> model = bg::db::loadPrefab(ctx, path.pathAddingComponent("data/test-shape.bg2"));
	
	Drawable * drw = model->component<Drawable>();
	Collider * hullCollider = new Collider();
	bg::physics::ConvexHullColliderShape * hullShape = new bg::physics::ConvexHullColliderShape();
	hullShape->setVertexList((*drw->element(0)).polyList->vertexVector());
	hullShape->optimizeVertexList();
	hullCollider->setShape(hullShape);
	model->addComponent(hullCollider);
	
	RigidBody * rigidBody = new RigidBody();
	rigidBody->body()->setMass(1.0f);
	model->addComponent(rigidBody);
	
	using namespace bg::math::trigonometry;
	model->addComponent(new Transform(bg::math::Matrix4::Translation(bg::math::random(), 9.0f, bg::math::random())
									  .rotate(degreesToRadians(46.0f), 1.0f, 0.0f, 0.0f)
									  .rotate(degreesToRadians(80.0f), 0.0f, 1.0f, 0.0f)));
	
	model->addComponent(new ObjectRemove(ObjectRemove::kTypeModel));
	return model.release();
}

bg::scene::Node * getCube(bg::base::Context * ctx) {
	using namespace bg::scene;
	bg::ptr<Node> cube = new Node(ctx, "Cube");
	
	cube->addComponent(new ObjectRemove(ObjectRemove::kTypeCube));
	
	Collider * cubeCollider = new Collider();
	cubeCollider->setShape(new bg::physics::BoxColliderShape(1.0f, 1.0f, 1.0f));
	cube->addComponent(cubeCollider);
	
	RigidBody * rigidBody = new RigidBody();
	rigidBody->body()->setMass(1.0f);
	cube->addComponent(rigidBody);
	
	using namespace bg::math::trigonometry;
	cube->addComponent(new Transform(bg::math::Matrix4::Translation(bg::math::random(), 7.0f, bg::math::random())
												.rotate(degreesToRadians(22.5f), 1.0f, 0.0f, 0.0f)
												.rotate(degreesToRadians(14.7f), 0.0f, 1.0f, 0.0f)
												.scale(0.4f,1.2f,1.5f)));
	
	PrimitiveFactory pf(ctx);
	Drawable * cubeDrawable = pf.cube(1.0f);
	cubeDrawable->material(0)->setDiffuse(bg::math::Color::Green());
	cubeDrawable->material(0)->setShininess(15.0f);
	cube->addComponent(cubeDrawable);
	
	return cube.release();
}

bg::scene::Node * getSphere(bg::base::Context * ctx) {
	using namespace bg::scene;
	float sphereSize = 1.0f;
	bg::ptr<Node> sphere = new Node(ctx, "Sphere");

	sphere->addComponent(new ObjectRemove(ObjectRemove::kTypeSphere));


	// Sphere collider
	Collider * sphereCollider = new Collider();
	sphereCollider->setShape(new bg::physics::SphereColliderShape(sphereSize));
	sphere->addComponent(sphereCollider);

	// Use a RigidBody component to process the box as a dynamic world object.
	// You can use only a collider to create a static world object, as we did with
	// the floor node.
	RigidBody * rigidBody = new RigidBody();
	rigidBody->body()->setMass(1.0f);
	//rigidBody->body()->setLinearFactor(bg::math::Vector3(0.0f, 1.0f, 0.0f));
	sphere->addComponent(rigidBody);

	// A dynamic world object needs a transform component to work
	sphere->addComponent(new Transform(bg::math::Matrix4::Translation(0.4f, 5.0f, 0.4f)
										.scale(1.0f,0.5f,0.5f)));

	// Sphere geometry
	PrimitiveFactory primitive(ctx);
	Drawable * sphereDrawable = primitive.sphere(sphereSize, 40);
	sphereDrawable->material(0)->setDiffuse(bg::math::Color::Red());
	sphereDrawable->material(0)->setShininess(55.0f);
	sphere->addComponent(sphereDrawable);
	return sphere.release();
}

MyEventHandler::MyEventHandler()
{
}

MyEventHandler::~MyEventHandler() {
	
}

void MyEventHandler::willCreateContext() {
	if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else if (bg::system::isDesktop()){
		throw bg::base::CompatibilityException("Fatal error: no suitable rendering engine found.");
	}
}

void MyEventHandler::initGL() {
	bg::Engine::Get()->initialize(context());
	
	using namespace bg::scene;
	
	bg::db::NodeLoader::RegisterPlugin(new bg::db::plugin::ReadPrefabBg2());
	bg::db::NodeLoader::RegisterPlugin(new bg::db::plugin::ReadScene());
	bg::db::NodeWriter::RegisterPlugin(new bg::db::plugin::WriteScene());

    if (bg::system::currentPlatform()==bg::system::kAndroid) {
        bg::system::Path resources = bg::system::Path::ResourcesDir().pathAddingComponent("data");
        bg::android::App::Get().extractAllAssets(resources);
    }

	PrimitiveFactory primitive(context());
	_sceneRoot = new Node(context(), "Scene Root");
	
	// All the nodes outside physicsWorld will be ignored, but it's
	// possible to have more than one node with a Dynamics component. In
	// this case, each dynamics component acts only in its own children
	Node * physicsWorld = new Node(context(), "World");
	Dynamics * dynamics = new Dynamics();
	dynamics->world()->setGravity(bg::math::Vector3(0.0f, -9.8f, 0.0f));
	physicsWorld->addComponent(dynamics);
	_sceneRoot->addChild(physicsWorld);
	_dynamics = dynamics;
	
	// Use a cube as a floor
	bg::math::Vector3 cubeSize(10.0f, 0.1f, 10.0f);
	Node * floor = new Node(context(), "Floor");
	physicsWorld->addChild(floor);
	
	// Floor collider
	Collider * floorCollider = new Collider();
	floorCollider->setShape(new bg::physics::BoxColliderShape(cubeSize));
	floor->addComponent(floorCollider);
	
	
	// Floor geometry
	floor->addComponent(primitive.cube(cubeSize));
	
	// Sphere
	physicsWorld->addChild(getSphere(context()));
	
	// Cube
	physicsWorld->addChild(getCube(context()));
	
	// Model
	physicsWorld->addChild(getModel(context()));
	
	// You can use transforms with the colliders
	Node * wedge = new Node(context(), "Wedge");
	wedge->addComponent(primitive.cube(0.5f));
	wedge->addComponent(new Transform(bg::math::Matrix4::Rotation(bg::math::kPiOver4, 0.0f, 1.0f, 0.0f)
										.rotate(bg::math::kPiOver4, 1.0f, 0.0f, 0.0f)));
	wedge->addComponent(new Collider(new bg::physics::BoxColliderShape(0.5f, 0.5f, 0.5f)));
	physicsWorld->addChild(wedge);
	
	
	
	_camera = new Camera();
	_camera->setProjectionStrategy(new OpticalProjectionStrategy());
	Node * cameraNode = new Node(context(), "Camera");
	cameraNode->addComponent(_camera.getPtr());
	cameraNode->addComponent(new Transform());
	bg::manipulation::OrbitNodeController * cameraController = new bg::manipulation::OrbitNodeController();
	cameraController->setDistance(10.0f);
	cameraController->setRotation(bg::math::Vector2(-12.5,45.0f));
	cameraNode->addComponent(cameraController);
	_sceneRoot->addChild(cameraNode);
	
	Node * light = new Node(context(),"Light");
	_sceneRoot->addChild(light);
	light->addComponent(new Light());
	light->addComponent(new Transform(bg::math::Matrix4::Identity()
										.rotate(bg::math::trigonometry::degreesToRadians(55.0f), -1.0f, 0.0f, 0.0f)));
	
    using namespace bg::math;
	_renderer = bg::render::Renderer::Create(context(), bg::render::Renderer::kRenderPathForward);
	_renderer->setRenderSetting(bg::render::settings::kAmbientOcclusion, bg::render::settings::kKernelSize, 32);
	_renderer->setRenderSetting(bg::render::settings::kAmbientOcclusion, bg::render::settings::kBlur, 2);
    _renderer->setRenderSetting(bg::render::settings::kAmbientOcclusion, bg::render::settings::kSampleRadius, Scalar(20,distance::cm));
	_renderer->setRenderSetting(bg::render::settings::kShadowMap, bg::render::settings::kShadowMapSize, bg::math::Size2Di(2048));

	_inputVisitor = new bg::scene::InputVisitor();
}

void MyEventHandler::destroy() {
}

void MyEventHandler::reshape(int w, int h) {
	_camera->setViewport(bg::math::Viewport(0,0,w,h));
}

void MyEventHandler::frame(float delta) {
	_renderer->frame(_sceneRoot.getPtr(), delta);
}

void MyEventHandler::draw() {
	_renderer->draw(_sceneRoot.getPtr(), _camera.getPtr());
	
	context()->swapBuffers();
}

void MyEventHandler::onMemoryWarning() {
	std::cout << "Memory warning received" << std::endl;
}

// Mobile touch events
void MyEventHandler::touchStart(const bg::base::TouchEvent & evt)  {
	std::chrono::time_point<std::chrono::system_clock> timeNow = std::chrono::system_clock::now();
	std::chrono::duration<float> difference = timeNow - _lastTouch;
	if (difference<std::chrono::milliseconds(300)) {
		std::cout << "Start simulation" << std::endl;
		if (evt.touches().size()==1) {
			if (_dynamics->getSimulationState() == bg::scene::Dynamics::kStatePaused ||
				_dynamics->getSimulationState() == bg::scene::Dynamics::kStateStopped)
			{
				_dynamics->play();
				bg::system::EnergySaver::Get().disableScreenIdleTimer();
			}
			else {
				_dynamics->pause();
				bg::system::EnergySaver::Get().enableScreenIdleTimer();
			}
		}
		else if (evt.touches().size()==2) {
			_dynamics->stop();
			
			// Reset the sphere transform
			bg::ptr<bg::scene::FindComponentVisitor<bg::scene::Transform>> visitor = new bg::scene::FindComponentVisitor<bg::scene::Transform>();
			_sceneRoot->accept(visitor.getPtr());
			for (auto trx : visitor->result()) {
				if (trx->node()->name()=="Sphere") {
					trx->matrix().identity()
					.translate(0.4f, 5.0f, 0.4f);
				}
			}

		}
		
	}
	_inputVisitor->touchStart(_sceneRoot.getPtr(), evt);
	
	_lastTouch = std::chrono::system_clock::now();
	
}

void MyEventHandler::touchMove(const bg::base::TouchEvent & evt)  {
	_inputVisitor->touchMove(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::touchEnd(const bg::base::TouchEvent & evt) {
	_inputVisitor->touchEnd(_sceneRoot.getPtr(), evt);
}


// Desktop events
void MyEventHandler::keyUp(const bg::base::KeyboardEvent & evt) {
	if (evt.keyboard().key() == bg::base::Keyboard::kKeyEsc) {
		bg::wnd::MainLoop::Get()->quit(0);
	}
	else if (evt.keyboard().key() == bg::base::Keyboard::kKeySpace) {
		if (_dynamics->getSimulationState() == bg::scene::Dynamics::kStatePaused ||
			_dynamics->getSimulationState() == bg::scene::Dynamics::kStateStopped)
		{
			_dynamics->play();
			bg::system::EnergySaver::Get().disableScreenIdleTimer();
		}
		else {
			_dynamics->pause();
			bg::system::EnergySaver::Get().enableScreenIdleTimer();
		}
	}
	else if (evt.keyboard().key() == bg::base::Keyboard::kKeyR) {
		_dynamics->stop();
		
		// Reset the sphere transform
		bg::ptr<bg::scene::FindComponentVisitor<bg::scene::Transform>> visitor = new bg::scene::FindComponentVisitor<bg::scene::Transform>();
		_sceneRoot->accept(visitor.getPtr());
		for (auto trx : visitor->result()) {
			if (trx->node()->name()=="Sphere") {
				trx->matrix().identity()
					.translate(0.4f, 5.0f, 0.4f);
			}
		}
	}
	_inputVisitor->keyUp(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::mouseDown(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseDown(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::mouseDrag(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseDrag(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::mouseWheel(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseWheel(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::mouseUp(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseUp(_sceneRoot.getPtr(), evt);
	
	// Important: the physics simulation must be started before using raycast,
	// but you can pause the simulation loop.
	if (_dynamics->getSimulationState() == bg::scene::Dynamics::kStateStopped) {
		std::cout << "Start the physics simulation loop before use raycast" << std::endl;
	}
	else {
		bg::physics::Ray ray = bg::physics::Ray::RayWithCamera(evt.pos(), _camera.getPtr());
		bg::ptr<bg::physics::RayCastResult> result = _dynamics->world()->rayTest(ray);
		if (result.valid()) {
			std::cout << "Raycast result: " << result->node()->name() << std::endl;
		}
	}
}

