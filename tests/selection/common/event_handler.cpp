/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

//
//  event-handler.cpp
//  bg2e-ios
//
//  Created by Fernando Serrano Carpena on 3/5/16.
//  Copyright © 2016 Fernando Serrano Carpena. All rights reserved.
//

#include <event_handler.hpp>

#include <bg/bg2e.hpp>

bg::scene::Drawable * createFloor(bg::base::Context * ctx) {
	using namespace bg::scene;
	bg::ptr<bg::scene::Drawable> drawable = bg::scene::PrimitiveFactory(ctx).plane(10.0f);
	
	bg::system::Path imagePath = bg::system::Path::AppDir();
	
	bg::ptr<bg::base::Texture> texture = bg::db::loadTexture(ctx, imagePath.pathAddingComponent("data/bricks.jpg"));
	bg::ptr<bg::base::Texture> nmapTex = bg::db::loadTexture(ctx, imagePath.pathAddingComponent("data/bricks_nm.png"));
	bg::ptr<bg::base::Texture> shinTex = bg::db::loadTexture(ctx, imagePath.pathAddingComponent("data/bricks_shin.jpg"));

	drawable->material(0)->setTexture(texture.getPtr());
	drawable->material(0)->setNormalMap(nmapTex.getPtr());
	drawable->material(0)->setShininessMask(shinTex.getPtr());
	drawable->material(0)->setShininess(85.0f);
	drawable->material(0)->setTextureScale(bg::math::Vector2(5.0f));
	drawable->material(0)->setNormalMapScale(bg::math::Vector2(5.0f));

	return drawable.release();
}

bg::scene::Node * createScene(bg::base::Context * ctx) {
	using namespace bg::scene;
	bg::ptr<Node> sceneRoot = new Node(ctx,"SceneRoot");
	
	bg::db::NodeLoader::RegisterPlugin(new bg::db::plugin::ReadPrefabBg2());
	
	bg::system::Path objectPath = bg::system::Path::AppDir();
	Node * cube = bg::db::loadPrefab(ctx, objectPath.pathAddingComponent("data/test-shape.bg2"));
	cube->component<Drawable>()->material(0)->diffuse().a(0.5f);
	cube->addComponent(new bg::manipulation::Selectable());
	sceneRoot->addChild(cube);
	
	bg::scene::Node * floor = new Node(ctx,"Floor");
	floor->addComponent(createFloor(ctx));
	floor->addComponent(new Transform(bg::math::Matrix4::Translation(0.0f, -0.5f, 0.0f)));
	floor->addComponent(new bg::manipulation::Selectable());
	sceneRoot->addChild(floor);
	

	Node * cam = new Node(ctx, "Camera");
	cam->addComponent(new bg::scene::Camera());
	bg::math::Matrix4 trx;
	trx.identity()
		.rotate(bg::math::trigonometry::degreesToRadians(22.5f), -1.0f, 0.0f, 0.0f)
		.translate(.0f, .0f, 5.0f);
	cam->addComponent(new bg::scene::Transform(trx));
	cam->component<bg::scene::Camera>()->setFocus(5.0f);	// Set the focus at the same distance as the transform is
	cam->addComponent(new bg::manipulation::OrbitNodeController());
	sceneRoot->addChild(cam);

	Node * light = new Node(ctx, "Light");
	light->addComponent(new bg::scene::Light());
	trx.identity()
		.rotate(bg::math::trigonometry::degreesToRadians(55.5f), -1.0f, 0.0f, 0.0f)
		.rotate(bg::math::trigonometry::degreesToRadians(35.0f), 0.0f, 1.0f, 0.0f)
		.translate(.0f, .0f, 10.0f);
	light->addComponent(new bg::scene::Transform(trx));
	sceneRoot->addChild(light);
	
	return sceneRoot.release();
}

MyEventHandler::MyEventHandler()
	:_inputVisitor(new bg::scene::InputVisitor())
{
	
}

MyEventHandler::~MyEventHandler() {
	
}

void MyEventHandler::willCreateContext() {
	if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else if (bg::system::isDesktop()){
		throw bg::base::CompatibilityException("Fatal error: no suitable rendering engine found.");
	}
}

void MyEventHandler::initGL() {
	bg::Engine::Get()->initialize(context());
	
	_renderer = bg::render::Renderer::Create(context(), bg::render::Renderer::kRenderPathForward);

	bg::render::ShadowMap * shadowMap = _renderer->settings<bg::render::ShadowMap>();
	if (shadowMap) {
		shadowMap->setShadowMapSize(bg::math::Size2Di(2048));
		shadowMap->setShadowType(bg::render::ShadowMap::kSoftShadows);
	}
		
	_sceneRoot = createScene(context());
	_sceneRoot->someChild([&](bg::scene::Node * node) -> bool {
		return (_camera = node->component<bg::scene::Camera>()).valid();
	});
	
	_mousePicker = new bg::manipulation::MousePicker(context());
}

void MyEventHandler::reshape(int w, int h) {
	_camera->setViewport(bg::math::Viewport(0,0,w,h));
	_camera->projection().perspective(45.0f, _camera->viewport().aspectRatio(), 0.1f, 100.0f);
}

void MyEventHandler::frame(float delta) {
	_renderer->frame(_sceneRoot.getPtr(), delta);
}

void MyEventHandler::draw() {
	_renderer->draw(_sceneRoot.getPtr(), _camera.getPtr());
	context()->swapBuffers();
}

void MyEventHandler::onMemoryWarning() {
	std::cout << "Memory warning received" << std::endl;
}

// Mobile touch events
void MyEventHandler::touchStart(const bg::base::TouchEvent & evt)  {
	_inputVisitor->touchStart(_sceneRoot.getPtr(), evt);
	selectionStart(*evt.touches().front());
}

void MyEventHandler::touchMove(const bg::base::TouchEvent & evt)  {
	_inputVisitor->touchMove(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::touchEnd(const bg::base::TouchEvent & evt) {
	_inputVisitor->touchEnd(_sceneRoot.getPtr(), evt);
	selectionEnd(*evt.touches().front());
}


// Desktop events
void MyEventHandler::keyUp(const bg::base::KeyboardEvent & evt) {
	if (evt.keyboard().key() == bg::base::Keyboard::kKeyEsc) {
		bg::wnd::MainLoop::Get()->quit(0);
	}
}

void MyEventHandler::mouseDown(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseDown(_sceneRoot.getPtr(), evt);
	selectionStart(evt.pos());
}

void MyEventHandler::mouseDrag(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseDrag(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::mouseUp(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseUp(_sceneRoot.getPtr(), evt);
	selectionEnd(evt.pos());
}

void MyEventHandler::mouseMove(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseMove(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::mouseWheel(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseWheel(_sceneRoot.getPtr(), evt);
}

void MyEventHandler::selectionStart(const bg::math::Position2Di & pos) {
	_mouseDownPos = pos;
}

void MyEventHandler::selectionEnd(const bg::math::Position2Di & pos) {
	using namespace bg::math;
	using namespace bg::manipulation;
	Position2Di upPosition = pos;
	
	if (bg::math::abs(_mouseDownPos.distance(upPosition))<3) {
		if (_selection.valid() && _selection->valid() && _materialBkp.valid()) {
			bg::tools::MaterialTools tools(_selection->material());
			tools.copyPropertiesOf(_materialBkp.getPtr());
			_materialBkp = nullptr;
			_selection = nullptr;
		}
		bg::ptr<SelectableItem> selected = _mousePicker->pick(_sceneRoot.getPtr(), _camera.getPtr(), upPosition);
		if (selected.valid() && selected->valid()) {
			_materialBkp = new bg::base::Material(selected->material());
			selected->material()->setDiffuse(bg::math::Color::Green());
			selected->material()->setTexture(nullptr);
			selected->material()->setLightEmission(0.6f);
			selected->material()->setReceiveShadows(false);
			_selection = selected;
			std::cout << "You picked something!" << std::endl;
		}
	}
}

