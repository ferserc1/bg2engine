/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef _composer_ui_action_hpp_
#define _composer_ui_action_hpp_

#include <bg/wnd/popup_menu.hpp>

#include <bg/gui/button.hpp>

#include <map>

namespace composer {
namespace ui {

class UserActions;
class ActionDelegate : public bg::base::ReferencedPointer {
public:
	virtual void registerActions(UserActions &) = 0;

protected:
	virtual ~ActionDelegate() {}
};

class Action : public bg::base::ReferencedPointer {
public:
	typedef int32_t ActionCode;
	typedef std::function<void()> ActionClosure;

	Action(ActionCode,const std::string & title, ActionClosure);
	Action(ActionCode, const std::string & title, const std::string & menuPath, ActionClosure);
	Action(ActionCode, const std::string & title, const std::string & menuPath, const bg::base::KeyboardShortcut &, ActionClosure);

	inline ActionCode code() const { return _code; }
	inline const std::string title() const { return _title; }
	inline const std::string menuPath() const { return _menuPath; }
	inline const bg::base::KeyboardShortcut & shortcut() const { return _shortcut; }

	inline void execute() const { if (_action) _action(); }

	inline bool operator==(ActionCode c) const { return _code == c; }

protected:
	virtual ~Action();

	ActionCode _code;
	std::string _title;
	std::string _menuPath;
	bg::base::KeyboardShortcut _shortcut;
	ActionClosure _action;
};

class UserActions {
public:

	void registerAction(Action *);

	Action * find(Action::ActionCode);

	void exec(Action::ActionCode);

	inline void addDelegate(ActionDelegate *d) { _actionDelegates.push_back(d); }

	void triggerDelegates();

protected:
	std::map<Action::ActionCode, bg::ptr<Action>> _actionMap;
	std::vector<bg::ptr<ActionDelegate>> _actionDelegates;
};


}
}

#endif
