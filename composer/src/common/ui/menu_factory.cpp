/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <ui/menu_factory.hpp>

#include <bg/base/exception.hpp>

#include <iostream>
#include <sstream>

namespace composer {
namespace ui {

MenuFactory::MenuFactory()
{
	using namespace bg::wnd;
	// TODO: Refactor this to allows building menu from other parts of the application
	
	//PopUpMenu * file = PopUpMenu::New("File");

	//PopUpMenu * edit = PopUpMenu::New("Edit");
}

MenuFactory::~MenuFactory() {
}

void MenuFactory::addMenu(const std::string & p, int32_t identifier) {
	using namespace bg::wnd;
	std::stringstream sstream(p);
	std::string s;
	std::vector<std::string> path;
	while (std::getline(sstream, s, '/')) {
		path.push_back(s);
	}

	if (path.size() != 2) {
		throw bg::base::InvalidParameterException("Menu path must have exactly two elements.");
	}

	bg::ptr<PopUpMenu> menu;
	if (_menuNames.find(path[0]) == _menuNames.end()) {
		menu = PopUpMenu::New(path[0]);
		_menu.push_back(menu);
		_menuNames[path[0]] = menu.getPtr();
	}
	else {
		menu = _menuNames[path[0]];
	}
	menu->addMenuItem({ identifier, path[1] });
}

void MenuFactory::addMenu(const std::string & p, int32_t identifier, const bg::base::KeyboardShortcut & sc) {
	using namespace bg::wnd;
	std::stringstream sstream(p);
	std::string s;
	std::vector<std::string> path;
	while (std::getline(sstream, s, '/')) {
		path.push_back(s);
	}

	if (path.size() != 2) {
		throw bg::base::InvalidParameterException("Menu path must have exactly two elements.");
	}

	bg::ptr<PopUpMenu> menu;
	if (_menuNames.find(path[0]) == _menuNames.end()) {
		menu = PopUpMenu::New(path[0]);
		_menu.push_back(menu);
		_menuNames[path[0]] = menu.getPtr();
	}
	else {
		menu = _menuNames[path[0]];
	}
	menu->addMenuItem({ identifier, path[1], sc });
}

}
}
