/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

//
//  event-handler.cpp
//  bg2e-ios
//
//  Created by Fernando Serrano Carpena on 3/5/16.
//  Copyright © 2016 Fernando Serrano Carpena. All rights reserved.
//

#include "event_handler.hpp"

#include <bg/bg2e.hpp>

MyEventHandler::MyEventHandler()
{
	
}

MyEventHandler::~MyEventHandler() {
	
}

void MyEventHandler::willCreateContext() {
	if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else if (bg::system::isDesktop()){
		throw bg::base::CompatibilityException("Fatal error: no suitable rendering engine found.");
	}
}

void MyEventHandler::initGL() {
	bg::Engine::Get()->initialize(context());
	
	bg::fx::Forward * fx = new bg::fx::Forward(context());
	_effect = fx;

	bg::math::Matrix4 trx;
	trx.identity()
		.rotate(bg::math::trigonometry::degreesToRadians(15.0f), 0.0f, 1.0, 0.0)
		.rotate(bg::math::trigonometry::degreesToRadians(55.0f), -1.0f, 0.0, 0.0)
		.translate(0.0f, 0.0f, -10.0f);
	fx->lightData().addLight(new bg::base::Light(), trx, nullptr);

	_pipeline = new bg::base::Pipeline(context());
	_pipeline->setClearColor(bg::math::Color(0.11f,0.3f,0.5f,1.0f));
	_pipeline->setEffect(_effect.getPtr());
	bg::base::Pipeline::SetCurrent(_pipeline.getPtr());

	_offscreenPipeline = new bg::base::Pipeline(context());
	_offscreenPipeline->setClearColor(bg::math::Color(0.95f,0.2f,0.33f,1.0f));
	_offscreenPipeline->setEffect(_effect.getPtr());

	bg::base::TextureRenderSurface * rs = new bg::base::TextureRenderSurface(context());
	rs->create();
	_offscreenPipeline->setRenderSurface(rs);
	_offscreenPipeline->setViewport(bg::math::Viewport(0,0,512,512));

	bg::system::Path imagePath = bg::system::Path::ExecDir();
	if (bg::system::currentPlatform()==bg::system::kMac) {
		imagePath.addComponent("../../../");
	}
	imagePath.addComponent("data/texture.jpg");

	bg::ptr<bg::base::Image> image = new bg::base::Image();
	image->load(imagePath);
	_texture = new bg::base::Texture(context());
	_texture->setWrapModeU(bg::base::Texture::kWrapModeRepeat);
	_texture->setWrapModeV(bg::base::Texture::kWrapModeRepeat);
	_texture->createWithImage(image.getPtr());
	_effect->material().setTexture(_texture.getPtr());
	
	_effect->matrixState().projectionMatrixStack().matrix()
		.perspective(45.0f, 1.0f, 0.1f, 100.0f);
	
	_effect->matrixState().viewMatrixStack().matrix()
		.identity()
		.rotate(bg::math::trigonometry::degreesToRadians(22.5f), -1.0f, 0.0f, 0.0f)
		.translate(0.0f, 0.0f, 5.0f)
		.invert();

	_effect->matrixState().modelMatrixStack().matrix()
		.identity();

	_plist = new bg::base::PolyList(context());

	std::vector<float> position = {
		 1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f,  1.0f, -1.0f,  1.0f,  1.0f, -1.0f,		// back face
		 1.0f, -1.0f,  1.0f,  1.0f, -1.0f, -1.0f,  1.0f,  1.0f, -1.0f,  1.0f,  1.0f,  1.0f,		// right face 
		-1.0f, -1.0f,  1.0f,  1.0f, -1.0f,  1.0f,  1.0f,  1.0f,  1.0f, -1.0f,  1.0f,  1.0f, 	// front face
		-1.0f, -1.0f, -1.0f, -1.0f, -1.0f,  1.0f, -1.0f,  1.0f,  1.0f, -1.0f,  1.0f, -1.0f,		// left face
		-1.0f,  1.0f,  1.0f,  1.0f,  1.0f,  1.0f,  1.0f,  1.0f, -1.0f, -1.0f,  1.0f, -1.0f,		// top face
		 1.0f, -1.0f,  1.0f, -1.0f, -1.0f,  1.0f, -1.0f, -1.0f, -1.0f,  1.0f, -1.0f, -1.0f		// bottom face
	};


	std::vector<float> normal = {
		 0.0f,  0.0f, -1.0f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f, -1.0f,		// back face
		 1.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,		// right face 
		 0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f, 	// front face
		-1.0f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,		// left face
		 0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,		// top face
		 0.0f, -1.0f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f, -1.0f,  0.0f		// bottom face
	};

	std::vector<float> texCoord0 = {
		0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f
	};

	std::vector<float> texCoord1 = {
		0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f
	};

	std::vector<unsigned int> index = {
		 0,  1,  2,  2,  3,  0,
		 4,  5,  6,  6,  7,  4,
		 8,  9, 10, 10, 11,  8,
		12, 13, 14, 14, 15, 12,
		16, 17, 18, 18, 19, 16,
		20, 21, 22, 22, 23, 20
	};

	_plist->addVertexVector(&position[0], position.size());
	_plist->addNormalVector(&normal[0], normal.size());
	_plist->addTexCoord0Vector(&texCoord0[0], texCoord0.size());
	_plist->addTexCoord1Vector(&texCoord1[0], texCoord1.size());

	_plist->addIndexVector(&index[0], index.size());

	_plist->build();
}

void MyEventHandler::reshape(int w, int h) {
	_pipeline->setViewport(bg::math::Viewport(0,0,w,h));
	_effect->matrixState().projectionMatrixStack().matrix()
		.perspective(45.0f, _pipeline->viewport().aspectRatio(), 0.1f, 100.0f);
}

void MyEventHandler::frame(float delta) {
	_effect->matrixState().modelMatrixStack()
		.rotate(bg::math::trigonometry::degreesToRadians(delta * 10.0f), 0.0f, 1.0f, 0.0f);
}

void MyEventHandler::draw() {
	bg::base::Pipeline::SetCurrent(_offscreenPipeline.getPtr());
	_offscreenPipeline->effect()->matrixState().projectionMatrixStack()
		.set(bg::math::Matrix4::Perspective(45.0f, _offscreenPipeline->viewport().aspectRatio(), 0.1f, 100.0f));
	_offscreenPipeline->clearBuffers(bg::base::ClearBuffers::kColorDepth);
	_offscreenPipeline->effect()->material().setTexture(_texture.getPtr());
	_offscreenPipeline->draw(_plist.getPtr());

	bg::base::Pipeline::SetCurrent(_pipeline.getPtr());
	bg::base::TextureRenderSurface * rs = dynamic_cast<bg::base::TextureRenderSurface*>(_offscreenPipeline->renderSurface());
	_pipeline->effect()->matrixState().projectionMatrixStack()
		.set(bg::math::Matrix4::Perspective(45.0f, _pipeline->viewport().aspectRatio(), 0.1f, 100.0f));
	_pipeline->effect()->material().setTexture(rs->texture(0));
	_pipeline->clearBuffers(bg::base::ClearBuffers::kColorDepth);
	_pipeline->draw(_plist.getPtr());

	context()->swapBuffers();
}

void MyEventHandler::onMemoryWarning() {
	std::cout << "Memory warning received" << std::endl;
}

// Mobile touch events
void MyEventHandler::touchStart(const bg::base::TouchEvent & evt)  {
}

void MyEventHandler::touchMove(const bg::base::TouchEvent & evt)  {
}

void MyEventHandler::touchEnd(const bg::base::TouchEvent & evt) {
}


// Desktop events
void MyEventHandler::keyUp(const bg::base::KeyboardEvent & evt) {
	if (evt.keyboard().key() == bg::base::Keyboard::kKeyEsc) {
		bg::wnd::MainLoop::Get()->quit(0);
	}
}

void MyEventHandler::mouseDown(const bg::base::MouseEvent & evt) {
}

void MyEventHandler::mouseDrag(const bg::base::MouseEvent & evt) {
}

