/*
 *	bg2 engine license
 *	Copyright (c) 2016 Fernando Serrano <ferserc1@gmail.com>
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *	of the Software, and to permit persons to whom the Software is furnished to do
 *	so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef _composer_scene_scene_hpp_
#define _composer_scene_scene_hpp_

#include <bg/base/context_object.hpp>

#include <bg/scene/node.hpp>
#include <bg/manipulation/orbit_node_controller.hpp>
#include <bg/scene/camera.hpp>
#include <bg/scene/light.hpp>


namespace composer {
namespace scene {

class Scene : public bg::base::ContextObject, public bg::base::ReferencedPointer {
public:
	Scene(bg::base::Context *);

	void newScene();
	void open(const std::string & path);

	void import (const std::string & path);
	
	void close();
	
	void destroy();

	inline bg::scene::Node * sceneRoot() { return _sceneRoot.getPtr(); }
	inline bg::scene::Camera * mainCamera() { return _mainCamera.getPtr(); }
	inline bg::manipulation::OrbitNodeController * cameraController() {
		return _mainCamera->node()->component<bg::manipulation::OrbitNodeController>();
	}
	inline bg::scene::OpticalProjectionStrategy * cameraProjection() {
		return dynamic_cast<bg::scene::OpticalProjectionStrategy*>(_mainCamera->projectionStrategy());
	}

protected:
	virtual ~Scene();

	bg::ptr<bg::scene::Node> _sceneRoot;
	bg::ptr<bg::scene::Camera> _mainCamera;
	bg::ptr<bg::scene::Light> _mainLight;

	std::string _currentScenePath;

	void createEmptyScene();

	void checkCameraPresent(bg::scene::Node * root);
	void checkLightPresent(bg::scene::Node * root);
};

}
}

#endif
